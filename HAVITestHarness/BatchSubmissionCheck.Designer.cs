﻿namespace HAVITestHarness
{
    partial class BatchSubmissionCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gvBatchSubmissionView = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbRejected = new System.Windows.Forms.CheckBox();
            this.cbApproved = new System.Windows.Forms.CheckBox();
            this.cbSubmitted = new System.Windows.Forms.CheckBox();
            this.cbInProgress = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbSentToJDEAll = new System.Windows.Forms.RadioButton();
            this.rbSentToJDEYes = new System.Windows.Forms.RadioButton();
            this.rbSentToJDENo = new System.Windows.Forms.RadioButton();
            this.lblCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gvBatchSubmissionView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvBatchSubmissionView
            // 
            this.gvBatchSubmissionView.AllowUserToAddRows = false;
            this.gvBatchSubmissionView.AllowUserToDeleteRows = false;
            this.gvBatchSubmissionView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvBatchSubmissionView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvBatchSubmissionView.Location = new System.Drawing.Point(115, 3);
            this.gvBatchSubmissionView.Name = "gvBatchSubmissionView";
            this.gvBatchSubmissionView.ReadOnly = true;
            this.gvBatchSubmissionView.Size = new System.Drawing.Size(685, 447);
            this.gvBatchSubmissionView.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbRejected);
            this.groupBox1.Controls.Add(this.cbApproved);
            this.groupBox1.Controls.Add(this.cbSubmitted);
            this.groupBox1.Controls.Add(this.cbInProgress);
            this.groupBox1.Location = new System.Drawing.Point(4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(105, 114);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status";
            // 
            // cbRejected
            // 
            this.cbRejected.AutoSize = true;
            this.cbRejected.Location = new System.Drawing.Point(7, 89);
            this.cbRejected.Name = "cbRejected";
            this.cbRejected.Size = new System.Drawing.Size(69, 17);
            this.cbRejected.TabIndex = 3;
            this.cbRejected.Text = "Rejected";
            this.cbRejected.UseVisualStyleBackColor = true;
            this.cbRejected.CheckedChanged += new System.EventHandler(this.cbRejected_CheckedChanged);
            // 
            // cbApproved
            // 
            this.cbApproved.AutoSize = true;
            this.cbApproved.Location = new System.Drawing.Point(7, 66);
            this.cbApproved.Name = "cbApproved";
            this.cbApproved.Size = new System.Drawing.Size(72, 17);
            this.cbApproved.TabIndex = 2;
            this.cbApproved.Text = "Approved";
            this.cbApproved.UseVisualStyleBackColor = true;
            this.cbApproved.CheckedChanged += new System.EventHandler(this.cbApproved_CheckedChanged);
            // 
            // cbSubmitted
            // 
            this.cbSubmitted.AutoSize = true;
            this.cbSubmitted.Location = new System.Drawing.Point(7, 43);
            this.cbSubmitted.Name = "cbSubmitted";
            this.cbSubmitted.Size = new System.Drawing.Size(73, 17);
            this.cbSubmitted.TabIndex = 1;
            this.cbSubmitted.Text = "Submitted";
            this.cbSubmitted.UseVisualStyleBackColor = true;
            this.cbSubmitted.CheckedChanged += new System.EventHandler(this.cbSubmitted_CheckedChanged);
            // 
            // cbInProgress
            // 
            this.cbInProgress.AutoSize = true;
            this.cbInProgress.Location = new System.Drawing.Point(7, 20);
            this.cbInProgress.Name = "cbInProgress";
            this.cbInProgress.Size = new System.Drawing.Size(79, 17);
            this.cbInProgress.TabIndex = 0;
            this.cbInProgress.Text = "In Progress";
            this.cbInProgress.UseVisualStyleBackColor = true;
            this.cbInProgress.CheckedChanged += new System.EventHandler(this.cbInProgress_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbSentToJDENo);
            this.groupBox2.Controls.Add(this.rbSentToJDEYes);
            this.groupBox2.Controls.Add(this.rbSentToJDEAll);
            this.groupBox2.Location = new System.Drawing.Point(4, 124);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(105, 90);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sent to JDE";
            // 
            // rbSentToJDEAll
            // 
            this.rbSentToJDEAll.AutoSize = true;
            this.rbSentToJDEAll.Location = new System.Drawing.Point(7, 20);
            this.rbSentToJDEAll.Name = "rbSentToJDEAll";
            this.rbSentToJDEAll.Size = new System.Drawing.Size(36, 17);
            this.rbSentToJDEAll.TabIndex = 0;
            this.rbSentToJDEAll.TabStop = true;
            this.rbSentToJDEAll.Text = "All";
            this.rbSentToJDEAll.UseVisualStyleBackColor = true;
            this.rbSentToJDEAll.CheckedChanged += new System.EventHandler(this.rbSentToJDEAll_CheckedChanged);
            // 
            // rbSentToJDEYes
            // 
            this.rbSentToJDEYes.AutoSize = true;
            this.rbSentToJDEYes.Location = new System.Drawing.Point(7, 43);
            this.rbSentToJDEYes.Name = "rbSentToJDEYes";
            this.rbSentToJDEYes.Size = new System.Drawing.Size(43, 17);
            this.rbSentToJDEYes.TabIndex = 1;
            this.rbSentToJDEYes.TabStop = true;
            this.rbSentToJDEYes.Text = "Yes";
            this.rbSentToJDEYes.UseVisualStyleBackColor = true;
            this.rbSentToJDEYes.CheckedChanged += new System.EventHandler(this.rbSentToJDEYes_CheckedChanged);
            // 
            // rbSentToJDENo
            // 
            this.rbSentToJDENo.AutoSize = true;
            this.rbSentToJDENo.Location = new System.Drawing.Point(7, 66);
            this.rbSentToJDENo.Name = "rbSentToJDENo";
            this.rbSentToJDENo.Size = new System.Drawing.Size(39, 17);
            this.rbSentToJDENo.TabIndex = 2;
            this.rbSentToJDENo.TabStop = true;
            this.rbSentToJDENo.Text = "No";
            this.rbSentToJDENo.UseVisualStyleBackColor = true;
            this.rbSentToJDENo.CheckedChanged += new System.EventHandler(this.rbSentToJDENo_CheckedChanged);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(4, 221);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(49, 13);
            this.lblCount.TabIndex = 3;
            this.lblCount.Text = "MyCount";
            // 
            // BatchSubmissionCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gvBatchSubmissionView);
            this.Name = "BatchSubmissionCheck";
            this.Text = "Batch Submission Check";
            this.Load += new System.EventHandler(this.BatchSubmissionCheck_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvBatchSubmissionView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvBatchSubmissionView;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbRejected;
        private System.Windows.Forms.CheckBox cbApproved;
        private System.Windows.Forms.CheckBox cbSubmitted;
        private System.Windows.Forms.CheckBox cbInProgress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbSentToJDENo;
        private System.Windows.Forms.RadioButton rbSentToJDEYes;
        private System.Windows.Forms.RadioButton rbSentToJDEAll;
        private System.Windows.Forms.Label lblCount;
    }
}