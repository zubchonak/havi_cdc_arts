﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HAVITestProject;

namespace HAVITestHarness
{
    public partial class BatchSubmissionCheck : Form
    {
        public string CDCEnv = "QA";
        

        public BatchSubmissionCheck()
        {
            InitializeComponent();
        }

        private void BatchSubmissionCheck_Load(object sender, EventArgs e)
        {
            this.Text = "Batch Submission (" + CDCEnv + ")";

            cbApproved.Checked = true;

        }
        private void LoadBatches()
        {
            string InProgressCode = "0";
            string SubmittedCode = "0";
            string ApprovedCode = "0";
            string RejectedCode = "0";
            if (cbInProgress.Checked) InProgressCode = "1";
            if (cbSubmitted.Checked) SubmittedCode = "2";
            if (cbApproved.Checked) ApprovedCode = "3";
            if (cbRejected.Checked) RejectedCode = "4";

            

            GlobalLib GlobalLib = new GlobalLib();

            string sSQL = @"SELECT bs.[SentToJDE]
	                              ,bs.[BatchNumber]
                                  ,bs.[BatchName]
                                  ,bs.[BatchDesc]
                                  ,bs.[EffectiveDate]
                                  ,rs.[ReviewStatusText]
                                  ,bs.[CreatedDate]
                                  ,u1.[UserUpn] as [CreatedBy]
                                  ,bs.[ModifiedDate]
                                  ,u2.[UserUpn] as [ModifiedBy]
                                  ,bs.[SubmittedDate]
                                  ,u3.[UserUpn] as [SubmittedBy]
                                  ,u4.[UserUpn] as [ReviewedBy]
                                  ,bs.[ReviewDate]      
                            FROM BatchSubmission bs
                            INNER JOIN ReviewStatus rs on bs.ReviewStatusId = rs.ReviewStatusId
                            LEFT JOIN CDCUser u1 ON bs.CreatedBy = u1.UserId
                            LEFT JOIN CDCUser u2 ON bs.ModifiedBy = u2.UserId
                            LEFT JOIN CDCUser u3 ON bs.SubmittedBy = u3.UserId
                            LEFT JOIN CDCUser u4 ON bs.ReviewedBy = u4.UserId
                            WHERE bs.ReviewStatusId in (" + RejectedCode + "," + ApprovedCode + "," + SubmittedCode + "," + InProgressCode + @") " +
                            SentToJDESelect() + " order by bs.ReviewStatusId, bs.BatchNumber";

            DataTable BatchSubmissionTable = GlobalLib.CDCQueryTableToDataTable(sSQL, CDCEnv);
            lblCount.Text = "Count: " + BatchSubmissionTable.Rows.Count.ToString();
            gvBatchSubmissionView.DataSource = BatchSubmissionTable;
        }
        private string SentToJDESelect()
        {
            string OutSQL = "";

            if (rbSentToJDEYes.Checked) OutSQL = "and bs.[SentToJDE] = 1";
            if (rbSentToJDENo.Checked) OutSQL = "and bs.[SentToJDE] = 0"; //"and bs.[SentToJDE] is null";
            if (rbSentToJDEAll.Checked) OutSQL = "";

            return (OutSQL);
        }
        private void cbInProgress_CheckedChanged(object sender, EventArgs e)
        {
            rbSentToJDEAll.Checked = true;
            LoadBatches();
        }

        private void cbSubmitted_CheckedChanged(object sender, EventArgs e)
        {
            rbSentToJDEAll.Checked = true;
            LoadBatches();
        }

        private void cbApproved_CheckedChanged(object sender, EventArgs e)
        {
            rbSentToJDEAll.Checked = true;
            LoadBatches();
        }

        private void cbRejected_CheckedChanged(object sender, EventArgs e)
        {
            rbSentToJDEAll.Checked = true;
            LoadBatches();
        }

        private void rbSentToJDEAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadBatches();
        }

        private void rbSentToJDEYes_CheckedChanged(object sender, EventArgs e)
        {
            LoadBatches();
        }

        private void rbSentToJDENo_CheckedChanged(object sender, EventArgs e)
        {
            LoadBatches();
        }
    }
}
