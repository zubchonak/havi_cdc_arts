﻿namespace HAVITestHarness
{
    partial class CDCUserMng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbFTS = new System.Windows.Forms.RadioButton();
            this.cbIsActiveUser = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.pbAddNewWSI = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtAddWSI = new System.Windows.Forms.TextBox();
            this.pbAddUpdate = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblGoupId = new System.Windows.Forms.Label();
            this.pbAddGroup = new System.Windows.Forms.Button();
            this.pbSetGroup = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtGroupDesc = new System.Windows.Forms.TextBox();
            this.txtGroupCode = new System.Windows.Forms.TextBox();
            this.lbGroupList = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pbAddWSI = new System.Windows.Forms.Button();
            this.pbRemoveWSI = new System.Windows.Forms.Button();
            this.pbAddCat = new System.Windows.Forms.Button();
            this.pbRemoveCat = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbAvailableCategory = new System.Windows.Forms.ListBox();
            this.lbAssignedCategory = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbAssignedWSI = new System.Windows.Forms.ListBox();
            this.lbAvailableWSI = new System.Windows.Forms.ListBox();
            this.rbCategoryLead = new System.Windows.Forms.RadioButton();
            this.rbSupplier = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.UserEmail = new System.Windows.Forms.TextBox();
            this.CDCUsers = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbFTS);
            this.groupBox1.Controls.Add(this.cbIsActiveUser);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtLastName);
            this.groupBox1.Controls.Add(this.pbAddNewWSI);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtFirstName);
            this.groupBox1.Controls.Add(this.txtAddWSI);
            this.groupBox1.Controls.Add(this.pbAddUpdate);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.pbAddWSI);
            this.groupBox1.Controls.Add(this.pbRemoveWSI);
            this.groupBox1.Controls.Add(this.pbAddCat);
            this.groupBox1.Controls.Add(this.pbRemoveCat);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lbAvailableCategory);
            this.groupBox1.Controls.Add(this.lbAssignedCategory);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lbAssignedWSI);
            this.groupBox1.Controls.Add(this.lbAvailableWSI);
            this.groupBox1.Controls.Add(this.rbCategoryLead);
            this.groupBox1.Controls.Add(this.rbSupplier);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.UserEmail);
            this.groupBox1.Controls.Add(this.CDCUsers);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(678, 412);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CDC Users";
            // 
            // rbFTS
            // 
            this.rbFTS.AutoSize = true;
            this.rbFTS.Location = new System.Drawing.Point(282, 130);
            this.rbFTS.Name = "rbFTS";
            this.rbFTS.Size = new System.Drawing.Size(45, 17);
            this.rbFTS.TabIndex = 32;
            this.rbFTS.TabStop = true;
            this.rbFTS.Text = "FTS";
            this.rbFTS.UseVisualStyleBackColor = true;
            // 
            // cbIsActiveUser
            // 
            this.cbIsActiveUser.AutoSize = true;
            this.cbIsActiveUser.Location = new System.Drawing.Point(385, 100);
            this.cbIsActiveUser.Name = "cbIsActiveUser";
            this.cbIsActiveUser.Size = new System.Drawing.Size(56, 17);
            this.cbIsActiveUser.TabIndex = 31;
            this.cbIsActiveUser.Text = "Active";
            this.cbIsActiveUser.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(343, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Last Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(487, 360);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "WSI #:";
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(346, 37);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(121, 20);
            this.txtLastName.TabIndex = 2;
            // 
            // pbAddNewWSI
            // 
            this.pbAddNewWSI.Location = new System.Drawing.Point(579, 372);
            this.pbAddNewWSI.Name = "pbAddNewWSI";
            this.pbAddNewWSI.Size = new System.Drawing.Size(83, 23);
            this.pbAddNewWSI.TabIndex = 29;
            this.pbAddNewWSI.Text = "Add New WSI";
            this.pbAddNewWSI.UseVisualStyleBackColor = true;
            this.pbAddNewWSI.Click += new System.EventHandler(this.pbAddNewWSI_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(219, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "First Name:";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(222, 37);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(121, 20);
            this.txtFirstName.TabIndex = 0;
            // 
            // txtAddWSI
            // 
            this.txtAddWSI.Location = new System.Drawing.Point(487, 375);
            this.txtAddWSI.MaxLength = 10;
            this.txtAddWSI.Name = "txtAddWSI";
            this.txtAddWSI.Size = new System.Drawing.Size(86, 20);
            this.txtAddWSI.TabIndex = 28;
            this.txtAddWSI.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddWSI_KeyDown);
            // 
            // pbAddUpdate
            // 
            this.pbAddUpdate.Location = new System.Drawing.Point(385, 121);
            this.pbAddUpdate.Name = "pbAddUpdate";
            this.pbAddUpdate.Size = new System.Drawing.Size(82, 24);
            this.pbAddUpdate.TabIndex = 27;
            this.pbAddUpdate.Text = "Add/Update";
            this.pbAddUpdate.UseVisualStyleBackColor = true;
            this.pbAddUpdate.Click += new System.EventHandler(this.pbAddUpdate_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Gainsboro;
            this.groupBox2.Controls.Add(this.lblGoupId);
            this.groupBox2.Controls.Add(this.pbAddGroup);
            this.groupBox2.Controls.Add(this.pbSetGroup);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtGroupDesc);
            this.groupBox2.Controls.Add(this.txtGroupCode);
            this.groupBox2.Controls.Add(this.lbGroupList);
            this.groupBox2.Location = new System.Drawing.Point(222, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 133);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Group:";
            // 
            // lblGoupId
            // 
            this.lblGoupId.AutoSize = true;
            this.lblGoupId.Location = new System.Drawing.Point(220, 11);
            this.lblGoupId.Name = "lblGoupId";
            this.lblGoupId.Size = new System.Drawing.Size(13, 13);
            this.lblGoupId.TabIndex = 26;
            this.lblGoupId.Text = "0";
            this.lblGoupId.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // pbAddGroup
            // 
            this.pbAddGroup.Location = new System.Drawing.Point(191, 102);
            this.pbAddGroup.Name = "pbAddGroup";
            this.pbAddGroup.Size = new System.Drawing.Size(44, 23);
            this.pbAddGroup.TabIndex = 24;
            this.pbAddGroup.Text = "Add Group";
            this.pbAddGroup.UseVisualStyleBackColor = true;
            this.pbAddGroup.Click += new System.EventHandler(this.pbAddGroup_Click);
            // 
            // pbSetGroup
            // 
            this.pbSetGroup.Location = new System.Drawing.Point(126, 102);
            this.pbSetGroup.Name = "pbSetGroup";
            this.pbSetGroup.Size = new System.Drawing.Size(47, 23);
            this.pbSetGroup.TabIndex = 25;
            this.pbSetGroup.Text = "Set Group";
            this.pbSetGroup.UseVisualStyleBackColor = true;
            this.pbSetGroup.Click += new System.EventHandler(this.pbSetGroup_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(123, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Group Desc:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(123, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Group Code:";
            // 
            // txtGroupDesc
            // 
            this.txtGroupDesc.Location = new System.Drawing.Point(126, 77);
            this.txtGroupDesc.MaxLength = 100;
            this.txtGroupDesc.Name = "txtGroupDesc";
            this.txtGroupDesc.Size = new System.Drawing.Size(109, 20);
            this.txtGroupDesc.TabIndex = 21;
            // 
            // txtGroupCode
            // 
            this.txtGroupCode.Location = new System.Drawing.Point(126, 33);
            this.txtGroupCode.MaxLength = 10;
            this.txtGroupCode.Name = "txtGroupCode";
            this.txtGroupCode.Size = new System.Drawing.Size(109, 20);
            this.txtGroupCode.TabIndex = 20;
            // 
            // lbGroupList
            // 
            this.lbGroupList.FormattingEnabled = true;
            this.lbGroupList.Location = new System.Drawing.Point(8, 17);
            this.lbGroupList.Name = "lbGroupList";
            this.lbGroupList.Size = new System.Drawing.Size(109, 108);
            this.lbGroupList.TabIndex = 5;
            this.lbGroupList.SelectedIndexChanged += new System.EventHandler(this.lbGroupList_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(222, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "User Role:";
            // 
            // pbAddWSI
            // 
            this.pbAddWSI.Location = new System.Drawing.Point(563, 208);
            this.pbAddWSI.Name = "pbAddWSI";
            this.pbAddWSI.Size = new System.Drawing.Size(24, 23);
            this.pbAddWSI.TabIndex = 17;
            this.pbAddWSI.Text = "<";
            this.pbAddWSI.UseVisualStyleBackColor = true;
            this.pbAddWSI.Click += new System.EventHandler(this.button1_Click);
            // 
            // pbRemoveWSI
            // 
            this.pbRemoveWSI.Location = new System.Drawing.Point(563, 179);
            this.pbRemoveWSI.Name = "pbRemoveWSI";
            this.pbRemoveWSI.Size = new System.Drawing.Size(24, 23);
            this.pbRemoveWSI.TabIndex = 16;
            this.pbRemoveWSI.Text = ">";
            this.pbRemoveWSI.UseVisualStyleBackColor = true;
            this.pbRemoveWSI.Click += new System.EventHandler(this.pbRemoveWSI_Click);
            // 
            // pbAddCat
            // 
            this.pbAddCat.Location = new System.Drawing.Point(333, 348);
            this.pbAddCat.Name = "pbAddCat";
            this.pbAddCat.Size = new System.Drawing.Size(24, 23);
            this.pbAddCat.TabIndex = 15;
            this.pbAddCat.Text = "<";
            this.pbAddCat.UseVisualStyleBackColor = true;
            this.pbAddCat.Click += new System.EventHandler(this.pbAddCat_Click);
            // 
            // pbRemoveCat
            // 
            this.pbRemoveCat.Location = new System.Drawing.Point(333, 319);
            this.pbRemoveCat.Name = "pbRemoveCat";
            this.pbRemoveCat.Size = new System.Drawing.Size(24, 23);
            this.pbRemoveCat.TabIndex = 14;
            this.pbRemoveCat.Text = ">";
            this.pbRemoveCat.UseVisualStyleBackColor = true;
            this.pbRemoveCat.Click += new System.EventHandler(this.pbRemoveCat_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(219, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Assigned Category:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(357, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Available Category:";
            // 
            // lbAvailableCategory
            // 
            this.lbAvailableCategory.FormattingEnabled = true;
            this.lbAvailableCategory.Location = new System.Drawing.Point(359, 301);
            this.lbAvailableCategory.Name = "lbAvailableCategory";
            this.lbAvailableCategory.Size = new System.Drawing.Size(109, 95);
            this.lbAvailableCategory.TabIndex = 11;
            this.lbAvailableCategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbAvailableCategory_MouseDoubleClick);
            // 
            // lbAssignedCategory
            // 
            this.lbAssignedCategory.FormattingEnabled = true;
            this.lbAssignedCategory.Location = new System.Drawing.Point(222, 301);
            this.lbAssignedCategory.Name = "lbAssignedCategory";
            this.lbAssignedCategory.Size = new System.Drawing.Size(109, 95);
            this.lbAssignedCategory.TabIndex = 10;
            this.lbAssignedCategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbAssignedCategory_MouseDoubleClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(590, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Available WSI:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(484, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Assigned WSI:";
            // 
            // lbAssignedWSI
            // 
            this.lbAssignedWSI.FormattingEnabled = true;
            this.lbAssignedWSI.Location = new System.Drawing.Point(487, 37);
            this.lbAssignedWSI.Name = "lbAssignedWSI";
            this.lbAssignedWSI.Size = new System.Drawing.Size(70, 316);
            this.lbAssignedWSI.TabIndex = 7;
            this.lbAssignedWSI.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbAssignedWSI_MouseDoubleClick);
            // 
            // lbAvailableWSI
            // 
            this.lbAvailableWSI.FormattingEnabled = true;
            this.lbAvailableWSI.Location = new System.Drawing.Point(592, 37);
            this.lbAvailableWSI.Name = "lbAvailableWSI";
            this.lbAvailableWSI.Size = new System.Drawing.Size(70, 316);
            this.lbAvailableWSI.TabIndex = 6;
            this.lbAvailableWSI.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbAvailableWSI_MouseDoubleClick);
            // 
            // rbCategoryLead
            // 
            this.rbCategoryLead.AutoSize = true;
            this.rbCategoryLead.Location = new System.Drawing.Point(282, 114);
            this.rbCategoryLead.Name = "rbCategoryLead";
            this.rbCategoryLead.Size = new System.Drawing.Size(94, 17);
            this.rbCategoryLead.TabIndex = 4;
            this.rbCategoryLead.TabStop = true;
            this.rbCategoryLead.Text = "Category Lead";
            this.rbCategoryLead.UseVisualStyleBackColor = true;
            this.rbCategoryLead.CheckedChanged += new System.EventHandler(this.rbCategoryLead_CheckedChanged);
            // 
            // rbSupplier
            // 
            this.rbSupplier.AutoSize = true;
            this.rbSupplier.Location = new System.Drawing.Point(282, 98);
            this.rbSupplier.Name = "rbSupplier";
            this.rbSupplier.Size = new System.Drawing.Size(63, 17);
            this.rbSupplier.TabIndex = 3;
            this.rbSupplier.TabStop = true;
            this.rbSupplier.Text = "Supplier";
            this.rbSupplier.UseVisualStyleBackColor = true;
            this.rbSupplier.CheckedChanged += new System.EventHandler(this.rbSupplier_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Login Name:";
            // 
            // UserEmail
            // 
            this.UserEmail.Location = new System.Drawing.Point(222, 75);
            this.UserEmail.Name = "UserEmail";
            this.UserEmail.Size = new System.Drawing.Size(246, 20);
            this.UserEmail.TabIndex = 1;
            // 
            // CDCUsers
            // 
            this.CDCUsers.FormattingEnabled = true;
            this.CDCUsers.Location = new System.Drawing.Point(17, 15);
            this.CDCUsers.Name = "CDCUsers";
            this.CDCUsers.Size = new System.Drawing.Size(189, 381);
            this.CDCUsers.TabIndex = 0;
            this.CDCUsers.SelectedIndexChanged += new System.EventHandler(this.CDCUsers_SelectedIndexChanged);
            // 
            // CDCUserMng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 437);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = this.Size;
            this.MinimizeBox = false;
            this.MinimumSize = this.Size;
            this.Name = "CDCUserMng";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CDCUserMng";
            this.Load += new System.EventHandler(this.CDCUserMng_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox CDCUsers;
        private System.Windows.Forms.TextBox UserEmail;
        private System.Windows.Forms.RadioButton rbCategoryLead;
        private System.Windows.Forms.RadioButton rbSupplier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbGroupList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lbAssignedWSI;
        private System.Windows.Forms.ListBox lbAvailableWSI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lbAvailableCategory;
        private System.Windows.Forms.ListBox lbAssignedCategory;
        private System.Windows.Forms.Button pbAddCat;
        private System.Windows.Forms.Button pbRemoveCat;
        private System.Windows.Forms.Button pbAddWSI;
        private System.Windows.Forms.Button pbRemoveWSI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button pbAddGroup;
        private System.Windows.Forms.Button pbSetGroup;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtGroupDesc;
        private System.Windows.Forms.TextBox txtGroupCode;
        private System.Windows.Forms.Button pbAddUpdate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button pbAddNewWSI;
        private System.Windows.Forms.TextBox txtAddWSI;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.CheckBox cbIsActiveUser;
        private System.Windows.Forms.Label lblGoupId;
        private System.Windows.Forms.RadioButton rbFTS;
    }
}