﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HAVITestProject;

namespace HAVITestHarness
{
    public partial class CDCUserMng : Form
    {
        GlobalLib GlobalLib = new GlobalLib();
        SOAPLib SOAPLib = new SOAPLib();
        
        string UserId = "";
        bool IsCSAActiveUser = false;
        string GroupId = "";
        string UserRole = "";
        public string CDCEnv = "";
        string PLUserId = "";
        string PLUserRoleId = "";

        string EmptyGroup = "< none >";
        public CDCUserMng()
        {
            InitializeComponent();            
        }
        private void CDCUserMng_Load(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            LoadUserList();
            LoadUserGroups();
            LoadAvailableWSI();
            rbFTS.Enabled = true;

            if (CDCEnv == "PROD")
            {
                SOAPLib.JDEEnv = "PROD";
                rbFTS.Enabled = false;
            }
            else SOAPLib.JDEEnv = "QA";

            this.Text = "CSA User Manager (" + CDCEnv + ")";
        }
        private void CDCUsers_SelectedIndexChanged(object sender, EventArgs e)
        {

            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            UserEmail.Text = CDCUsers.SelectedItem.ToString();
            UserId = "";
            GroupId = "";
            PLUserId = "";
            //groupBox1.Enabled = false;
            txtFirstName.Text = "";
            txtLastName.Text = "";

            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from CDCUser where UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);
            DataTable PLUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from ConnectUser where Email = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", "PL_" + CDCEnv);

            //string UserProdCategoryId = "";

            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                //UserProdCategoryId = dataRow["UserProdCategoryId"].ToString();
                UserId = dataRow["UserId"].ToString();
                Boolean.TryParse(dataRow["IsActive"].ToString(), out IsCSAActiveUser);
                cbIsActiveUser.Checked = IsCSAActiveUser;
                break;
            }
            foreach (DataRow dataRow in PLUserTable.Rows)
            {
                //UserProdCategoryId = dataRow["UserProdCategoryId"].ToString();
                PLUserId = dataRow["ConnectUserId"].ToString();
                txtFirstName.Text = dataRow["FirstName"].ToString();
                txtLastName.Text = dataRow["LastName"].ToString();
                break;
            }

            if (PLUserId != "")
            {
                LoadPlatformUser(PLUserId);
            }

            LoadUserRole();

            LoadUserGroups();

            lbGroupList.Enabled = true;
            groupBox2.Enabled = true;

            SelectUserGroups();
            LoadAvailableWSI();
            LoadAssignedWSI();

            LoadAssignedCategory();
        }
        private void LoadPlatformUser(string ConnectUserId)
        {

        }
        private void LoadUserList()
        {
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select UserUpn from CDCUser", CDCEnv);
            CDCUsers.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                CDCUsers.Items.Add(dataRow["UserUpn"].ToString());
                
            }
            
        }

        private void LoadUserRole()
        {

            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from CDCUserRole ur, CDCUser u where ur.UserId = u.UserId and UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);
            
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                UserRole = dataRow["RoleId"].ToString();

                if (UserRole == "1")
                {
                    rbSupplier.PerformClick();
                }
                if (UserRole == "2")
                {
                    rbCategoryLead.PerformClick();
                }
                if (UserRole == "3")
                {
                    rbFTS.PerformClick();
                }
            }

        }

        private void LoadUserGroups()
        {
            lblGoupId.Text = "0";
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select SiteGroupCode from SiteGroup", CDCEnv);
            lbGroupList.Items.Clear();
            lbGroupList.Items.Add(EmptyGroup);
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbGroupList.Items.Add(dataRow["SiteGroupCode"].ToString());
            }

        }

        private void SelectUserGroups()
        {
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select ug.SiteGroupId, sg.SiteGroupCode from CDCUserSiteGroup ug, CDCUser u, SiteGroup sg 
                                                            where ug.UserId = u.UserId and ug.SiteGroupId = sg.SiteGroupId and u.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);

            if(CDCUserTable.Rows.Count > 0)
            {
                foreach (DataRow dataRow in CDCUserTable.Rows)
                {
                    
                    lbGroupList.SelectedItem = dataRow["SiteGroupCode"].ToString();
                    GroupId = dataRow["SiteGroupId"].ToString();
                    lblGoupId.Text = GroupId.ToString();
                    break;
                }
            }
            else
            {
                lbGroupList.SelectedItem = EmptyGroup;
                lbGroupList.Enabled = false;
                groupBox2.Enabled = false;
            }

            
        }

        private void LoadAvailableWSI()
        {
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from SupplierSite", CDCEnv);
            lbAvailableWSI.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbAvailableWSI.Items.Add(dataRow["WsiId"].ToString());
            }

        }

        private void LoadAssignedWSI()
        {
            DataTable CDCUserTable;
            DataTable JDEUserTable = new DataTable("JDEItems"); ;
            DataTable TempUserTable;
            

            if (lbGroupList.Enabled && GroupId != "")
            {
                //CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select u.UserID, sgss.WsiId, u.UserUpn, pc.CategoryId, pc.CategoryCode, pc.CategoryDesc, f.RoleName
                //                                                            from CDCUser u, CDCUserSiteGroup usg, SiteGroupSupplierSite sgss, ProductCategory pc, UserProductCategory upc, CDCRole f, CDCUserRole e
                //                                                            where u.UserID = usg.UserID and usg.SiteGroupId = sgss.SiteGroupId and usg.UserID=upc.UserID 
                //                                                            and pc.CategoryId = upc.CategoryId  and u.UserID=e.UserID and f.RoleId = e.RoleId
                //                                                            and u.UserUpn = '" + UserEmail.Text.ToString() + "'", CDCEnv);
                //CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from SiteGroupSupplierSite a, SiteGroup b, CDCUserSiteGroup c, CDCUser d where
                //                                                   a.SiteGroupId = b.SiteGroupId and a.SiteGroupId = c.SiteGroupId and c.UserId = d.UserId and d.UserUpn = '" + UserEmail.Text.ToString() + "'", CDCEnv);
                CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from SiteGroupSupplierSite a, SiteGroup b where
                                                                    a.SiteGroupId = b.SiteGroupId and a.SiteGroupId = " + GroupId.ToString(), CDCEnv);
            }
            else
            {
                //CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select a.UserID, a.WsiId, b.UserUpn, c.CategoryId, c.CategoryCode, c.CategoryDesc, f.RoleName
                //                                                            from CdcUserSupplierSite a, CDCUser b, ProductCategory c, UserProductCategory d, CDCRole f, CDCUserRole e
                //                                                            where a.UserID=b.UserID and a.UserID=d.UserID and c.CategoryId = d.CategoryId and a.UserID=e.UserID and f.RoleId = e.RoleId
                //                                                            and b.UserUpn = '" + UserEmail.Text.ToString() + "'", CDCEnv);
                CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from CdcUserSupplierSite a, CDCUser b where a.UserID=b.UserID and b.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);
                lbGroupList.Enabled = false;
                groupBox2.Enabled = false;
            }
            var ListWSI = (from DataRow dRow in CDCUserTable.Rows
                           select dRow["WsiId"]);
            lbAssignedWSI.Items.Clear();
            int WSICount = 0;
            
            foreach (string dataRow in ListWSI)
            {
                lbAssignedWSI.Items.Add(dataRow.ToString());
                lbAvailableWSI.Items.Remove(dataRow.ToString());
                TempUserTable = SOAPLib.GetSupplierLineOfSupply(Convert.ToInt32(dataRow));

                JDEUserTable.Merge(TempUserTable);
                WSICount++;
            }

            if (WSICount == 0)
            {
                lbGroupList.Enabled = true;
                groupBox2.Enabled = true;
            }
            
            CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from UserProductCategory a, CDCUser b, ProductCategory c where a.UserID=b.UserID and a.CategoryId = c.CategoryId
                                                                and b.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);
            var JDECatList = (from DataRow dRow in JDEUserTable.Rows
                           select dRow["itemCategoryCode8"]).Distinct();
            int count = 0;
            string SQLCat = "";
            foreach (string dataRow in JDECatList)
            {
                string cat = dataRow.ToString().Trim();

                if (count == 0) SQLCat = "'" + cat + "'";
                else SQLCat = SQLCat + ", '" + cat + "'";
                count++;
            }
            CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select* from ProductCategory where CategoryCode in (" + SQLCat + ")", CDCEnv);
            lbAvailableCategory.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbAvailableCategory.Items.Add(dataRow["CategoryCode"].ToString().Trim());
            }
            groupBox1.Enabled = true;
        }

        private void LoadAssignedCategory()
        {
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from UserProductCategory a, CDCUser b, ProductCategory c where a.UserID=b.UserID and a.CategoryId = c.CategoryId and b.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);
            lbAssignedCategory.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbAssignedCategory.Items.Add(dataRow["CategoryCode"].ToString());
                //lbAssignedCategory.Items.Add(dataRow["CategoryCode"].ToString());
            }

        }

        private void lbAvailableCategory_MouseDoubleClick(object sender, EventArgs e)
        {
            AddCategory();
        }

        private void lbAssignedCategory_MouseDoubleClick(object sender, EventArgs e)
        {
            RemoveCat();
        }
        private void pbRemoveCat_Click(object sender, EventArgs e)
        {
            RemoveCat();

        }
        private void RemoveCat()
        {
            if (lbAssignedCategory.SelectedItem == null) return;
            string SelectedCat = lbAssignedCategory.SelectedItem.ToString();
            if (SelectedCat == "")
            {
                MessageBox.Show("Select Category");
                return;
            }
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from UserProductCategory a, CDCUser b, ProductCategory c where a.UserID=b.UserID and a.CategoryId = c.CategoryId
                                                                and b.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "' and CategoryCode = '" + SelectedCat.ToString() + "'", CDCEnv);
            string UserProdCategoryId = "";
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                UserProdCategoryId = dataRow["UserProdCategoryId"].ToString();
                break;
            }
            if (UserProdCategoryId != "")
            {
                CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM UserProductCategory WHERE UserProdCategoryId = " + UserProdCategoryId.ToString(), CDCEnv);
            }

            LoadAssignedCategory();

        }
        private void pbAddCat_Click(object sender, EventArgs e)
        {
            AddCategory();
        }
        private void AddCategory()
        {
            if (lbAvailableCategory.SelectedItem == null) return;
            string SelectedCat = lbAvailableCategory.SelectedItem.ToString();
            if (lbAssignedCategory.Items.Contains(SelectedCat)) return;
            if (SelectedCat == "")
            {
                MessageBox.Show("Select Category");
                return;
            }

            if (SelectedCat != "" && UserId != "")
            {
                DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO UserProductCategory (UserId, CategoryId)
                                                                    select " + UserId.ToString() + ", CategoryId from ProductCategory where CategoryCode = '" + SelectedCat.ToString() + "'", CDCEnv);
            }

            LoadAssignedCategory();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            AddWSI();
        }
        private void lbAvailableWSI_MouseDoubleClick(object sender, EventArgs e)
        {
            AddWSI();
        }
        private void lbAssignedWSI_MouseDoubleClick(object sender, EventArgs e)
        {
            RemoveWSI();
        }
        private void AddWSI()
        {
            //groupBox1.Enabled = false;

            if (lbAvailableWSI.SelectedItem == null) return;
            string SelectedWSI = lbAvailableWSI.SelectedItem.ToString();
            if (lbAssignedWSI.Items.Contains(SelectedWSI)) return;
            if (SelectedWSI == "")
            {
                MessageBox.Show("Select WSI");
                return;
            }

            if (SelectedWSI != "" && UserId != "")
            {
                if (lbGroupList.Enabled && GroupId != "")
                {

                    DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO SiteGroupSupplierSite (SiteGroupId, WsiId)
                                                                                    VALUES (" + GroupId.ToString() + ", " + SelectedWSI.ToString() + ")", CDCEnv);
                }
                else
                {
                    string UserGroupId = "";
                    DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select ug.SiteGroupId, sg.SiteGroupCode from CDCUserSiteGroup ug, CDCUser u, SiteGroup sg 
                                                        where ug.UserId = u.UserId and ug.SiteGroupId = sg.SiteGroupId and u.UserUpn = '" + UserEmail.Text.ToString().Replace("'","''") + "'", CDCEnv);

                    if (CDCUserTable.Rows.Count > 0)
                    {
                        foreach (DataRow dataRow in CDCUserTable.Rows)
                        {

                            UserGroupId = dataRow["SiteGroupId"].ToString();
                            break;
                        }
                    }

                    if (UserGroupId == "")
                    {
                        GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CdcUserSupplierSite (UserId, WsiId) VALUES (" + UserId.ToString() + ", " + SelectedWSI.ToString() + ")", CDCEnv);
                    }

                }

                LoadAssignedWSI();
            }
        }
        private void pbRemoveWSI_Click(object sender, EventArgs e)
        {
            RemoveWSI();
        }
        private void RemoveWSI()
        {
            //groupBox1.Enabled = false;
            if (lbAssignedWSI.SelectedItem == null) return;
            string SelectedWSI = lbAssignedWSI.SelectedItem.ToString();
            if (SelectedWSI == "")
            {
                MessageBox.Show("Select WSI");
                return;
            }

            if (SelectedWSI != "")
            {
                //CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM SiteGroupSupplierSiteId WHERE UserProdCategoryId = " + UserProdCategoryId.ToString(), CDCEnv);
                if (lbGroupList.Enabled && GroupId != "")
                {

                    DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM SiteGroupSupplierSite 
                                                                                    where SiteGroupId = " + GroupId.ToString() + " and WsiId = " + SelectedWSI.ToString(), CDCEnv);
                }
                else
                {
                    DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM CdcUserSupplierSite 
                                                                                where UserId = " + UserId.ToString() + " and WsiId = " + SelectedWSI.ToString(), CDCEnv);

                }
            }
            LoadAvailableWSI();
            LoadAssignedWSI();
        }
        private void lbGroupList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //groupBox1.Enabled = false;
            if (lbGroupList.SelectedItem.ToString() == EmptyGroup)
            {
                GroupId = "";

            }
            else
            {
                DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select SiteGroupId, SiteGroupCode from SiteGroup where SiteGroupCode = '" + lbGroupList.SelectedItem.ToString() + "'", CDCEnv);
            
                foreach (DataRow dataRow in CDCUserTable.Rows)
                {
                    GroupId = dataRow["SiteGroupId"].ToString();
                    break;
                }
            }
            lblGoupId.Text = GroupId.ToString();

            LoadAvailableWSI();
            LoadAssignedWSI();
        }

        private void rbSupplier_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbCategoryLead_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void pbAddGroup_Click(object sender, EventArgs e)
        {
            string strGroupCode = txtGroupCode.Text.ToString().Trim();
            string strGroupDesc = txtGroupDesc.Text.ToString().Trim();

            if (strGroupCode == "" || strGroupDesc == "") return;

            GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO SiteGroup (SiteGroupCode, SiteGroupDesc) VALUES ('" + strGroupCode + "', '" + strGroupDesc + "')", CDCEnv);


            LoadUserGroups();
            txtGroupCode.Text = "";
            txtGroupDesc.Text = "";

            lbGroupList.SelectedItem = EmptyGroup;
            lbGroupList.Enabled = true;
            groupBox2.Enabled = true;



        }

        private void pbSetGroup_Click(object sender, EventArgs e)
        {

            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select ug.UserSiteGroupId, ug.SiteGroupId, sg.SiteGroupCode from CDCUserSiteGroup ug, CDCUser u, SiteGroup sg 
                                                                where ug.UserId = u.UserId and ug.SiteGroupId = sg.SiteGroupId and u.UserUpn = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", CDCEnv);

            if (UserId != "")
            {
                if (CDCUserTable.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in CDCUserTable.Rows)
                    {

                        string RemoveGroupId = dataRow["UserSiteGroupId"].ToString();
                        GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM CDCUserSiteGroup  
                                                        where UserSiteGroupId = " + RemoveGroupId.ToString(), CDCEnv);

                        //break;
                    }
                }
                if (GroupId != "")
                {
                    GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CDCUserSiteGroup (UserId, SiteGroupId) 
                                                     VALUES (" + UserId.ToString() + ", " + GroupId.ToString() + ")", CDCEnv);
                }
                

                MessageBox.Show("Group is updated");
            }
            
        }
        private void txtAddWSI_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode.Equals(Keys.Enter))
            {
                pbAddNewWSI.PerformClick();
            }
        }
        private void pbAddNewWSI_Click(object sender, EventArgs e)
        {
            string strNewWSI = txtAddWSI.Text.Trim();
            if (strNewWSI == "") return;

            DataTable TempUserTable = SOAPLib.GetSupplierLineOfSupply(Convert.ToInt32(strNewWSI));

            var JDESupplierNameList = (from DataRow dRow in TempUserTable.Rows
                                       select dRow["supplierName"]).Distinct();
            string supplierName = "";

            foreach (string dataRow in JDESupplierNameList)
            {
                supplierName = dataRow.ToString().Trim();
                break;
            }

            if (lbAvailableWSI.Items.Contains(strNewWSI))
            {
                lbAvailableWSI.SelectedItem = strNewWSI.ToString();
                txtAddWSI.Text = "";
                MessageBox.Show("WSI#: " + strNewWSI + " - " + supplierName + " selected", "Select WSI");
                return;
            }
            else if(lbAssignedWSI.Items.Contains(strNewWSI))
            {
                lbAssignedWSI.SelectedItem = strNewWSI.ToString();
                MessageBox.Show("WSI#: " + strNewWSI + " - " + supplierName + " selected", "Select WSI");
                txtAddWSI.Text = "";
                return;
            }


            if(supplierName.Trim() == "")
            {
                MessageBox.Show("Invalid WSI or WSI not found in JDE");
                return;
            }

            if (MessageBox.Show("Add WSI#: " + strNewWSI + " - " + supplierName + "?", "New WSI", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO SupplierSite (WsiId, WsiName) 
                                                     VALUES (" + strNewWSI.ToString() + ", '" + supplierName.ToString() + "')", CDCEnv);

                txtAddWSI.Text = "";

                LoadAvailableWSI();

                lbAvailableWSI.SelectedItem = strNewWSI.ToString();
            }
        }

        private void pbAddUpdate_Click(object sender, EventArgs e)
        {
            string strUserEmail = UserEmail.Text.Trim();
            if (strUserEmail == "") return;
            string NewUserRole = "";
            string NewSuplyerRole = "";
            string IdentityProviderId = "";
            string SQL = "";

            if (rbSupplier.Checked) { NewUserRole = "1"; NewSuplyerRole = "Supplier"; }
            else if (rbCategoryLead.Checked) { NewUserRole = "2"; NewSuplyerRole = "Category Lead"; }
            else if (rbFTS.Checked) { NewUserRole = "3"; NewSuplyerRole = "Financial Transaction Services"; }
            IsCSAActiveUser = cbIsActiveUser.Checked;
            PLUserId = GlobalLib.CDCQuerySingle(@"select ConnectUserId from ConnectUser where Email = '" + UserEmail.Text.ToString().Replace("'","''") + "'", "PL_" + CDCEnv);
                        

            string FirstName = txtFirstName.Text.Trim();
            string LastName = txtLastName.Text.Trim();
            int IsActiveFlag = 0;
            if (IsCSAActiveUser) IsActiveFlag = 1;

            if (FirstName == "")
            {
                MessageBox.Show("Enter First Name");
                return;
            }
            if (LastName == "")
            {
                MessageBox.Show("Enter Last Name");
                return;
            }

            if (CDCUsers.Items.Contains(strUserEmail))
            {
                if (UserId != "")
                {
                    //GlobalLib.CDCQueryTableToDataTable(@"DELETE FROM CDCUserRole where UserId = " + UserId.ToString(), CDCEnv);
                    DataTable UserRoleList = GlobalLib.CDCQueryTableToDataTable(@"SELECT * FROM CDCUserRole WHERE UserId = " + UserId.ToString(), CDCEnv);

                    if (UserRoleList.Rows.Count <= 0)
                    {
                        GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CDCUserRole (UserId, RoleId) VALUES (" + UserId.ToString() + ", " + NewUserRole.ToString() + ")", CDCEnv);
                    }
                    else
                    {
                        GlobalLib.CDCQueryTableToDataTable(@"UPDATE CDCUserRole SET RoleId = " + NewUserRole.ToString() + " WHERE UserId = " + UserId.ToString(), CDCEnv);
                    }         
                    MessageBox.Show("User updated successfully");
                }
            }
            else if(strUserEmail != "")
            {
                if(NewUserRole == "")
                {
                    MessageBox.Show("Please select role");
                    return;
                }

                GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CDCUser (UserUpn, IsActive) VALUES ('" + strUserEmail.ToString().Replace("'", "''") + "', " + IsActiveFlag + ")", CDCEnv);
                DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from CDCUser where UserUpn = '" + strUserEmail.ToString().Replace("'","''") + "'", CDCEnv);
                
                foreach (DataRow dataRow in CDCUserTable.Rows)
                {
                    UserId = dataRow["UserId"].ToString();
                    //IsCSAActiveUser = dataRow["IsActive"];
                    break;
                }
                if (UserId == "") return;

                GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CDCUserRole (UserId, RoleId) VALUES (" + UserId.ToString() + ", " + NewUserRole.ToString() + ")", CDCEnv);
               
                if (PLUserId != "")
                {
                    GlobalLib.CDCQueryTableToDataTable(@"UPDATE ConnectUser 
                                    SET FirstName = '" + FirstName.ToString().Replace("'", "''") + "', LastName = '" + LastName.ToString().Replace("'", "''") + "' " +
                                    "WHERE UserId = " + UserId.ToString(), "PL_" + CDCEnv);
                }
                else
                {
                    //                        
                    GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[ConnectUser]([ConnectUserId],[FirstName],[LastName],[Email]) 
                                    VALUES(newid(), '" + FirstName.Trim().Replace("'", "''") + "','" + LastName.Trim().Replace("'", "''").Replace("'", "''") + "','" + strUserEmail.Trim().Replace("'", "''") + "')", "PL_" + CDCEnv);

                    DataTable PLUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from ConnectUser where Email = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", "PL_" + CDCEnv);
                    foreach (DataRow dataRow in PLUserTable.Rows)
                    {
                        //UserProdCategoryId = dataRow["UserProdCategoryId"].ToString();
                        PLUserId = dataRow["ConnectUserId"].ToString();
                        txtFirstName.Text = dataRow["FirstName"].ToString();
                        txtLastName.Text = dataRow["LastName"].ToString();
                        break;
                    }
                    if (PLUserId != "")
                    {
                        IdentityProviderId = GlobalLib.CDCQuerySingle("select IdentityProviderId from [dbo].[IdentityProvider] where ProviderName = 'Havi Global'", "PL_" + CDCEnv);

                        GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[UserIdentityProvider] ([UserIdentityProviderId],[IdentityProviderId],[ConnectUserId],[ProviderKey]) 
                                    VALUES(newid(), '" + IdentityProviderId.ToUpper() + "','" + PLUserId.ToUpper() + "', null)", "PL_" + CDCEnv);

                    }
                }
                               

                MessageBox.Show("New user has been created");
            }
            string ScopeType = "1";
            if (NewUserRole == "1")
            {
                ScopeType = GlobalLib.CDCQuerySingle("select ScopeTypeId from[dbo].[ScopeType] where ScopeKey = 'SL'", "PL_" + CDCEnv);
            }
            else if (NewUserRole == "2")
            {
                ScopeType = GlobalLib.CDCQuerySingle("select ScopeTypeId from[dbo].[ScopeType] where ScopeKey = 'OR'", "PL_" + CDCEnv);
            }
            else if (NewUserRole == "3")
            {
                ScopeType = GlobalLib.CDCQuerySingle("select ScopeTypeId from[dbo].[ScopeType] where ScopeKey = 'OR'", "PL_" + CDCEnv);
            }
            if (PLUserId != "")
            {
                SQL = @"UPDATE ConnectUser 
                                    SET FirstName = '" + FirstName.ToString().Replace("'", "''") + "', LastName = '" + LastName.ToString().Replace("'", "''") + "' " +
                                "WHERE ConnectUserId = '" + PLUserId.ToString().ToUpper() + "'";
                GlobalLib.CDCQueryTableToDataTable(SQL, "PL_" + CDCEnv);
                                
            }
            else
            {

                GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[ConnectUser]([ConnectUserId],[FirstName],[LastName],[Email]) 
                                    VALUES(newid(), '" + FirstName.Trim().Replace("'", "''") + "','" + LastName.Trim().Replace("'", "''") + "','" + strUserEmail.Trim().Replace("'", "''") + "')", "PL_" + CDCEnv);

                DataTable PLUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from ConnectUser where Email = '" + UserEmail.Text.ToString().Replace("'", "''") + "'", "PL_" + CDCEnv);
                foreach (DataRow dataRow in PLUserTable.Rows)
                {
                    PLUserId = dataRow["ConnectUserId"].ToString();
                    txtFirstName.Text = dataRow["FirstName"].ToString();
                    txtLastName.Text = dataRow["LastName"].ToString();
                    break;
                }
                if (PLUserId != "")
                {
                    IdentityProviderId = GlobalLib.CDCQuerySingle("select IdentityProviderId from [dbo].[IdentityProvider] where ProviderName = 'Havi Global'", "PL_" + CDCEnv);

                    GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[UserIdentityProvider] ([UserIdentityProviderId],[IdentityProviderId],[ConnectUserId],[ProviderKey]) 
                                    VALUES(newid(), '" + IdentityProviderId.ToUpper() + "','" + PLUserId.ToUpper() + "', null)", "PL_" + CDCEnv);
                    

                    GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[UserScopeType]([UserScopeTypeId],[ConnectUserId],[ScopeTypeId]) 
                                    VALUES(newid(), '" + PLUserId.Trim().ToUpper() + "','" + ScopeType.Trim().ToUpper() + "')", "PL_" + CDCEnv);
                }

            }
            string UserScopeTypeId = GlobalLib.CDCQuerySingle("SELECT UserScopeTypeId FROM [dbo].[UserScopeType] WHERE [ConnectUserId] = '" + PLUserId.Trim().ToUpper() + "'", "PL_" + CDCEnv).ToUpper();
            if(UserScopeTypeId != "")
            {
                GlobalLib.CDCQueryTableToDataTable(@"UPDATE UserScopeType SET ScopeTypeId = '" + ScopeType.Trim().ToUpper() + "' " +
                                                    "WHERE [ConnectUserId] = '" + PLUserId.Trim().ToUpper() + "'", "PL_" + CDCEnv);
            }
            else
            {
                GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO[dbo].[UserScopeType]([UserScopeTypeId],[ConnectUserId],[ScopeTypeId]) 
                                    VALUES(newid(), '" + PLUserId.Trim().ToUpper() + "','" + ScopeType.Trim().ToUpper() + "')", "PL_" + CDCEnv);
            }

            string CSAModuleId = GlobalLib.CDCQuerySingle("SELECT ModuleId FROM Module WHERE ModuleCode in ('CDC', 'CSA')", "PL_" + CDCEnv).ToUpper();

            string SQLCount = @"select * from[dbo].[UserRole] where ConnectUserId = '" + PLUserId.Trim().ToUpper() + @"' 
                        and ConnectRoleId in (select ConnectRoleId from [dbo].[ConnectRole] where ModuleId = '" + CSAModuleId + "')";
            DataTable RoleCount = GlobalLib.CDCQueryTableToDataTable(SQLCount, "PL_" + CDCEnv);
            string OrganizationId = "";
            if (NewUserRole == "1")
            {
                PLUserRoleId = GlobalLib.CDCQuerySingle("select * from [dbo].[ConnectRole] where RoleCode = 'CSA_SL'", "PL_" + CDCEnv).ToUpper();
                OrganizationId = GlobalLib.CDCQuerySingle("select OrganizationId from [dbo].[Organization] where OrgCode = 'MCD'", "PL_" + CDCEnv).ToUpper();

            }
            else if (NewUserRole == "2")
            {
                PLUserRoleId = GlobalLib.CDCQuerySingle("select * from [dbo].[ConnectRole] where RoleCode = 'CSA_CL'", "PL_" + CDCEnv).ToUpper();
                OrganizationId = GlobalLib.CDCQuerySingle("select OrganizationId from [dbo].[Organization] where OrgCode = 'MCD'", "PL_" + CDCEnv).ToUpper();

            }
            else if (NewUserRole == "3")
            {
                PLUserRoleId = GlobalLib.CDCQuerySingle("select * from [dbo].[ConnectRole] where RoleCode = 'CSA_FTS'", "PL_" + CDCEnv).ToUpper();
                OrganizationId = GlobalLib.CDCQuerySingle("select OrganizationId from [dbo].[Organization] where OrgCode = 'MCD'", "PL_" + CDCEnv).ToUpper();

            }
            else PLUserRoleId = "";

            if (RoleCount.Rows.Count > 0)
            {
                GlobalLib.CDCQuerySingle(@"DELETE FROM [dbo].[UserRole] where ConnectUserId = '" + PLUserId.Trim().ToUpper() + "' and OrganizationId = '" + OrganizationId.Trim().ToUpper() + "' and ConnectRoleId in (select ConnectRoleId from [dbo].[ConnectRole] where ModuleId = '" + CSAModuleId + "')", "PL_" + CDCEnv);
            }
            
            RoleCount = GlobalLib.CDCQueryTableToDataTable(SQLCount, "PL_" + CDCEnv);
                    
            string SQL2 = @"INSERT INTO[dbo].[UserRole]([UserRoleId],[ConnectUserId],[ConnectRoleId],[OrganizationId])
                                    VALUES(newid(), '" + PLUserId.Trim().ToUpper() + "','" + PLUserRoleId.Trim().ToUpper() + "','" + OrganizationId.Trim().ToUpper() + "')";
            
            if (PLUserRoleId != "" && RoleCount.Rows.Count == 0) GlobalLib.CDCQuerySingle(SQL2, "PL_" + CDCEnv);

            //GlobalLib.CDCQueryTableToDataTable(@"UPDATE CDCUser SET ConnectUserId = '" + PLUserId.ToString() + "' WHERE UserId = " + UserId.ToString(), CDCEnv);
            //GlobalLib.CDCQueryTableToDataTable(@"INSERT INTO CDCUser (UserUpn, IsActive) VALUES ('" + strUserEmail.ToString() + "', " + IsCSAActiveUser + ")", CDCEnv);
            
            SQL = @"UPDATE CDCUser SET IsActive = " + IsActiveFlag + " " +
                                "WHERE UserUpn = '" + strUserEmail.ToString().Replace("'", "''") + "'";
            GlobalLib.CDCQueryTableToDataTable(SQL, CDCEnv);

            LoadUserList();
            CDCUsers.SelectedItem = strUserEmail.ToString();
           
        }

        
    }
}
