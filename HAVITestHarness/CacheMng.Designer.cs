﻿namespace HAVITestHarness
{
    partial class CacheMng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.lbCacheList = new System.Windows.Forms.ListBox();
            this.pbDeleteCache = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDeleteCache = new System.Windows.Forms.TextBox();
            this.lbAssWSI = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(315, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbCacheList
            // 
            this.lbCacheList.FormattingEnabled = true;
            this.lbCacheList.Location = new System.Drawing.Point(33, 48);
            this.lbCacheList.Name = "lbCacheList";
            this.lbCacheList.Size = new System.Drawing.Size(265, 355);
            this.lbCacheList.TabIndex = 1;
            this.lbCacheList.SelectedIndexChanged += new System.EventHandler(this.lbCacheList_SelectedIndexChanged);
            // 
            // pbDeleteCache
            // 
            this.pbDeleteCache.Location = new System.Drawing.Point(11, 19);
            this.pbDeleteCache.Name = "pbDeleteCache";
            this.pbDeleteCache.Size = new System.Drawing.Size(89, 23);
            this.pbDeleteCache.TabIndex = 2;
            this.pbDeleteCache.Text = "Delete Cache";
            this.pbDeleteCache.UseVisualStyleBackColor = true;
            this.pbDeleteCache.Click += new System.EventHandler(this.pbDeleteCache_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbDeleteCache);
            this.groupBox1.Controls.Add(this.pbDeleteCache);
            this.groupBox1.Location = new System.Drawing.Point(304, 303);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 100);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Delete Cache";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Password:";
            // 
            // tbDeleteCache
            // 
            this.tbDeleteCache.Location = new System.Drawing.Point(11, 69);
            this.tbDeleteCache.Name = "tbDeleteCache";
            this.tbDeleteCache.PasswordChar = '*';
            this.tbDeleteCache.Size = new System.Drawing.Size(89, 20);
            this.tbDeleteCache.TabIndex = 3;
            // 
            // lbAssWSI
            // 
            this.lbAssWSI.FormattingEnabled = true;
            this.lbAssWSI.Location = new System.Drawing.Point(315, 92);
            this.lbAssWSI.Name = "lbAssWSI";
            this.lbAssWSI.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbAssWSI.Size = new System.Drawing.Size(89, 173);
            this.lbAssWSI.TabIndex = 4;
            this.lbAssWSI.SelectedIndexChanged += new System.EventHandler(this.lbAssWSI_SelectedIndexChanged);
            // 
            // CacheMng
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 425);
            this.Controls.Add(this.lbAssWSI);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbCacheList);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(439, 464);
            this.MinimumSize = new System.Drawing.Size(439, 464);
            this.Name = "CacheMng";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Redis Cache Manager";
            this.Load += new System.EventHandler(this.CacheMng_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox lbCacheList;
        private System.Windows.Forms.Button pbDeleteCache;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDeleteCache;
        private System.Windows.Forms.ListBox lbAssWSI;
    }
}