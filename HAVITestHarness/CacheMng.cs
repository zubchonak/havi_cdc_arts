﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StackExchange.Redis;
using System.Configuration;
using HAVITestProject;

namespace HAVITestHarness
{
    public partial class CacheMng : Form
    {
        public string CDCEnv = "";
        public string DeletePass = Properties.Settings.Default.UserMngPsw;
        GlobalLib GlobalLib = new GlobalLib();

        public CacheMng()
        {

            InitializeComponent();
            
        }
        private void CacheMng_Load(object sender, EventArgs e)
        {
            this.Text = "CSA Cache Manager (" + CDCEnv + ")";

            if (CDCEnv == "") CDCEnv = "dev";
            if (CDCEnv == "DEMO") CDCEnv = "demo";
            if (CDCEnv == "PROD") CDCEnv = "prod";

            LoadCDCCache();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            LoadCDCCache();
        }
        private void pbDeleteCache_Click(object sender, EventArgs e)
        {
            string CacheToDelete = lbCacheList.SelectedItem.ToString();
            if (tbDeleteCache.Text.ToString() != DeletePass)
            {
                MessageBox.Show("Incorrect password.");
                return;
            }

            if (CacheToDelete == "")
                return;

            IDatabase cache = ConnectionDev.GetDatabase();
            if (CDCEnv == "dev") cache = ConnectionDev.GetDatabase();
            if (CDCEnv == "demo") cache = ConnectionDemo.GetDatabase();
            if (CDCEnv == "prod") cache = ConnectionProd.GetDatabase();
            else cache = ConnectionDev.GetDatabase();
            var aCacheName = CacheToDelete.ToString().Trim().Split(' ');
            if (MessageBox.Show("DELETE: " + aCacheName[0].ToString().Trim() + "\r\n\r\nAre you sure?", "WARNING!!! CDC Cache Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //cache.KeyTimeToLive
                cache.KeyDelete(aCacheName[0].ToString().Trim());

                LoadCDCCache();
            }
        }
        
        private void LoadCDCCache()
        {

            IServer server;
            if (CDCEnv == "dev") server = ConnectionDev.GetServer("cdc-dev.redis.cache.windows.net", 6380);
            if (CDCEnv == "demo") server = ConnectionDemo.GetServer("cdc-demo.redis.cache.windows.net", 6380);
            if (CDCEnv == "prod") server = ConnectionProd.GetServer("cdc-prod.redis.cache.windows.net", 6380);
            else server = ConnectionDev.GetServer("cdc-dev.redis.cache.windows.net", 6380);

            lbCacheList.Items.Clear();
            string TimeToLive = "";
            int counter = 0;
            foreach (var key in server.Keys())
            {

                if (key.ToString().Contains("WSI") && key.ToString().Contains(CDCEnv.ToLower()))
                {
                    TimeToLive = GetKeyTimeToLive(key.ToString());
                    lbCacheList.Items.Add(String.Format("{0} ({1})", key.ToString(), TimeToLive));
                    //lbCacheList.Items.Add(String.Format("{0}", key.ToString()));
                    counter++;
                }
            }
            if(counter > 0) lbCacheList.SelectedIndex = 0;
        }
        private string GetKeyTimeToLive(string txtKey)
        {
            IDatabase cache = ConnectionDev.GetDatabase();
            if (CDCEnv == "dev") cache = ConnectionDev.GetDatabase();
            if (CDCEnv == "demo") cache = ConnectionDemo.GetDatabase();
            if (CDCEnv == "prod") cache = ConnectionProd.GetDatabase();

            string KeyTime = String.Format("{0:hh\\:mm\\:ss}", cache.KeyTimeToLive(txtKey));
         

            return (KeyTime);

        }
        private static Lazy<ConnectionMultiplexer> lazyConnectionDev = new Lazy<ConnectionMultiplexer>(() =>
        {
            ConfigurationOptions config = new ConfigurationOptions();
            //config.EndPoints.Add(ConfigurationManager.AppSettings["RedisCacheName"]);
            config.EndPoints.Add("cdc-dev.redis.cache.windows.net:6380");
            config.Password = "Koy8qmIfFXGFcgtyZdZs7HYRwKtRM2LlIph8h/CO39s="; //ConfigurationManager.AppSettings["RedisCachePassword"];
            config.Ssl = true;
            config.AbortOnConnectFail = false;
            config.ConnectRetry = 5;
            config.ConnectTimeout = 1000;
            return ConnectionMultiplexer.Connect(config);
        });
        private static Lazy<ConnectionMultiplexer> lazyConnectionDemo = new Lazy<ConnectionMultiplexer>(() =>
        {
            ConfigurationOptions config = new ConfigurationOptions();
            //config.EndPoints.Add(ConfigurationManager.AppSettings["RedisCacheName"]);
            config.EndPoints.Add("cdc-demo.redis.cache.windows.net:6380");
            config.Password = "/2ilt4+beTgQfivpEbQholRWc8aoaWpB/Xs4FRPDFYA="; //ConfigurationManager.AppSettings["RedisCachePassword"];
            config.Ssl = true;
            config.AbortOnConnectFail = false;
            config.ConnectRetry = 5;
            config.ConnectTimeout = 1000;
            return ConnectionMultiplexer.Connect(config);
        });
        private static Lazy<ConnectionMultiplexer> lazyConnectionProd = new Lazy<ConnectionMultiplexer>(() =>
        {
            ConfigurationOptions config = new ConfigurationOptions();
            //config.EndPoints.Add(ConfigurationManager.AppSettings["RedisCacheName"]);
            config.EndPoints.Add("cdc-prod.redis.cache.windows.net:6380");
            config.Password = "E7a+UbcNLVPjGspbCqEcLYrAnVmZOgGtbqmrfGiJPoI="; //ConfigurationManager.AppSettings["RedisCachePassword"];
            config.Ssl = true;
            config.AbortOnConnectFail = false;
            config.ConnectRetry = 5;
            config.ConnectTimeout = 1000;
            return ConnectionMultiplexer.Connect(config);
        });

        public static ConnectionMultiplexer ConnectionDev
        {
            get
            {
                return lazyConnectionDev.Value;
            }
        }
        public static ConnectionMultiplexer ConnectionDemo
        {
            get
            {
                return lazyConnectionDemo.Value;
            }
        }

        public static ConnectionMultiplexer ConnectionProd
        {
            get
            {
                return lazyConnectionProd.Value;
            }
        }
        private void lbCacheList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CacheToDelete = lbCacheList.SelectedItem.ToString();
            
            if (CacheToDelete == "")
                return;

            IDatabase cache = ConnectionDev.GetDatabase();
            var aCacheName = CacheToDelete.ToString().Trim().Split(' ');
            aCacheName = aCacheName[0].ToString().Trim().Split('_');
            string selectedWSI = aCacheName[aCacheName.Length - 1];

            string SQL = @"select distinct WsiId from (
                            select sgss.WsiId from [dbo].[SiteGroupSupplierSite] sgss, [dbo].[SiteGroup] sg
                            where sgss.SiteGroupId = sg.SiteGroupId  
                            and sgss.SiteGroupId in (select distinct sgss.SiteGroupId from [dbo].[SiteGroupSupplierSite] sgss where wsiid = '" + selectedWSI + @"')
                            union all
                            select sgss.WsiId from [dbo].[CdcUserSupplierSite] sgss
                            where sgss.UserId in (select sgss.UserId from [dbo].[CdcUserSupplierSite] sgss
                            where sgss.WsiId in (select sgss.WsiId from [dbo].[SiteGroupSupplierSite] sgss, [dbo].[SiteGroup] sg
                            where sgss.SiteGroupId = sg.SiteGroupId  
                            and sgss.SiteGroupId in (select distinct sgss.SiteGroupId from [dbo].[SiteGroupSupplierSite] sgss where wsiid = '" + selectedWSI + @"')))union all select '" + selectedWSI + @"') WsiList";
            DataTable AssWSI = new DataTable();
            String WSIList = "'" + selectedWSI + "'";
            int counter = 0;
            int lastCount = 0;
            do {
                lastCount = counter;
                SQL = @"select distinct sgss.WsiId from [dbo].[SiteGroupSupplierSite] sgss, [dbo].[SiteGroup] sg
                        where sgss.SiteGroupId = sg.SiteGroupId  
                        and sgss.SiteGroupId in (select distinct sgss.SiteGroupId from [dbo].[SiteGroupSupplierSite] sgss where wsiid in (" + WSIList + @"))";
                WSIList = "'" + selectedWSI + "'";
                DataTable tmpAssWSI = GlobalLib.CDCQueryTableToDataTable(SQL, CDCEnv);
                counter = tmpAssWSI.Rows.Count;
                AssWSI.Merge(tmpAssWSI);
                foreach (DataRow dataRow in tmpAssWSI.Rows)
                {
                    WSIList = WSIList + ", '" + dataRow["WsiId"].ToString() + "'";
                }

                SQL = @"select distinct sgss.WsiId from [dbo].[CdcUserSupplierSite] sgss
                        where sgss.UserId in (select sgss.UserId from [dbo].[CdcUserSupplierSite] sgss
                        where sgss.WsiId in (" + WSIList + "))";
                tmpAssWSI = GlobalLib.CDCQueryTableToDataTable(SQL, CDCEnv);

                AssWSI.Merge(tmpAssWSI);
                AssWSI =  (from DataRow dRow in AssWSI.Rows
                          select dRow).Distinct(DataRowComparer.Default).CopyToDataTable();

                DataTable OutAssWSI = AssWSI.AsEnumerable().Distinct(DataRowComparer.Default).CopyToDataTable();

                counter = OutAssWSI.Rows.Count;

                foreach (DataRow dataRow in OutAssWSI.Rows)
                {
                    WSIList = WSIList + ", '" + dataRow["WsiId"].ToString() + "'";
                }

                //counter = counter;
            } while (lastCount != counter);
            lbAssWSI.Items.Clear();
            foreach (DataRow dataRow in AssWSI.Rows) lbAssWSI.Items.Add(dataRow["WsiId"].ToString());

        }

        private void lbAssWSI_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
