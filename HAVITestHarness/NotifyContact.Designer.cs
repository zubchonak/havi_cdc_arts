﻿namespace HAVITestHarness
{
    partial class NotifyContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbUsers = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbIsActive = new System.Windows.Forms.CheckBox();
            this.pbAddUpdate = new System.Windows.Forms.Button();
            this.rbAlways = new System.Windows.Forms.RadioButton();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.rbSiteGroup = new System.Windows.Forms.RadioButton();
            this.rbSupplierSite = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbListToSelect = new System.Windows.Forms.ListBox();
            this.lblListToSelect = new System.Windows.Forms.Label();
            this.pbReset = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbReset);
            this.groupBox1.Controls.Add(this.lblListToSelect);
            this.groupBox1.Controls.Add(this.lbListToSelect);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.pbAddUpdate);
            this.groupBox1.Controls.Add(this.cbIsActive);
            this.groupBox1.Location = new System.Drawing.Point(193, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 201);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Notify Contact";
            // 
            // lbUsers
            // 
            this.lbUsers.FormattingEnabled = true;
            this.lbUsers.Location = new System.Drawing.Point(13, 25);
            this.lbUsers.Name = "lbUsers";
            this.lbUsers.Size = new System.Drawing.Size(174, 186);
            this.lbUsers.TabIndex = 1;
            this.lbUsers.SelectedIndexChanged += new System.EventHandler(this.lbUsers_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Contact Email";
            // 
            // cbIsActive
            // 
            this.cbIsActive.AutoSize = true;
            this.cbIsActive.Location = new System.Drawing.Point(6, 19);
            this.cbIsActive.Name = "cbIsActive";
            this.cbIsActive.Size = new System.Drawing.Size(56, 17);
            this.cbIsActive.TabIndex = 0;
            this.cbIsActive.Text = "Active";
            this.cbIsActive.UseVisualStyleBackColor = true;
            // 
            // pbAddUpdate
            // 
            this.pbAddUpdate.Location = new System.Drawing.Point(242, 168);
            this.pbAddUpdate.Name = "pbAddUpdate";
            this.pbAddUpdate.Size = new System.Drawing.Size(62, 23);
            this.pbAddUpdate.TabIndex = 1;
            this.pbAddUpdate.Text = "Add";
            this.pbAddUpdate.UseVisualStyleBackColor = true;
            this.pbAddUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbAlways
            // 
            this.rbAlways.AutoSize = true;
            this.rbAlways.Location = new System.Drawing.Point(13, 3);
            this.rbAlways.Name = "rbAlways";
            this.rbAlways.Size = new System.Drawing.Size(58, 17);
            this.rbAlways.TabIndex = 2;
            this.rbAlways.Text = "Always";
            this.rbAlways.UseVisualStyleBackColor = true;
            this.rbAlways.CheckedChanged += new System.EventHandler(this.rbAlways_CheckedChanged);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(9, 55);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(169, 20);
            this.txtName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Email:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(9, 93);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(169, 20);
            this.txtEmail.TabIndex = 5;
            // 
            // rbSiteGroup
            // 
            this.rbSiteGroup.AutoSize = true;
            this.rbSiteGroup.Location = new System.Drawing.Point(13, 26);
            this.rbSiteGroup.Name = "rbSiteGroup";
            this.rbSiteGroup.Size = new System.Drawing.Size(75, 17);
            this.rbSiteGroup.TabIndex = 7;
            this.rbSiteGroup.Text = "Site Group";
            this.rbSiteGroup.UseVisualStyleBackColor = true;
            this.rbSiteGroup.CheckedChanged += new System.EventHandler(this.rbSiteGroup_CheckedChanged);
            // 
            // rbSupplierSite
            // 
            this.rbSupplierSite.AutoSize = true;
            this.rbSupplierSite.Location = new System.Drawing.Point(13, 49);
            this.rbSupplierSite.Name = "rbSupplierSite";
            this.rbSupplierSite.Size = new System.Drawing.Size(84, 17);
            this.rbSupplierSite.TabIndex = 8;
            this.rbSupplierSite.Text = "Supplier Site";
            this.rbSupplierSite.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.rbSupplierSite.UseVisualStyleBackColor = true;
            this.rbSupplierSite.CheckedChanged += new System.EventHandler(this.rbSupplierSite_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.rbAlways);
            this.panel1.Controls.Add(this.rbSupplierSite);
            this.panel1.Controls.Add(this.rbSiteGroup);
            this.panel1.Location = new System.Drawing.Point(9, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(169, 72);
            this.panel1.TabIndex = 9;
            // 
            // lbListToSelect
            // 
            this.lbListToSelect.Enabled = false;
            this.lbListToSelect.FormattingEnabled = true;
            this.lbListToSelect.Location = new System.Drawing.Point(184, 39);
            this.lbListToSelect.Name = "lbListToSelect";
            this.lbListToSelect.Size = new System.Drawing.Size(120, 121);
            this.lbListToSelect.TabIndex = 10;
            // 
            // lblListToSelect
            // 
            this.lblListToSelect.AutoSize = true;
            this.lblListToSelect.Location = new System.Drawing.Point(184, 20);
            this.lblListToSelect.Name = "lblListToSelect";
            this.lblListToSelect.Size = new System.Drawing.Size(28, 13);
            this.lblListToSelect.TabIndex = 11;
            this.lblListToSelect.Text = "WSI";
            // 
            // pbReset
            // 
            this.pbReset.Location = new System.Drawing.Point(184, 167);
            this.pbReset.Name = "pbReset";
            this.pbReset.Size = new System.Drawing.Size(52, 23);
            this.pbReset.TabIndex = 12;
            this.pbReset.Text = "Reset";
            this.pbReset.UseVisualStyleBackColor = true;
            this.pbReset.Click += new System.EventHandler(this.pbReset_Click);
            // 
            // NotifyContact
            // 
            this.ClientSize = new System.Drawing.Size(531, 218);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbUsers);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotifyContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.NotifyContact_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbUsers;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbAlways;
        private System.Windows.Forms.RadioButton rbSupplierSite;
        private System.Windows.Forms.RadioButton rbSiteGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button pbAddUpdate;
        private System.Windows.Forms.CheckBox cbIsActive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbListToSelect;
        private System.Windows.Forms.Label lblListToSelect;
        private System.Windows.Forms.Button pbReset;
    }
}