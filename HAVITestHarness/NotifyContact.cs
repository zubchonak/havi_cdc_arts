﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HAVITestProject;

namespace HAVITestHarness
{
    public partial class NotifyContact : Form
    {
        public string CDCEnv = "";
        GlobalLib GlobalLib = new GlobalLib();
        public NotifyContact()
        {
            InitializeComponent();
        }

        private void NotifyContact_Load(object sender, EventArgs e)
        {
            this.Text = "CSA Notify Contact (" + CDCEnv + ")";
            LoadUsersList();

        }


        private void LoadUsersList()
        {
            
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from NotifyContact", CDCEnv);
            lbUsers.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                string UserToList = dataRow["ContactEmail"].ToString();
                string UserIdToList = dataRow["NotifyContactId"].ToString();

                UserToList = String.Format("{0} ({1})", UserToList, UserIdToList);

                lbUsers.Items.Add(UserToList);
            }            
        }
        private void LoadUsersList(string SelectEmailId)
        {

            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from NotifyContact", CDCEnv);
            lbUsers.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                string UserToList = dataRow["ContactEmail"].ToString();
                string UserIdToList = dataRow["NotifyContactId"].ToString();

                UserToList = String.Format("{0} ({1})", UserToList, UserIdToList);

                lbUsers.Items.Add(UserToList);
                if (SelectEmailId == UserIdToList) lbUsers.SelectedItem = UserToList;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string IsActive = "0";
            string sSQL="";
            string UserGroupCode = "";
            if (cbIsActive.Checked == true) IsActive = "1";
            else if (cbIsActive.Checked == false) IsActive = "0";

            if (lbListToSelect.SelectedItem == null && !rbAlways.Checked)
            {
                MessageBox.Show("Select Group or WSI", "Error", MessageBoxButtons.OK);
                return;
            }
            
            string UserEmail = txtEmail.Text.ToString().Trim().Replace("'", "''").Replace("\r\n", "");
            string UserName = txtName.Text.ToString().Trim();


            if (UserEmail.ToString().Trim() == "")
            {
                MessageBox.Show("Enter Email", "Error", MessageBoxButtons.OK);
                return;
            }
            if (UserName.ToString().Trim() == "")
            {
                MessageBox.Show("Enter Name", "Error", MessageBoxButtons.OK);
                return;
            }

            string NotifyContactEntityType = "Always";
                       

            if (pbAddUpdate.Text == "Add")
            {
                sSQL = @"INSERT INTO[dbo].[NotifyContact]
                        ([ContactName]
                          ,[ContactEmail]
                          ,[SendEmailNotification]
                          ,[NotifyContactEntityType]
                          ,[WsiId]
                          ,[SiteGroupId])
                    VALUES
                          ('" + UserName.ToString() + @"'
                           ,'" + UserEmail.ToString() + @"'
                           ," + IsActive.ToString() + @"
                           ,'" + NotifyContactEntityType.ToString() + @"'
                           ,null
                           ,null)";
                string BeforeUserEmailId = GlobalLib.CDCQuerySingle(@"SELECT max ([NotifyContactId]) as Id FROM [dbo].[NotifyContact]", CDCEnv);

                GlobalLib.CDCQuerySingle(sSQL, CDCEnv);

                string UserEmailId = GlobalLib.CDCQuerySingle(@"SELECT max ([NotifyContactId]) as Id FROM [dbo].[NotifyContact]", CDCEnv);
                if (BeforeUserEmailId == UserEmailId) return;
                LoadUsersList(UserEmailId);
                pbAddUpdate.Text = "Update";
            }

            string SelectedEmail = lbUsers.SelectedItem.ToString();
            var aSelectedEmail = SelectedEmail.ToString().Trim().Split(' ');
            SelectedEmail = aSelectedEmail[0];
            string SelectedEmailId = aSelectedEmail[1].ToString().Replace("(", "").Replace(")", "").Trim();

            if (pbAddUpdate.Text == "Update" && lbUsers.SelectedIndex > -1)
            {
                if (rbAlways.Checked )
                {
                    NotifyContactEntityType = "Always";
                    sSQL = @"UPDATE [dbo].[NotifyContact]
                            SET [ContactName] = '" + txtName.Text.ToString().Trim() + @"'
                                ,[ContactEmail] = '" + UserEmail.ToString().Trim() + @"'
                                ,[SendEmailNotification] = " + IsActive.ToString() + @"
                                ,[NotifyContactEntityType] = '" + NotifyContactEntityType.ToString() + @"'
                                ,[WsiId] = null                                
                                ,[SiteGroupId] = null
                            WHERE [NotifyContactId] = " + SelectedEmailId;
                }
                else if (rbSiteGroup.Checked)
                {
                    rbSiteGroup.Checked = true;
                    NotifyContactEntityType = "SiteGroup";
                    UserGroupCode = lbListToSelect.SelectedItem.ToString();

                    string UserGroupId = GlobalLib.CDCQuerySingle(@"select SiteGroupId from SiteGroup where SiteGroupCode = '" + UserGroupCode.ToString() + "'", CDCEnv);

                    sSQL = @"UPDATE [dbo].[NotifyContact]
                            SET [ContactName] = '" + txtName.Text.ToString().Trim() + @"'
                                ,[ContactEmail] = '" + UserEmail.ToString().Trim() + @"'
                                ,[SendEmailNotification] = " + IsActive.ToString() + @"
                                ,[NotifyContactEntityType] = '" + NotifyContactEntityType.ToString() + @"'
                                ,[WsiId] = null                                
                                ,[SiteGroupId] = " + UserGroupId.ToString() + @"
                            WHERE [NotifyContactId] = " + SelectedEmailId;

                }
                else if (rbSupplierSite.Checked)
                {
                    UserGroupCode = lbListToSelect.SelectedItem.ToString();

                    NotifyContactEntityType = "SupplierSite";
                    sSQL = @"UPDATE [dbo].[NotifyContact]
                            SET [ContactName] = '" + txtName.Text.ToString().Trim() + @"'
                                ,[ContactEmail] = '" + UserEmail.ToString().Trim() + @"'
                                ,[SendEmailNotification] = " + IsActive.ToString() + @"
                                ,[NotifyContactEntityType] = '" + NotifyContactEntityType.ToString() + @"'
                                ,[WsiId] = " + UserGroupCode.ToString() + @"                                
                                ,[SiteGroupId] = null
                            WHERE [NotifyContactId] = " + SelectedEmailId;

                }
                else
                {
                    sSQL = @"UPDATE [dbo].[NotifyContact]
                            SET [ContactName] = '" + txtName.Text.ToString().Trim() + @"'
                                ,[ContactEmail] = '" + UserEmail.ToString().Trim() + @"'
                                ,[SendEmailNotification] = " + IsActive.ToString() + @"
                           WHERE [NotifyContactId] = " + SelectedEmailId;
                }
            }

            if(sSQL != "") GlobalLib.CDCQueryTableToDataTable(sSQL, CDCEnv);

            LoadUsersList(SelectedEmailId);
        }

        private void lbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadNotifyContact();
            pbAddUpdate.Text = "Update";
        }

        private void LoadNotifyContact()
        {
            string ContactName;
            string ContactEmail;
            bool SendEmailNotification;
            string NotifyContactEntityType;
            string WsiId;
            string SiteGroupId;
            string SelectedEmailId;
            if (lbUsers.SelectedIndex == -1) { return; }
            string SelectedEmail = lbUsers.SelectedItem.ToString();

            var aSelectedEmail = SelectedEmail.ToString().Trim().Split(' ');
            SelectedEmail = aSelectedEmail[0];
            SelectedEmailId = aSelectedEmail[1].ToString().Replace("(","").Replace(")","").Trim();

            txtEmail.Text = SelectedEmail;

            DataTable NotifyContactTable = GlobalLib.CDCQueryTableToDataTable(@"select * from NotifyContact where NotifyContactId = " + SelectedEmailId.ToString(), CDCEnv);
            foreach (DataRow dataRow in NotifyContactTable.Rows)
            {
                ContactName = dataRow["ContactName"].ToString();
                ContactEmail = dataRow["ContactEmail"].ToString();
                Boolean.TryParse(dataRow["SendEmailNotification"].ToString(), out SendEmailNotification);
                NotifyContactEntityType = dataRow["NotifyContactEntityType"].ToString();
                WsiId = dataRow["WsiId"].ToString();
                SiteGroupId = dataRow["SiteGroupId"].ToString();

                txtName.Text = ContactName;
                cbIsActive.Checked = SendEmailNotification;
                if (NotifyContactEntityType == "Always")
                {
                    rbAlways.Checked = true;
                }
                else if (NotifyContactEntityType == "SiteGroup")
                {
                    rbSiteGroup.Checked = true;
                    string UserGroupName = GlobalLib.CDCQuerySingle(@"select SiteGroupCode from SiteGroup where SiteGroupId = " + SiteGroupId.ToString(), CDCEnv);
                    lbListToSelect.SelectedItem = UserGroupName.Trim();
                }
                else if (NotifyContactEntityType == "SupplierSite")
                {
                    rbSupplierSite.Checked = true;
                    lbListToSelect.SelectedItem = WsiId.Trim();
                }
                else
                {
                    rbAlways.Checked = true;                    
                }
            }
        }

        private void rbAlways_CheckedChanged(object sender, EventArgs e)
        {
            lbListToSelect.Enabled = false;
            lblListToSelect.Text = "";
            lbListToSelect.Items.Clear();
        }

        private void rbSiteGroup_CheckedChanged(object sender, EventArgs e)
        {
            lbListToSelect.Enabled = true;
            lblListToSelect.Text = "Groups";
            LoadGroups();
        }

        private void rbSupplierSite_CheckedChanged(object sender, EventArgs e)
        {
            lbListToSelect.Enabled = true;
            lblListToSelect.Text = "WSIs";
            LoadWSI();
        }

        private void pbReset_Click(object sender, EventArgs e)
        {
            
            txtEmail.Text = "";
            txtName.Text = "";
            lbUsers.SelectedIndex = -1;
            rbAlways.Checked = true;
            cbIsActive.Checked = false;
            pbAddUpdate.Text = "Add";
        }

        private void LoadGroups()
        {

            //lblGoupId.Text = "0";
            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select SiteGroupCode from SiteGroup", CDCEnv);
            lbListToSelect.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbListToSelect.Items.Add(dataRow["SiteGroupCode"].ToString());
            }
        }
        private void LoadWSI()
        {

            DataTable CDCUserTable = GlobalLib.CDCQueryTableToDataTable(@"select * from SupplierSite", CDCEnv);
            lbListToSelect.Items.Clear();
            foreach (DataRow dataRow in CDCUserTable.Rows)
            {
                lbListToSelect.Items.Add(dataRow["WsiId"].ToString());
            }
        }
    }
}
