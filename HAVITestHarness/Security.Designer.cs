﻿namespace HAVITestHarness
{
    partial class frmSecurity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.gbPassword = new System.Windows.Forms.GroupBox();
            this.pbOK = new System.Windows.Forms.Button();
            this.gbPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(6, 19);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '8';
            this.tbPassword.Size = new System.Drawing.Size(119, 20);
            this.tbPassword.TabIndex = 0;
            this.tbPassword.UseSystemPasswordChar = true;
            this.tbPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPassword_KeyDown);
            // 
            // gbPassword
            // 
            this.gbPassword.Controls.Add(this.pbOK);
            this.gbPassword.Controls.Add(this.tbPassword);
            this.gbPassword.Location = new System.Drawing.Point(13, 13);
            this.gbPassword.Name = "gbPassword";
            this.gbPassword.Size = new System.Drawing.Size(218, 57);
            this.gbPassword.TabIndex = 1;
            this.gbPassword.TabStop = false;
            this.gbPassword.Text = "Password";
            // 
            // pbOK
            // 
            this.pbOK.Location = new System.Drawing.Point(132, 17);
            this.pbOK.Name = "pbOK";
            this.pbOK.Size = new System.Drawing.Size(75, 23);
            this.pbOK.TabIndex = 1;
            this.pbOK.Text = "OK";
            this.pbOK.UseVisualStyleBackColor = true;
            this.pbOK.Click += new System.EventHandler(this.pbOK_Click);
            // 
            // frmSecurity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 83);
            this.Controls.Add(this.gbPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSecurity";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Security";
            this.Load += new System.EventHandler(this.frmSecurity_Load);
            this.gbPassword.ResumeLayout(false);
            this.gbPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.GroupBox gbPassword;
        private System.Windows.Forms.Button pbOK;
    }
}