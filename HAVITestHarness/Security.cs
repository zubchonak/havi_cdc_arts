﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HAVITestProject;

namespace HAVITestHarness
{
    public partial class frmSecurity : Form
    {
        public bool status = false;
        public string CallFrom = "";
        public string CDCEnv = "";

        public frmSecurity()
        {
            InitializeComponent();
        }
        private void frmSecurity_Load(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            Settings.RunTimeParam.UserNetId = Environment.UserName.ToString();
            if (CallFrom == "TestHarness")
            {
                bool greet = false;
                switch (Settings.RunTimeParam.UserNetId)
                {
                    case "dzubchon":
                        {
                            MessageBox.Show("Welcome back Denis!", "TestHarness Security", MessageBoxButtons.OK);
                            greet = true;
                            break;
                        }
                    case "kzaidi":
                        {
                            MessageBox.Show("Welcome back Kiran!", "TestHarness Security", MessageBoxButtons.OK);
                            greet = true;
                            break;
                        }
                    
                }
                if (greet)
                {
                    Settings.RunTimeParam.AuthUser = true;
                    this.Close();
                }
            }
        }
        private void pbOK_Click(object sender, EventArgs e)
        {
            if (CallFrom == "TestHarness")
            {
                
                if (CallFrom == "TestHarness" && tbPassword.Text == Properties.Settings.Default.THPassword)
                {
                    Settings.RunTimeParam.AuthUser = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Incorrect password", "TestHarness Security", MessageBoxButtons.OK);
                    Settings.RunTimeParam.AuthUser = false;
                    this.Close();
                }
                                       
                return;
            }

            if (tbPassword.Text == Properties.Settings.Default.UserMngPsw)
            {
                status = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrect password", "TestHarness Security", MessageBoxButtons.OK);                
                status = false;
                this.Close();
            }
            if (!status) return;


            if (CallFrom == "CDCUserMng")
            {
                CDCUserMng CDCUserMng = new CDCUserMng();
                if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CDCUserMng.CDCEnv = "PROD";
                    CDCUserMng.ShowDialog();
                }
            }
            if (CallFrom == "CDCCacheMng")
            {
                CacheMng CacheMng = new CacheMng();
                if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC Chache Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CacheMng.CDCEnv = "PROD";
                    CacheMng.ShowDialog();
                }
            }
            if (CallFrom == "CDCNotifyMng")
            {
                NotifyContact NotifyContact = new NotifyContact();
                if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    NotifyContact.CDCEnv = "PROD";
                    NotifyContact.ShowDialog();
                }
            }
            if (CallFrom == "TestRunner")
            {
                TestRunner TestRunner = new TestRunner();
                if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    TestRunner.CDCEnv = "PROD";
                    TestRunner.ShowDialog();
                }
            }
            if (CallFrom == "BatchSubmissionCheck")
            {
                BatchSubmissionCheck BatchSubmissionCheck = new BatchSubmissionCheck();
                if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    BatchSubmissionCheck.CDCEnv = "PROD";
                    BatchSubmissionCheck.ShowDialog();
                }
            }
            
        }
        private void tbPassword_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode.Equals(Keys.Enter))
            {
                pbOK.PerformClick();
            }
        }

        
        
        

    }
}
