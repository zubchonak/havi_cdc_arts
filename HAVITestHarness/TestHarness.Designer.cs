﻿namespace HAVITestHarness
{
    partial class FormHAVITestHarness
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHAVITestHarness));
            this.pbCDCLogin = new System.Windows.Forms.Button();
            this.UserIdListBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbLegacy = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.DEMORadioButton = new System.Windows.Forms.RadioButton();
            this.QARadioButton = new System.Windows.Forms.RadioButton();
            this.DEVRadioButton = new System.Windows.Forms.RadioButton();
            this.LOCALRadioButton = new System.Windows.Forms.RadioButton();
            this.PRODRadioButton = new System.Windows.Forms.RadioButton();
            this.IERadioButton = new System.Windows.Forms.RadioButton();
            this.FirefoxRadiobutton = new System.Windows.Forms.RadioButton();
            this.ChromeRadioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRunnerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRunnerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cDCUserManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cacheManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyContactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchSubmissionCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbCDCLogin
            // 
            this.pbCDCLogin.Location = new System.Drawing.Point(164, 63);
            this.pbCDCLogin.Name = "pbCDCLogin";
            this.pbCDCLogin.Size = new System.Drawing.Size(84, 23);
            this.pbCDCLogin.TabIndex = 0;
            this.pbCDCLogin.Text = "CDC Login";
            this.pbCDCLogin.UseVisualStyleBackColor = true;
            this.pbCDCLogin.Click += new System.EventHandler(this.Button1_Click);
            // 
            // UserIdListBox
            // 
            this.UserIdListBox.FormattingEnabled = true;
            this.UserIdListBox.Items.AddRange(new object[] {
            "CDC_demo_bama1@havi.com",
            "CDC_demo_bama2@havi.com",
            "CDC_demo_quaker1@havi.com",
            "CDC_demo_quaker2@havi.com",
            "CDC_demo_Mullins1@havi.com",
            "CDC_demo_Mullins2@havi.com",
            "CDC_demo_GSF1@havi.com",
            "CDC_demo_GSF2@havi.com",
            "CDC_demo_gortons1@havi.com",
            "CDC_demo_ldfoods1@havi.com",
            "CDC_demo_cc-sd-tea1@havi.com",
            "CDC_demo_cc-sd-coffee1@havi.com",
            "CDC_demo_cc-damron1@havi.com",
            "CDC_demo_damron1@havi.com",
            "CDC_demo_Taylor-Farms1@havi.com",
            "CDC_demo_Peterson-Farms1@havi.com",
            "CDC_demo_Saputo1@havi.com",
            "CDC_demo_Prairie-Farms1@havi.com",
            "CDC_demo_Lyons1@havi.com",
            "CDC_demo_Lopez1@havi.com",
            "CDC_demo_Aryzta1@havi.com",
            "CDC_demo_Conagra1@havi.com",
            "CDC_demo_C-H-Guenther1@havi.com",
            "CDC_demo_Pioneer1@havi.com",
            "CDC_demo_Bimbo1@havi.com",
            "CDC_demo_General-Mills1@havi.com",
            "CDC_demo_Sons-Bakery1@havi.com",
            "CDC_demo_supp@havi.com",
            "",
            "CDC_demo_categorylead1@havi.com",
            "CDC_demo_categorylead2@havi.com",
            "CDC_demo_categorylead3@havi.com",
            "CDC_demp_catlead@havi.com"});
            this.UserIdListBox.Location = new System.Drawing.Point(6, 33);
            this.UserIdListBox.Name = "UserIdListBox";
            this.UserIdListBox.Size = new System.Drawing.Size(245, 21);
            this.UserIdListBox.TabIndex = 1;
            this.UserIdListBox.SelectedIndexChanged += new System.EventHandler(this.UserIdListBox_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbLegacy);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.IERadioButton);
            this.groupBox1.Controls.Add(this.FirefoxRadiobutton);
            this.groupBox1.Controls.Add(this.ChromeRadioButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.UserIdListBox);
            this.groupBox1.Controls.Add(this.pbCDCLogin);
            this.groupBox1.Location = new System.Drawing.Point(12, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 120);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "CDC Login";
            // 
            // cbLegacy
            // 
            this.cbLegacy.AutoSize = true;
            this.cbLegacy.Enabled = false;
            this.cbLegacy.Location = new System.Drawing.Point(164, 92);
            this.cbLegacy.Name = "cbLegacy";
            this.cbLegacy.Size = new System.Drawing.Size(61, 17);
            this.cbLegacy.TabIndex = 9;
            this.cbLegacy.Text = "Legacy";
            this.cbLegacy.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.DEMORadioButton);
            this.panel1.Controls.Add(this.QARadioButton);
            this.panel1.Controls.Add(this.DEVRadioButton);
            this.panel1.Controls.Add(this.LOCALRadioButton);
            this.panel1.Controls.Add(this.PRODRadioButton);
            this.panel1.Location = new System.Drawing.Point(6, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 54);
            this.panel1.TabIndex = 8;
            // 
            // DEMORadioButton
            // 
            this.DEMORadioButton.AutoSize = true;
            this.DEMORadioButton.Location = new System.Drawing.Point(95, 3);
            this.DEMORadioButton.Name = "DEMORadioButton";
            this.DEMORadioButton.Size = new System.Drawing.Size(57, 17);
            this.DEMORadioButton.TabIndex = 10;
            this.DEMORadioButton.TabStop = true;
            this.DEMORadioButton.Text = "DEMO";
            this.DEMORadioButton.UseVisualStyleBackColor = true;
            this.DEMORadioButton.CheckedChanged += new System.EventHandler(this.DEMORadioButton_CheckedChanged);
            // 
            // QARadioButton
            // 
            this.QARadioButton.AutoSize = true;
            this.QARadioButton.Location = new System.Drawing.Point(3, 3);
            this.QARadioButton.Name = "QARadioButton";
            this.QARadioButton.Size = new System.Drawing.Size(40, 17);
            this.QARadioButton.TabIndex = 6;
            this.QARadioButton.TabStop = true;
            this.QARadioButton.Text = "QA";
            this.QARadioButton.UseVisualStyleBackColor = true;
            this.QARadioButton.CheckedChanged += new System.EventHandler(this.QARadioButton_CheckedChanged);
            // 
            // DEVRadioButton
            // 
            this.DEVRadioButton.AutoSize = true;
            this.DEVRadioButton.Location = new System.Drawing.Point(46, 3);
            this.DEVRadioButton.Name = "DEVRadioButton";
            this.DEVRadioButton.Size = new System.Drawing.Size(47, 17);
            this.DEVRadioButton.TabIndex = 7;
            this.DEVRadioButton.TabStop = true;
            this.DEVRadioButton.Text = "DEV";
            this.DEVRadioButton.UseVisualStyleBackColor = true;
            this.DEVRadioButton.CheckedChanged += new System.EventHandler(this.DEVRadioButton_CheckedChanged);
            // 
            // LOCALRadioButton
            // 
            this.LOCALRadioButton.AutoSize = true;
            this.LOCALRadioButton.Location = new System.Drawing.Point(3, 26);
            this.LOCALRadioButton.Name = "LOCALRadioButton";
            this.LOCALRadioButton.Size = new System.Drawing.Size(59, 17);
            this.LOCALRadioButton.TabIndex = 12;
            this.LOCALRadioButton.TabStop = true;
            this.LOCALRadioButton.Text = "LOCAL";
            this.LOCALRadioButton.UseVisualStyleBackColor = true;
            this.LOCALRadioButton.CheckedChanged += new System.EventHandler(this.LOCALRadioButton_CheckedChanged);
            // 
            // PRODRadioButton
            // 
            this.PRODRadioButton.AutoSize = true;
            this.PRODRadioButton.Location = new System.Drawing.Point(95, 26);
            this.PRODRadioButton.Name = "PRODRadioButton";
            this.PRODRadioButton.Size = new System.Drawing.Size(56, 17);
            this.PRODRadioButton.TabIndex = 11;
            this.PRODRadioButton.TabStop = true;
            this.PRODRadioButton.Text = "PROD";
            this.PRODRadioButton.UseVisualStyleBackColor = true;
            this.PRODRadioButton.CheckedChanged += new System.EventHandler(this.PRODRadioButton_CheckedChanged);
            // 
            // IERadioButton
            // 
            this.IERadioButton.AutoSize = true;
            this.IERadioButton.Location = new System.Drawing.Point(257, 63);
            this.IERadioButton.Name = "IERadioButton";
            this.IERadioButton.Size = new System.Drawing.Size(35, 17);
            this.IERadioButton.TabIndex = 5;
            this.IERadioButton.Text = "IE";
            this.IERadioButton.UseVisualStyleBackColor = true;
            // 
            // FirefoxRadiobutton
            // 
            this.FirefoxRadiobutton.AutoSize = true;
            this.FirefoxRadiobutton.Location = new System.Drawing.Point(257, 42);
            this.FirefoxRadiobutton.Name = "FirefoxRadiobutton";
            this.FirefoxRadiobutton.Size = new System.Drawing.Size(56, 17);
            this.FirefoxRadiobutton.TabIndex = 4;
            this.FirefoxRadiobutton.Text = "Firefox";
            this.FirefoxRadiobutton.UseVisualStyleBackColor = true;
            // 
            // ChromeRadioButton
            // 
            this.ChromeRadioButton.AutoSize = true;
            this.ChromeRadioButton.Checked = true;
            this.ChromeRadioButton.Location = new System.Drawing.Point(257, 20);
            this.ChromeRadioButton.Name = "ChromeRadioButton";
            this.ChromeRadioButton.Size = new System.Drawing.Size(61, 17);
            this.ChromeRadioButton.TabIndex = 3;
            this.ChromeRadioButton.TabStop = true;
            this.ChromeRadioButton.Text = "Chrome";
            this.ChromeRadioButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "User ID";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(351, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testRunnerToolStripMenuItem,
            this.testRunnerToolStripMenuItem1});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.testToolStripMenuItem.Text = "Test";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // testRunnerToolStripMenuItem
            // 
            this.testRunnerToolStripMenuItem.Name = "testRunnerToolStripMenuItem";
            this.testRunnerToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.testRunnerToolStripMenuItem.Text = "ARTS";
            this.testRunnerToolStripMenuItem.Click += new System.EventHandler(this.testRunnerToolStripMenuItem_Click);
            // 
            // testRunnerToolStripMenuItem1
            // 
            this.testRunnerToolStripMenuItem1.Name = "testRunnerToolStripMenuItem1";
            this.testRunnerToolStripMenuItem1.Size = new System.Drawing.Size(136, 22);
            this.testRunnerToolStripMenuItem1.Text = "Test Runner";
            this.testRunnerToolStripMenuItem1.Click += new System.EventHandler(this.testRunnerToolStripMenuItem1_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cDCUserManagerToolStripMenuItem,
            this.cacheManagerToolStripMenuItem,
            this.notifyContactToolStripMenuItem,
            this.batchSubmissionCheckToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // cDCUserManagerToolStripMenuItem
            // 
            this.cDCUserManagerToolStripMenuItem.Name = "cDCUserManagerToolStripMenuItem";
            this.cDCUserManagerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.cDCUserManagerToolStripMenuItem.Text = "CDC User Manager";
            this.cDCUserManagerToolStripMenuItem.Click += new System.EventHandler(this.cDCUserManagerToolStripMenuItem_Click);
            // 
            // cacheManagerToolStripMenuItem
            // 
            this.cacheManagerToolStripMenuItem.Name = "cacheManagerToolStripMenuItem";
            this.cacheManagerToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.cacheManagerToolStripMenuItem.Text = "Cache Manager";
            this.cacheManagerToolStripMenuItem.Click += new System.EventHandler(this.cacheManagerToolStripMenuItem_Click);
            // 
            // notifyContactToolStripMenuItem
            // 
            this.notifyContactToolStripMenuItem.Name = "notifyContactToolStripMenuItem";
            this.notifyContactToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.notifyContactToolStripMenuItem.Text = "Notify Contact";
            this.notifyContactToolStripMenuItem.Click += new System.EventHandler(this.notifyContactToolStripMenuItem_Click);
            // 
            // batchSubmissionCheckToolStripMenuItem
            // 
            this.batchSubmissionCheckToolStripMenuItem.Name = "batchSubmissionCheckToolStripMenuItem";
            this.batchSubmissionCheckToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.batchSubmissionCheckToolStripMenuItem.Text = "Batch Submission Check";
            this.batchSubmissionCheckToolStripMenuItem.Click += new System.EventHandler(this.batchSubmissionCheckToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.updateToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // updateToolStripMenuItem
            // 
            this.updateToolStripMenuItem.Name = "updateToolStripMenuItem";
            this.updateToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.updateToolStripMenuItem.Text = "Update";
            this.updateToolStripMenuItem.Click += new System.EventHandler(this.updateToolStripMenuItem_Click);
            // 
            // FormHAVITestHarness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 161);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormHAVITestHarness";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CDC TestHarness";
            this.Load += new System.EventHandler(this.FormHAVITestHarness_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button pbCDCLogin;
        private System.Windows.Forms.ComboBox UserIdListBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton IERadioButton;
        private System.Windows.Forms.RadioButton FirefoxRadiobutton;
        private System.Windows.Forms.RadioButton ChromeRadioButton;
        private System.Windows.Forms.RadioButton DEVRadioButton;
        private System.Windows.Forms.RadioButton QARadioButton;
        private System.Windows.Forms.RadioButton DEMORadioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cDCUserManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cacheManagerToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbLegacy;
        private System.Windows.Forms.RadioButton LOCALRadioButton;
        private System.Windows.Forms.RadioButton PRODRadioButton;
        private System.Windows.Forms.ToolStripMenuItem notifyContactToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRunnerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRunnerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem batchSubmissionCheckToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateToolStripMenuItem;
    }
}

