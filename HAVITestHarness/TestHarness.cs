﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HAVITestProject;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace HAVITestHarness
{
    public partial class FormHAVITestHarness : Form
    {
        GlobalLib GlobalLib = new GlobalLib();

        public FormHAVITestHarness()
        {
            InitializeComponent();
        }
        public IWebDriver driver;
        int syncTime = 15;
        string sUrl = Settings.CSAEndPoints.CsaUrlPLEGQA;

        private void FormHAVITestHarness_Load(object sender, EventArgs e)
        {

            frmSecurity frmSecurity = new frmSecurity();
            frmSecurity.CallFrom = "TestHarness";
            frmSecurity.ShowDialog();
            if (!Settings.RunTimeParam.AuthUser)
            {
                this.Close();
                return;
            }

            UserIdListBox.SelectedIndex = 0;
            ChromeRadioButton.Checked = true;
            QARadioButton.Checked = true;
            cbLegacy.Checked = false;

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            WebDriverWait load;
            int SleepTime = 3000;
            string sUserID = "CDC_demo_bama1@havi.com";
            
            if (UserIdListBox.SelectedItem.ToString() != "") sUserID = this.UserIdListBox.SelectedItem.ToString();

            CDCAppDriver CDCAppDriver = new CDCAppDriver();

            if (ChromeRadioButton.Checked)
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--incognito");
                options.AddArguments("start-maximized");
                driver = new ChromeDriver(options);
            }
            else if (FirefoxRadiobutton.Checked)
            {
                //FirefoxProfile firefoxProfile = new FirefoxProfile();
                //firefoxProfile.SetPreference("browser.private.browsing.autostart", true);
                driver = new FirefoxDriver();
            }
            else if (IERadioButton.Checked)
            {
                driver = new InternetExplorerDriver();
            }
            string sUserPsw = "Zy660n!g8zz";

                      
            if (QARadioButton.Checked && cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlPLEGQA;
            else if (DEVRadioButton.Checked && cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlLEGDEV;
            else if (QARadioButton.Checked && !cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlQA;
            else if (DEMORadioButton.Checked && !cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlDEMO;
            else if (DEVRadioButton.Checked && !cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlDEV;
            else if (LOCALRadioButton.Checked && !cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlDEV;
            else if (PRODRadioButton.Checked && !cbLegacy.Checked) sUrl = Settings.CSAEndPoints.CsaUrlPROD;

            driver.Navigate().GoToUrl(sUrl);
            load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

            if (!cbLegacy.Checked)
            {
                var wait1 = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                var clickableElement1 = wait1.Until(ExpectedConditions.ElementToBeClickable(By.Id("loginButton")));
                clickableElement1.Click();
            
            
                driver.FindElement(By.Id(@"username")).SendKeys(sUserID);
                
                driver.FindElement(By.Id(@"password")).SendKeys(sUserPsw);

                
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div/div/div[2]/form/div/div[3]/button")));
                clickableElement.Click();

                
            }
            else
            {
                driver.FindElement(By.Name(@"loginfmt")).SendKeys(sUserID);

                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("idSIButton9")));
                clickableElement.Click();

                load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                //Thread.Sleep(SleepTime);

                driver.FindElement(By.Name(@"passwd")).SendKeys(sUserPsw);

                //Thread.Sleep(SleepTime);


                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("idSIButton9")));
                clickableElement.Click();

                if (GlobalLib.IsElementPresent(driver, By.XPath(@"/html/body/div/form/div[1]/div/div[1]/div[2]/div/div/div[2]")))
                {
                    var element = driver.FindElement(By.XPath(@"/html/body/div/form/div[1]/div/div[1]/div[2]/div/div/div[2]"));
                    if (element.Text == "Stay signed in?")
                    {
                        Thread.Sleep(SleepTime);
                        CDCAppDriver.SyncAndPress(driver, CFORLib.LoginPage.LoginButton, syncTime);


                    }
                }
            }


        }

        private void InstallUpdateSyncWithInfo()
        {
            throw new NotImplementedException();
        }

        private void UserIdListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
        private void testRunnerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("HAVITestProject.exe");
        }


        private void cDCUserManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            CDCUserMng CDCUserMng = new CDCUserMng();
            frmSecurity frmSecurity = new frmSecurity();

            if (QARadioButton.Checked) CDCUserMng.CDCEnv = "QA";
            else if (DEVRadioButton.Checked) CDCUserMng.CDCEnv = "DEV";
            else if (DEMORadioButton.Checked) CDCUserMng.CDCEnv = "DEMO";
            else if (LOCALRadioButton.Checked) CDCUserMng.CDCEnv = "LOCAL";
            else if (PRODRadioButton.Checked) {
                CDCUserMng.CDCEnv = "PROD";

                frmSecurity.CallFrom = "CDCUserMng";
                frmSecurity.CDCEnv = CDCUserMng.CDCEnv;
                frmSecurity.ShowDialog();
                return;
            }

            if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CDCUserMng.CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CDCUserMng.ShowDialog();
            }
        }


        private void cacheManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            CacheMng CacheMng = new CacheMng();
            frmSecurity frmSecurity = new frmSecurity();

            if (QARadioButton.Checked) CacheMng.CDCEnv = "QA";
            else if (DEVRadioButton.Checked) CacheMng.CDCEnv = "DEV";
            else if (DEMORadioButton.Checked) CacheMng.CDCEnv = "DEMO";
            else if (LOCALRadioButton.Checked) CacheMng.CDCEnv = "LOCAL";
            else if (PRODRadioButton.Checked)
            {
                CacheMng.CDCEnv = "PROD";

                frmSecurity.CallFrom = "CDCCacheMng";
                frmSecurity.CDCEnv = CacheMng.CDCEnv;
                frmSecurity.ShowDialog();
                return;
            }

            if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + CacheMng.CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CacheMng.ShowDialog();
            }
        }
        private void notifyContactToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            NotifyContact NotifyContact = new NotifyContact();
            frmSecurity frmSecurity = new frmSecurity();

            if (QARadioButton.Checked) NotifyContact.CDCEnv = "QA";
            else if (DEVRadioButton.Checked) NotifyContact.CDCEnv = "DEV";
            else if (DEMORadioButton.Checked) NotifyContact.CDCEnv = "DEMO";
            else if (LOCALRadioButton.Checked) NotifyContact.CDCEnv = "LOCAL";
            else if (PRODRadioButton.Checked)
            {
                NotifyContact.CDCEnv = "PROD";

                frmSecurity.CallFrom = "CDCNotifyMng";
                frmSecurity.CDCEnv = NotifyContact.CDCEnv;
                frmSecurity.ShowDialog();
                return;
            }

            if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + NotifyContact.CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                NotifyContact.ShowDialog();
            }
        }
        private void testRunnerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestRunner TestRunner = new TestRunner();
            frmSecurity frmSecurity = new frmSecurity();

            if (QARadioButton.Checked) TestRunner.CDCEnv = "QA";
            else if (DEVRadioButton.Checked) TestRunner.CDCEnv = "DEV";
            else if (DEMORadioButton.Checked) TestRunner.CDCEnv = "DEMO";
            else if (LOCALRadioButton.Checked) TestRunner.CDCEnv = "LOCAL";
            else if (PRODRadioButton.Checked)
            {
                TestRunner.CDCEnv = "PROD";

                frmSecurity.CallFrom = "TestRunner";
                frmSecurity.CDCEnv = TestRunner.CDCEnv;
                frmSecurity.ShowDialog();
                return;
            }

            if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + TestRunner.CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                TestRunner.ShowDialog();
            }
        }

        private void batchSubmissionCheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();

            BatchSubmissionCheck BatchSubmissionCheck = new BatchSubmissionCheck();
            frmSecurity frmSecurity = new frmSecurity();

            if (QARadioButton.Checked) BatchSubmissionCheck.CDCEnv = "QA";
            else if (DEVRadioButton.Checked) BatchSubmissionCheck.CDCEnv = "DEV";
            else if (DEMORadioButton.Checked) BatchSubmissionCheck.CDCEnv = "DEMO";
            else if (LOCALRadioButton.Checked) BatchSubmissionCheck.CDCEnv = "LOCAL";
            else if (PRODRadioButton.Checked)
            {
                BatchSubmissionCheck.CDCEnv = "PROD";

                frmSecurity.CallFrom = "BatchSubmissionCheck";
                frmSecurity.CDCEnv = BatchSubmissionCheck.CDCEnv;
                frmSecurity.ShowDialog();
                return;
            }

            if (MessageBox.Show("WARNING!!!\r\n\r\nThis change will impact CDC in " + BatchSubmissionCheck.CDCEnv.ToString() + " environment.\r\n\r\nAre you sure?", "CDC User Manager", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                BatchSubmissionCheck.ShowDialog();
            }
        }

        private void DEVRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            GlobalLib.CDCEnv = "DEV";
        }
        private void QARadioButton_CheckedChanged(object sender, EventArgs e)
        {
            GlobalLib.CDCEnv = "QA";
        }
        private void DEMORadioButton_CheckedChanged(object sender, EventArgs e)
        {
            GlobalLib.CDCEnv = "DEMO";
        }
        private void PRODRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            GlobalLib.CDCEnv = "PROD";
        }
        private void LOCALRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            GlobalLib.CDCEnv = "LOCAL";
        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Update Update = new Update();
            Update.InstallUpdateSyncWithInfo();
        }
    }
}
