﻿namespace HAVITestHarness
{
    partial class TestRunner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTestList = new System.Windows.Forms.ListBox();
            this.pbRunTest = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbClearResults = new System.Windows.Forms.Button();
            this.pbOpenFolder = new System.Windows.Forms.Button();
            this.txtTestResultPath = new System.Windows.Forms.TextBox();
            this.pbTestResult = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTestList
            // 
            this.lbTestList.FormattingEnabled = true;
            this.lbTestList.Location = new System.Drawing.Point(6, 34);
            this.lbTestList.Name = "lbTestList";
            this.lbTestList.Size = new System.Drawing.Size(202, 251);
            this.lbTestList.TabIndex = 0;
            this.lbTestList.SelectedIndexChanged += new System.EventHandler(this.lbTestList_SelectedIndexChanged);
            // 
            // pbRunTest
            // 
            this.pbRunTest.Location = new System.Drawing.Point(463, 262);
            this.pbRunTest.Name = "pbRunTest";
            this.pbRunTest.Size = new System.Drawing.Size(75, 23);
            this.pbRunTest.TabIndex = 1;
            this.pbRunTest.Text = "Run Test";
            this.pbRunTest.UseVisualStyleBackColor = true;
            this.pbRunTest.Click += new System.EventHandler(this.pbRunTest_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbClearResults);
            this.groupBox1.Controls.Add(this.pbOpenFolder);
            this.groupBox1.Controls.Add(this.txtTestResultPath);
            this.groupBox1.Controls.Add(this.pbTestResult);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pbRunTest);
            this.groupBox1.Controls.Add(this.lbTestList);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(548, 298);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ARTS";
            // 
            // pbClearResults
            // 
            this.pbClearResults.Location = new System.Drawing.Point(214, 262);
            this.pbClearResults.Name = "pbClearResults";
            this.pbClearResults.Size = new System.Drawing.Size(80, 23);
            this.pbClearResults.TabIndex = 6;
            this.pbClearResults.Text = "Clear Results";
            this.pbClearResults.UseVisualStyleBackColor = true;
            // 
            // pbOpenFolder
            // 
            this.pbOpenFolder.Location = new System.Drawing.Point(300, 262);
            this.pbOpenFolder.Name = "pbOpenFolder";
            this.pbOpenFolder.Size = new System.Drawing.Size(75, 23);
            this.pbOpenFolder.TabIndex = 5;
            this.pbOpenFolder.Text = "Open Folder";
            this.pbOpenFolder.UseVisualStyleBackColor = true;
            this.pbOpenFolder.Click += new System.EventHandler(this.pbOpenFolder_Click);
            // 
            // txtTestResultPath
            // 
            this.txtTestResultPath.Location = new System.Drawing.Point(214, 236);
            this.txtTestResultPath.Name = "txtTestResultPath";
            this.txtTestResultPath.Size = new System.Drawing.Size(242, 20);
            this.txtTestResultPath.TabIndex = 4;
            // 
            // pbTestResult
            // 
            this.pbTestResult.Location = new System.Drawing.Point(381, 262);
            this.pbTestResult.Name = "pbTestResult";
            this.pbTestResult.Size = new System.Drawing.Size(75, 23);
            this.pbTestResult.TabIndex = 3;
            this.pbTestResult.Text = "Open Result";
            this.pbTestResult.UseVisualStyleBackColor = true;
            this.pbTestResult.Click += new System.EventHandler(this.pbTestResult_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Test List:";
            // 
            // TestRunner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 324);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TestRunner";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TestRunner";
            this.Load += new System.EventHandler(this.TestRunner_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbTestList;
        private System.Windows.Forms.Button pbRunTest;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button pbTestResult;
        private System.Windows.Forms.Button pbClearResults;
        private System.Windows.Forms.Button pbOpenFolder;
        private System.Windows.Forms.TextBox txtTestResultPath;
    }
}