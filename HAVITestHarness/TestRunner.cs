﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using HAVITestProject;
using System.Reflection;
using System.Diagnostics;

namespace HAVITestHarness
{
    public partial class TestRunner : Form
    {
        public string CDCEnv = "QA";
        //public string strResultFolder = Settings.Report.ReportDir;
        public string strResultFolder = "C:\\temp";

        public TestRunner()
        {
            InitializeComponent();
        }
        private void TestRunner_Load(object sender, EventArgs e)
        {

            GetListOfTests();

        }


        private void lbTestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtTestResultPath.Text = String.Format("{1}\\{0}_TestResult_{2}.html", CDCEnv, strResultFolder, lbTestList.SelectedItem);
            
        }

        public void GetListOfTests()
        {
            var ARTS = new TestInitialize.ARTS();
            var assembly = ARTS.GetType().Assembly;

            var testTypes = from t in assembly.GetTypes()
                            let attributes = t.GetCustomAttributes(typeof(NUnit.Framework.TestFixtureAttribute), true)
                            where attributes != null && attributes.Length > 0
                            orderby t.Name
                            select t;
            foreach (var type in testTypes)
            {
                //get test method in class.
                var testMethods = from m in type.GetMethods()
                                  let attributes = m.GetCustomAttributes(typeof(NUnit.Framework.TestAttribute), true)
                                  where attributes != null && attributes.Length > 0
                                  orderby m.Name
                                  select m;
                foreach (var method in testMethods)
                {
                    lbTestList.Items.Add(method.Name);
                    
                }
            }

        }

        private void pbRunTest_Click(object sender, EventArgs e)
        {
            var ARTS = new TestInitialize.ARTS();
            ReportLib ReportLib = new ReportLib();
            string TestToRun = lbTestList.SelectedItem.ToString();

            Settings.RunTimeParam.CSAEnv = CDCEnv;

            Settings.Report.ReportName = txtTestResultPath.Text;
            Settings.Report.ReportDir = System.IO.Path.GetDirectoryName(Settings.Report.ReportName);
            strResultFolder = Settings.Report.ReportDir;

            if (TestToRun == "")
            {
                MessageBox.Show("Select test to run");
                return;
            }
            var assembly = ARTS.GetType().Assembly;

            var testTypes = from t in assembly.GetTypes()
                            let attributes = t.GetCustomAttributes(typeof(NUnit.Framework.TestFixtureAttribute), true)
                            where attributes != null && attributes.Length > 0
                            orderby t.Name
                            select t;
            foreach (var type in testTypes)
            {
                //get test method in class.
                var testMethods = from m in type.GetMethods()
                                  let attributes = m.GetCustomAttributes(typeof(NUnit.Framework.TestAttribute), true)
                                  where attributes != null && attributes.Length > 0
                                  orderby m.Name
                                  select m;
                foreach (var method in testMethods)
                {
                    if(TestToRun == method.Name)
                    {

                        if (method != null)
                        {
                            object result = null;
                            ParameterInfo[] parameters = method.GetParameters();
                            object classInstance = Activator.CreateInstance(type, null);

                            if (parameters.Length == 0)
                            {
                                // This works fine
                                result = method.Invoke(classInstance, null);
                            }
                            else
                            {
                                object[] parametersArray = new string[] { "Chrome" };

                                if (System.IO.Directory.Exists(strResultFolder))
                                {
                                    bool exists = System.IO.Directory.Exists(strResultFolder);
                                        if (!exists)
                                        System.IO.Directory.CreateDirectory(strResultFolder);

                                    Settings.Report.ReportImg = Settings.Report.ReportDir + "\\img\\";


                                    exists = System.IO.Directory.Exists(Settings.Report.ReportImg);
                                    if (!exists)
                                        System.IO.Directory.CreateDirectory(Settings.Report.ReportImg);

                                    Settings.Report.ReportDir = System.IO.Path.GetDirectoryName(txtTestResultPath.Text);
                                    Settings.Report.ReportName = txtTestResultPath.Text;


                                    //ARTS.strReportFilePath = txtTestResultPath.Text;
                                    //ReportLib.ImgDir = strResultFolder + "\\img\\";
                                    result = method.Invoke(classInstance, parametersArray);
                                    //ARTS.TearDown();
                                }
                                else
                                {
                                    MessageBox.Show("Cannot create report");
                                }
                                
                            }
                        }
                    }


                }
            }
        }

        private void pbTestResult_Click(object sender, EventArgs e)
        {
            var ARTS = new TestInitialize.ARTS();
            ARTS.AfterTest();
        }

        private void pbOpenFolder_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("explorer.exe", strResultFolder.ToString());
            }
            catch
            {

            }
            
        }
    }
}
