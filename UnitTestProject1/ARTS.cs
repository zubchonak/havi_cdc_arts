﻿using System;
using System.Threading;
using System.Data;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using OpenQA.Selenium.Firefox;
using System.Collections.Generic;
using OpenQA.Selenium.Opera;
using System.Drawing.Imaging;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace HAVITestProject
{

    [SetUpFixture]
    public abstract class TestInitialize
    {
        public IWebDriver driver;
        ReportLib ReportLib = new ReportLib();
        WebDriverWait load;

        public string CSAEnv = Settings.RunTimeParam.CSAEnv;
        string sUrl;

        int syncTime = 20;
        //string sUrl = "https://ui.cdc-qa.havi.com/";
        //string sUrl = "https://ui.cdc-dev.havi.com/";

        //string sUrl = "https://qa.connect.havi.com/app/login";
        //string sUrl = "https://dev.connect.havi.com/app/login";
        //string sUrl = "https://demo.connect.havi.com/app/login";
        

        string dir = Settings.Report.ReportDir + "\\";
        
        ExtentReports extent = new ExtentReports();
        CDCAppDriver CDCAppDriver = new CDCAppDriver();
        ExcelLib ExcelLib = new ExcelLib();

        string strReportFilePath = "";
        public ExtentTest test;

        String[] aFCost = new string[0];
        public static IEnumerable<String> BrowserToRun()
        {            
            String[] browsers = GetBrowsers();
            foreach (String b in browsers) yield return b;
        }

        private static string[] GetBrowsers()
        {
            //List of browsers to run with
            //String[] browsers = { "Chrome", "Firefox", "IE", "BrowserStack" };
            return new string[] { "Chrome", "Firefox" };
        }

        public string GetEnvURL(string CSAEnv)
        {
            string EnvURL;

            if (CSAEnv == "QA")
                EnvURL = Settings.CSAEndPoints.CsaUrlQA;
            else if(CSAEnv == "DEV")
                EnvURL = Settings.CSAEndPoints.CsaUrlDEV;
            else if (CSAEnv == "DEMO")
                EnvURL = Settings.CSAEndPoints.CsaUrlDEMO;
            else
                EnvURL = Settings.CSAEndPoints.CsaUrlDEV;

            return EnvURL;
        }
        
        public void CreateDriver()
        {
            // start reporters
            if (Settings.Report.ReportName == null)
            {
                strReportFilePath = Settings.Report.ReportDir + "\\" + "CDC-ARTS.html";
                Settings.Report.ReportName = strReportFilePath;
            }
            else
            {
                strReportFilePath = Settings.Report.ReportName;
            }
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(strReportFilePath);
            htmlReporter.Configuration().DocumentTitle = "HAVI - CDC Automation";
            htmlReporter.Configuration().ReportName = "CDC - ARTS";
            extent.AttachReporter(htmlReporter);

        }

        public void BeforeTest(string BrowserName)
        {
            sUrl = GetEnvURL(Settings.RunTimeParam.CSAEnv);

            if (test == null)
            {
                CreateDriver();
                test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
            }
            if (BrowserName.Equals("Chrome"))
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--incognito");
                options.AddArguments("start-maximized");
                driver = new ChromeDriver(options);
            }
            else if (BrowserName.Equals("Firefox"))
            {
                FirefoxOptions firefoxProfile = new FirefoxOptions();
                //SetPreference(String, Boolean)
                firefoxProfile.SetPreference("browser.privatebrowsing.autostart", true);
                //FirefoxOptions firefoxProfile = new FirefoxOptions();
                //firefoxProfile.SetPreference("browser.private.browsing.autostart", true);

                driver = new FirefoxDriver(firefoxProfile);
                driver.Manage().Window.Maximize();
            }
            else if (BrowserName.Equals("BrowserStack"))
            {
                //IWebDriver driver;
                DesiredCapabilities capability = DesiredCapabilities.Chrome();
                capability.SetCapability("browser", "Chrome");
                capability.SetCapability("browser_version", "62.0");
                capability.SetCapability("os", "Windows");
                capability.SetCapability("os_version", "10");
                capability.SetCapability("resolution", "1920x1080");
                capability.SetCapability("browserstack.user", "deniszubchonak1");
                capability.SetCapability("browserstack.key", "Q6JfRLqLu4F6W1zojwqM");
                capability.SetCapability("browserstack.local", "true");
                capability.SetCapability("browserstack.debug", "true");

                driver = new RemoteWebDriver(new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability);
            }
            else if (BrowserName.Equals("IE"))
            {
                driver = new InternetExplorerDriver();
            }

            ReportLib.PrintResult("Open '" + sUrl + "'");
            driver.Navigate().GoToUrl(sUrl);
            load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

        }
        public void BeforeTest(string BrowserName, string sNote)
        {
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
            if (BrowserName.Equals("Chrome"))
            {
                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--incognito");
                options.AddArguments("start-maximized");
                driver = new ChromeDriver(options);
            }
            else if (BrowserName.Equals("Firefox"))
            {
                //FirefoxProfile firefoxProfile = new FirefoxProfile();
                //firefoxProfile.SetPreference("browser.private.browsing.autostart", true);
                driver = new FirefoxDriver();
            }
            else if (BrowserName.Equals("Opera"))
            {
                driver = new OperaDriver();
            }
            else if (BrowserName.Equals("IE"))
            {
                driver = new InternetExplorerDriver();
            }

            ReportLib.PrintResult("Open '" + sUrl + "'");
            driver.Navigate().GoToUrl(sUrl);
            load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

        }
        [TestFixture]
        public class ARTS : TestInitialize
        {

            [Test]
            [TestCaseSource("BrowserToRun")]
            public void SmokeTest(string BrowserName)
            {
                BeforeTest(BrowserName);

                //Login into HAVI - CDC system
                string sUserID = "CDC_demo_bama1@havi.com";
                string sUserPsw = "Zy660n!g8zz";
                string TestWSI = "02682021";

                ReportLib.PrintResult(test, "<h5>STEP 1: Login into HAVI - Cost Data Capture</h5>");
                CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);

                //Open CREATE NEW BATCH page
                ReportLib.PrintResult(test, "<h5>STEP 2: Click 'CREATE NEW BATCH' button</h5>");
                CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);

                //Fill New Batch
                ReportLib.PrintResult(test, "<h5>STEP 3: Fill New Batch page</h5>");
                string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, "", "", "Baked Goods", syncTime);
                ReportLib.PrintResult(driver, test, "Fill New Batch page");
                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 4: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                ReportLib.PrintResult(test, "Batch #: + sBatchNumber");

                // Select Finish Goods
                ReportLib.PrintResult(test, "<h5>STEP 5: Select Finish Good from the list</h5>");
                CDCAppDriver.CDCSelectFinishGoods(driver, test, TestWSI, syncTime);
                ReportLib.PrintResult(driver, test, "Select Finish Good");
                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 6: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);

                //Enter Cost Forcast
                ReportLib.PrintResult(test, "<h5>STEP 7: Fill Cost Forcast page</h5>");
                string[] aFGCost = CDCAppDriver.CDCFillCostForecast(driver, test, "BISCUIT", aFCost, "", syncTime);
                ReportLib.PrintResult(driver, test, "Fill Cost Forecast page");

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 8: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                //Read Cost Summar Page
                ReportLib.PrintResult(test, "<h5>STEP 9: Review Cost Summary page</h5>");
                CDCAppDriver.CDCReadCostSummary(driver, test, "BISCUIT", "BISCUIT", aFGCost, "", syncTime);
                ReportLib.PrintResult(driver, test, "Cost Summary page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 10: Press 'SUBMIT' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.SubmitButton, syncTime);

                //Check and Close Confirmation Massage
                ReportLib.PrintResult(test, "<h5>STEP 11: Check Confirmation Massage</h5>");
                CDCAppDriver.CloseConfMassage(driver, test, syncTime);

                //Batch Confirmation Submited Page
                ReportLib.PrintResult(test, "<h5>STEP 12: Check Batch Submited Page</h5>");
                CDCAppDriver.CDCBatchSubmitedPage(driver, test, sBatchNumber, syncTime);
                ReportLib.PrintResult(driver, test, "Batch Submited Page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 13: Press 'GO TO HOME' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.ConfirmMassagePage.GoToHomeButton, syncTime);

                //Check batch number in the batch table
                ReportLib.PrintResult(test, "<h5>STEP 14: Verify submited batch</h5>");
                CDCAppDriver.CDCCheckBatchExists(driver, test, sBatchNumber, syncTime);
                ReportLib.PrintResult(driver, test, "Back to Home Page");
                //return;
                //Build Expected results
                string[,] aCompCost = new string[1,24];
                aCompCost[0, 0] = "BISCUIT";
                aCompCost[0, 1] = "";
                aCompCost[0, 2] = "";
                aCompCost[0, 3] = "";
                //SOAPLib SOAPLib = new SOAPLib();
                int start = 5;
                for (int CompRow = start; CompRow < aCompCost.GetLength(1); CompRow++)
                {
                    aCompCost[0, CompRow] = aFGCost[CompRow - start];
                }

                ReportLib.PrintResult(driver, test, "Check review batch page");
                CDCAppDriver.CDCCheckReviewBatch(driver, test, sBatchNumber, aCompCost, syncTime);

                CDCAppDriver.SyncAndPress(driver, CFORLib.ReviewBatch.ViewSummaryButton, syncTime);

                CDCAppDriver.CDCReviewCostSummary(driver, test, "BISCUIT", aFGCost, "", syncTime);

                ReportLib.PrintResult(driver, test, "Back to Home Page");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.CancelButton, syncTime);
                
                AfterTest();
            }

            [Test]
            [TestCaseSource("BrowserToRun")]
            public void CheckComponents(string BrowserName)
            {
                BeforeTest(BrowserName);

                int StartRow = 0;

                DataTable TestData = ExcelLib.GetExpDataFromExcel("[CheckComponents$]");
                for (int curRow = StartRow; curRow < TestData.Rows.Count; curRow++)
                {
                    string sUserID = TestData.Rows[curRow]["UserName"].ToString().Trim();
                    string sUserPsw = TestData.Rows[curRow]["Password"].ToString().Trim();
                    if (curRow == StartRow)
                    {
                        //Login into HAVI - CDC system
                        ReportLib.PrintResult(test, "<h5>STEP1: Login into HAVI - Cost Data Capture</h5>");

                        CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);
                    }
                    else
                    {
                        driver.FindElement(By.ClassName("app-logout-button")).Click();
                        driver.Close();
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                        test = null;

                        BeforeTest(BrowserName);
                        //Login into HAVI - CDC system
                        ReportLib.PrintResult(test, "<h5>STEP1: Login into HAVI - Cost Data Capture</h5>");

                        CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);
                    }
                    //Open CREATE NEW BATCH page
                    ReportLib.PrintResult(test, "<h5>STEP 2: Click 'CREATE NEW BATCH' button</h5>");
                    CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);

                    //Fill New Batch
                    ReportLib.PrintResult(test, "<h5>STEP 3: Fill New Batch page with data from Excel</h5>");
                    string sBatchname = TestData.Rows[curRow]["BatchName"].ToString().Trim();
                    string sBatchDesc = TestData.Rows[curRow]["BatchDesc"].ToString().Trim();
                    string sBatchCat = TestData.Rows[curRow]["BatchCat"].ToString().Trim();

                    string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, sBatchname, sBatchDesc, sBatchCat, syncTime);
                    ReportLib.PrintResult(driver, test, "Fill New Batch page");

                    // Press Next Step button
                    ReportLib.PrintResult(test, "<h5>STEP 4: Press 'NEXT STEP' button</h5>");
                    CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                    sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                    ReportLib.PrintResult(test, "Batch #:" + sBatchNumber);

                    // Select Finish Goods
                    ReportLib.PrintResult(test, "<h5>STEP 5: Select Finish Good with data from Excel</h5>");
                    string[] aFinishGoodNum = new string[1];
                    string sFinishGoodNum;
                    string sNextBatchName;
                    string tmpFinishGoodNum;
                    int fgSetCount = 0;
                    int CostCounter = 0;
                    //Build Finish Good array set
                    for (int fgCounter = curRow; fgCounter < TestData.Rows.Count; fgCounter++)
                    {
                        sNextBatchName = TestData.Rows[fgCounter]["BatchName"].ToString().Trim();

                        if (sNextBatchName == sBatchname || sNextBatchName == "")
                        {
                            tmpFinishGoodNum = TestData.Rows[fgCounter]["FinishGoodNum"].ToString().Trim();
                            if (tmpFinishGoodNum != "")
                            {
                                Array.Resize<string>(ref aFinishGoodNum, fgSetCount + 1);
                                aFinishGoodNum[fgSetCount] = tmpFinishGoodNum;
                                fgSetCount++;
                            }
                            CostCounter++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    //Select Finish Good from array set
                    for (int d = 0; d < aFinishGoodNum.Length; d++)
                    {
                        sFinishGoodNum = aFinishGoodNum[d].Trim();

                        CDCAppDriver.CDCSelectFinishGoods(driver, test, sFinishGoodNum, syncTime);
                    }
                    ReportLib.PrintResult(driver, test, "Select Finish Good");

                    // Press Next Step button
                    ReportLib.PrintResult(test, "<h5>STEP 6: Press 'NEXT STEP' button</h5>");
                    CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);

                    //Enter Cost Forcast from excel CF1-CF18
                    ReportLib.PrintResult(test, "<h5>STEP 7: Fill Cost Forcast page</h5>");
                    string sCompName = "";
                    string tmpFinishGoodNumNext = "";
                    int cmpSetCount = 0;
                    int NewCostCounter = CostCounter;
                    for (int cfCounter = curRow; cfCounter < curRow + CostCounter; cfCounter++)
                    {
                        sNextBatchName = TestData.Rows[cfCounter]["BatchName"].ToString().Trim();

                        if (sNextBatchName == sBatchname || sNextBatchName == "")
                        {
                            tmpFinishGoodNum = TestData.Rows[cfCounter]["FinishGoodNum"].ToString().Trim();

                            if (cfCounter < NewCostCounter)
                                tmpFinishGoodNumNext = TestData.Rows[cfCounter + 1]["FinishGoodNum"].ToString().Trim();
                            else
                                tmpFinishGoodNumNext = TestData.Rows[cfCounter]["FinishGoodNum"].ToString().Trim();
                            if (tmpFinishGoodNum == "" || tmpFinishGoodNumNext != "")
                            {
                                sCompName = TestData.Rows[cfCounter]["WRIN/CMPT"].ToString().Trim();
                                Array.Resize<string>(ref aFCost, 19);
                                aFCost[0] = TestData.Rows[cfCounter]["CurrentPrice"].ToString().Trim();
                                for (int a = 1; a < aFCost.Length; a++) aFCost[a] = TestData.Rows[cfCounter]["CF" + a.ToString()].ToString().Trim();

                                CDCAppDriver.CDCFillCostForecast(driver, test, sCompName, aFCost, "", syncTime);

                                cmpSetCount++;
                            }
                            else NewCostCounter--;
                        }
                        else
                        {
                            break;
                        }
                    }

                    //break;

                    //sCompName = TestData.Rows[curRow]["CompName"].ToString().Trim();
                    //Array.Resize<string>(ref aFCost, 19);
                    //aFCost[0] = TestData.Rows[curRow]["CurrentPrice"].ToString().Trim();
                    //for (int a =1;a< aFCost.Length; a++) aFCost[a] = TestData.Rows[curRow]["CF"+a.ToString()].ToString().Trim();

                    //CDCAppDriver.CDCFillCostForecast(driver, test, sCompName, aFCost, syncTime);
                    ReportLib.PrintResult(driver, test, "Fill Cost Forecast page");

                    // Press Next Step button
                    ReportLib.PrintResult(test, "<h5>STEP 8: Press 'NEXT STEP' button</h5>");
                    CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                    //Read Cost Summar Page
                    ReportLib.PrintResult(test, "<h5>STEP 9: Review Cost Summary page with data from Excel</h5>");
                    string[] aFinishGoodCost = new string[19];
                    for (int fgCounter = curRow; fgCounter < TestData.Rows.Count; fgCounter++)
                    {
                        sNextBatchName = TestData.Rows[fgCounter]["BatchName"].ToString().Trim();

                        if (sNextBatchName == sBatchname || sNextBatchName == "")
                        {
                            tmpFinishGoodNum = TestData.Rows[fgCounter]["FinishGoodNum"].ToString().Trim();
                            if (tmpFinishGoodNum != "")
                            {
                                aFinishGoodCost[0] = TestData.Rows[fgCounter]["CurrentPrice"].ToString().Trim();
                                for (int a = 1; a < aFinishGoodCost.Length; a++) aFinishGoodCost[a] = TestData.Rows[fgCounter]["CF" + a.ToString()].ToString().Trim();

                                CDCAppDriver.CDCReadCostSummary(driver, test, tmpFinishGoodNum, tmpFinishGoodNum, aFinishGoodCost, "", syncTime);
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    //CDCAppDriver.CDCReadCostSummary(driver, test, sCompName, aFCost, syncTime);
                    ReportLib.PrintResult(driver, test, "Cost Summary page");

                    //break;

                    // Press Submit button
                    ReportLib.PrintResult(test, "<h5>STEP 10: Press 'SUBMIT' button</h5>");
                    CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.SubmitButton, syncTime);

                    //Check and Close Confirmation Massage
                    ReportLib.PrintResult(test, "<h5>STEP 11: Check Confirmation Massage</h5>");
                    CDCAppDriver.CloseConfMassage(driver, test, syncTime);

                    //Batch Confirmation Submited Page
                    ReportLib.PrintResult(test, "<h5>STEP 12: Check Batch Submited Page</h5>");
                    CDCAppDriver.CDCBatchSubmitedPage(driver, test, sBatchNumber, syncTime);
                    ReportLib.PrintResult(driver, test, "Batch Submited Page");

                    // Press Submit button
                    ReportLib.PrintResult(test, "<h5>STEP 13: Press 'GO TO HOME' button</h5>");
                    CDCAppDriver.SyncAndPress(driver, CFORLib.ConfirmMassagePage.GoToHomeButton, syncTime);

                    //Check batch number in the batch table
                    ReportLib.PrintResult(test, "<h5>STEP 14: Verify submited batch</h5>");
                    CDCAppDriver.CDCCheckBatchExists(driver, test, sBatchNumber, syncTime);
                    ReportLib.PrintResult(driver, test, "Back to Home Page");


                    curRow = curRow + CostCounter - 1;
                    //break;
                    AfterTest();

                }
            }
            [Test]
            [TestCaseSource("BrowserToRun")]
            public void Login(string BrowserName)
            {
                BeforeTest(BrowserName);

                //Login into HAVI - CDC system
                string sUserID = "CDC_demo_bama2@havi.com";
                string sUserPsw = "Zy660n!g8zz";

                CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);




                //Open CREATE NEW BATCH page
                CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);


                //Fill New Batch
                string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, "", "", "Baked Goods", syncTime);

                // Press Next Step button
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                ReportLib.PrintResult(test, "Batch #: + sBatchNumber");

                AfterTest();

            }
            [Test]
            [TestCaseSource("BrowserToRun")]
            public void GetDataFromJDE(string BrowserName)
            {
                string JDE_URL = "http://erpcrp.perseco.com/jde/E1Menu.maf?envRadioGroup=&jdeowpBackButtonProtect=PROTECTED&selectNACRP900J=NAPAMGR";

                ChromeOptions options = new ChromeOptions();
                options.AddArguments("--incognito");
                options.AddArguments("start-maximized");
                driver = new ChromeDriver(options);

                driver.Navigate().GoToUrl(JDE_URL);
                var load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

                //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                //var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(ORLib.CostForcastPage.CostForecastTable));

                //Login to JDE
                driver.FindElement(ORJde.Login.UserId).SendKeys("dzubchon");
                driver.FindElement(ORJde.Login.Password).SendKeys("Welcome1");

                CDCAppDriver CDCAppDriver = new CDCAppDriver();
                load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                //Thread.Sleep(SleepTime);
                CDCAppDriver.SyncAndPress(driver, ORJde.Login.SignInButton, syncTime);

                CDCAppDriver.SyncAndPress(driver, ORJde.Login.rbEnv, syncTime);

                // select the drop down list
                var education = driver.FindElement(ORJde.Login.EnvSelect);
                //create select element object 
                var selectElement = new SelectElement(education);

                //select by value
                selectElement.SelectByText("NAPAMGR (North America Pricing Manager)");

                CDCAppDriver.SyncAndPress(driver, ORJde.Login.OKButton, syncTime);
                load = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

                //End Login to JDE
                CDCAppDriver.SyncAndPress(driver, ORJde.Login.WRINSearch, syncTime);

                
                int StartRow = 0;

                DataTable TestData = ExcelLib.GetExpDataFromExcel("[WRINDataFromJDE$]");
                for (int curRow = StartRow; curRow < TestData.Rows.Count; curRow++)
                {
                    string sWRIN = TestData.Rows[curRow]["WRIN"].ToString().Trim();

                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));

                    wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                    var HeaderColumnRow = wait.Until(ExpectedConditions.ElementExists(ORJde.WRINSearch.HeaderColumnRow));
                    
                    driver.FindElement(ORJde.WRINSearch.WRINField).Click();
                    driver.FindElement(ORJde.WRINSearch.WRINField).SendKeys(sWRIN);
                    driver.FindElement(ORJde.WRINSearch.StatusField).SendKeys("A");

                    wait = new WebDriverWait(driver, TimeSpan.FromSeconds(syncTime));
                    var WRINTable = wait.Until(ExpectedConditions.ElementExists(ORJde.WRINSearch.WRINFieldTable));

                    AfterTest();

                    return;
                }
            }

            [Test]
            [TestCaseSource("BrowserToRun")]
            public void CheckUserWRINwithComp(string BrowserName)
            {
                BeforeTest(BrowserName);
                GlobalLib GlobalLib = new GlobalLib();

                int StartRow = 0;
                try
                { 
                    DataTable TestData = ExcelLib.GetExpDataFromExcel("[CheckUserWSI$]");
                    for (int curRow = StartRow; curRow < TestData.Rows.Count; curRow++)
                    {
                        string sUserID = TestData.Rows[curRow]["UserName"].ToString().Trim();
                        string sUserPsw = TestData.Rows[curRow]["Password"].ToString().Trim();

                        int TestWSI = Convert.ToInt32(TestData.Rows[curRow]["WSI"].ToString().Trim());
                        string ItemNumber = TestData.Rows[curRow]["ItemNumber"].ToString().Trim();
                        string TestCategory = TestData.Rows[curRow]["Category"].ToString().Trim();

                        string InputTestData = String.Format(@"Input Test Data:<br>
                                                        UserId:   {0}<br>
                                                        Password: {1}<br>
                                                        WSI:      {2}<br>
                                                        WRIN:     {3}<br>
                                                        Category: {4}<br>", sUserID, sUserPsw, TestWSI, ItemNumber, TestCategory);

                        if (curRow == StartRow)
                        {
                            //Login into HAVI - CDC system
                            ReportLib.PrintResult(test, InputTestData);

                            ReportLib.PrintResult(test, "<h5>STEP1: Login into HAVI - Cost Data Capture</h5>");

                            CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);
                        }
                        else
                        {
                            //driver.FindElement(By.ClassName("app-logout-button")).Click();
                            driver.Close();
                            Thread.Sleep(TimeSpan.FromSeconds(1));
                            test = null;

                            BeforeTest(BrowserName);
                            //Login into HAVI - CDC system
                            ReportLib.PrintResult(test, InputTestData);

                            ReportLib.PrintResult(test, "<h5>STEP1: Login into HAVI - Category Spend & Analytics</h5>");

                            CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);
                        }


                        //Open CREATE NEW BATCH page
                        ReportLib.PrintResult(test, "<h5>STEP 2: Click 'CREATE NEW BATCH' button</h5>");
                        CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);

                        //Fill New Batch
                        ReportLib.PrintResult(test, "<h5>STEP 3: Fill New Batch page</h5>");
                        string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, "", "", TestCategory, syncTime);
                        ReportLib.PrintResult(driver, test, "Fill New Batch page");

                        // Press Next Step button
                        ReportLib.PrintResult(test, "<h5>STEP 4: Press 'NEXT STEP' button</h5>");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                        sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                        ReportLib.PrintResult(test, "Batch #:" + sBatchNumber);

                        // Select Finish Goods
                        ReportLib.PrintResult(test, "<h5>STEP 5: Select Finish Good from the list</h5>");

                        List<string> SelectedWSIList = CDCAppDriver.CDCSelectFinishGoods(driver, test, ItemNumber, syncTime);
                        ReportLib.PrintResult(driver, test, "Select Finish Good");

                        // Press Next Step button
                        ReportLib.PrintResult(test, "<h5>STEP 6: Press 'NEXT STEP' button</h5>");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);

                        //Enter Cost Forcast
                        ReportLib.PrintResult(test, "<h5>STEP 7: Fill Cost Forcast page</h5>");

                        SOAPLib SOAPLib = new SOAPLib();
                        SOAPLib.JDEEnv = CSAEnv;

                        DataTable SupplierBOM = new DataTable();
                        foreach (string WSI in SelectedWSIList)
                        {
                            DataTable WSIBOM = SOAPLib.GetSupplierBOM(Convert.ToInt32(WSI.ToString().Trim()));
                            SupplierBOM.Merge(WSIBOM);
                        }

                        var BOMAllDist = (from DataRow dRow in SupplierBOM.Rows
                                          select dRow["componentItemNumber"].ToString().Trim()).Distinct();
                        int bump = 5;
                        string[,] BOMCostArray = new string[BOMAllDist.Count(), 19 + bump];
                        int CompRow = 0;
                        foreach (string BOM in BOMAllDist)
                        {
                            string[] aFCostOut = CDCAppDriver.CDCFillCostForecast(driver, test, BOM, aFCost, "Test Comments", syncTime);
                            BOMCostArray[CompRow, 0] = BOM;
                            BOMCostArray[CompRow, 1] = "Test Comments";
                            BOMCostArray[CompRow, 2] = ""; //componentQuantity
                            BOMCostArray[CompRow, 3] = ""; //costPercentage
                            BOMCostArray[CompRow, 4] = ""; //componentCost

                            for (int Col = bump; Col < aFCostOut.Count() + bump; Col++)
                            {
                                BOMCostArray[CompRow, Col] = aFCostOut[Col - bump];
                            }
                            CompRow++;
                        }

                        ReportLib.PrintResult(driver, test, "Fill Cost Forecast page");

                        // Press Next Step button
                        ReportLib.PrintResult(test, "<h5>STEP 8: Press 'NEXT STEP' button</h5>");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                        //Read Cost Summary Page
                        ReportLib.PrintResult(test, "<h5>STEP 9: Review Cost Summary page</h5>");

                        string[] FGCost = new string[19];
                        SupplierBOM = SOAPLib.GetSupplierBOM(TestWSI);
                        for (int ArrRow = 0; ArrRow < BOMCostArray.GetLength(0); ArrRow++)
                        {
                            var ItemBOMData = (from DataRow dRow in SupplierBOM.Rows
                                               where dRow["componentItemNumber"].ToString().Trim() == BOMCostArray[ArrRow, 0].ToString().Trim() &&
                                               dRow["parentItemNumber"].ToString().Trim() == ItemNumber
                                               select dRow).ToArray();
                            if (ItemBOMData.Length > 0)
                            {

                                BOMCostArray[ArrRow, 2] = ItemBOMData[0].ItemArray[3].ToString().Trim(); //componentQuantity
                                BOMCostArray[ArrRow, 3] = ItemBOMData[0].ItemArray[4].ToString().Trim(); //costPercentage
                                BOMCostArray[ArrRow, 4] = ItemBOMData[0].ItemArray[5].ToString().Trim(); //componentCost
                            }
                        }

                        string[,] aFCWithCasePrice = new string[0, 0];

                        int currentRow = 0;
                        for (int i = 0; i < BOMCostArray.GetLength(0); i++)
                        {
                            var ItemCount = (from DataRow dRow in SupplierBOM.Rows
                                             where dRow["componentItemNumber"].ToString().Trim() == BOMCostArray[i, 0].ToString().Trim()
                                             select dRow).ToArray();
                            if (ItemCount.Count() > 0)
                            {
                                GlobalLib.ResizeArray(ref aFCWithCasePrice, currentRow + 1, BOMCostArray.GetLength(1));

                                for (int j = 0; j < BOMCostArray.GetLength(1); j++)
                                {
                                    aFCWithCasePrice[currentRow, j] = BOMCostArray[i, j].ToString().Trim();
                                }
                                currentRow++;
                            }
                        }

                        for (int ArrCol = bump; ArrCol < aFCWithCasePrice.GetLength(1); ArrCol++)
                        {
                            decimal FGPerCost = 0.0m;
                            for (int ArrRow = 0; ArrRow < aFCWithCasePrice.GetLength(0); ArrRow++)
                            {
                                decimal componentQuantity = Convert.ToDecimal(aFCWithCasePrice[ArrRow, 2].ToString().Trim() == "" ? "0" : aFCWithCasePrice[ArrRow, 2].ToString().Trim());
                                decimal costPercentage = Convert.ToDecimal(aFCWithCasePrice[ArrRow, 3].ToString().Trim() == "" ? "0" : aFCWithCasePrice[ArrRow, 3].ToString().Trim());
                                decimal CompCost = Math.Round((((Convert.ToDecimal(aFCWithCasePrice[ArrRow, ArrCol].ToString().Trim()) * componentQuantity) / 100) * costPercentage), 4);
                                if (Convert.ToDecimal(aFCWithCasePrice[ArrRow, ArrCol].ToString().Trim()) == 0m) CompCost = 0m;
                                //if (ArrCol == bump) CompCost = Convert.ToDecimal(aFCWithCasePrice[ArrRow, 4].ToString().Trim());
                                aFCWithCasePrice[ArrRow, ArrCol] = CompCost.ToString();
                                FGPerCost += CompCost;
                            }
                            FGCost[ArrCol - bump] = decimal.Round(FGPerCost, 4).ToString();

                        }

                        ReportLib.PrintResult(driver, test, "Cost Summary page");
                        CDCAppDriver.CDCReadCostSummaryWithComp(driver, test, ItemNumber, TestWSI.ToString(), FGCost, aFCWithCasePrice, "Test Comments", syncTime);

                        //Check UOM
                        //DataTable UserCatWrinUOMTable = SOAPLib.GetUomConversionManager(WRIN.ToString());

                        // Press Submit button
                        ReportLib.PrintResult(test, "<h5>STEP 10: Press 'SUBMIT' button</h5>");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.SubmitButton, syncTime);

                        //Check and Close Confirmation Massage
                        ReportLib.PrintResult(test, "<h5>STEP 11: Check Confirmation Massage</h5>");
                        CDCAppDriver.CloseConfMassage(driver, test, syncTime);

                        //Batch Confirmation Submited Page
                        ReportLib.PrintResult(test, "<h5>STEP 12: Check Batch Submited Page</h5>");
                        CDCAppDriver.CDCBatchSubmitedPage(driver, test, sBatchNumber, syncTime);
                        ReportLib.PrintResult(driver, test, "Batch Submited Page");

                        // Press Submit button
                        ReportLib.PrintResult(test, "<h5>STEP 13: Press 'GO TO HOME' button</h5>");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.ConfirmMassagePage.GoToHomeButton, syncTime);

                        //Check batch number in the batch table
                        ReportLib.PrintResult(test, "<h5>STEP 14: Verify submited batch</h5>");
                        CDCAppDriver.CDCCheckBatchExists(driver, test, sBatchNumber, syncTime);

                        // Check review batch page

                        ReportLib.PrintResult(driver, test, "Check review batch page");
                        CDCAppDriver.CDCCheckReviewBatch(driver, test, sBatchNumber, BOMCostArray, syncTime);

                        CDCAppDriver.SyncAndPress(driver, CFORLib.ReviewBatch.ViewSummaryButton, syncTime);

                        //CDCAppDriver.CDCReviewCostSummary(driver, test, "BISCUIT", FGCost, "Test Comments", syncTime);
                        CDCAppDriver.CDCReviewCostSummaryWithComp(driver, test, ItemNumber, TestWSI.ToString(), FGCost, aFCWithCasePrice, "Test Comments", syncTime);

                        ReportLib.PrintResult(driver, test, "Back to Home Page");
                        CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.GoHomeButton, syncTime);
                    }
                    
                    //return;
                }
                finally
                {
                    AfterTest();
                }
            }

            [Test]
            [TestCaseSource("BrowserToRun")]
            public void SaveAndContinue(string BrowserName)
            {
                BeforeTest(BrowserName);

                //Login into HAVI - CDC system
                string sUserID = "CDC_demo_quaker1@havi.com";
                string sUserPsw = "Zy660n!g8zz";

                ReportLib.PrintResult(test, "<h5>STEP 1: Login into HAVI - Cost Data Capture</h5>");
                CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);

                //Open CREATE NEW BATCH page
                ReportLib.PrintResult(test, "<h5>STEP 2: Click 'CREATE NEW BATCH' button</h5>");
                CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);

                //Fill New Batch
                ReportLib.PrintResult(test, "<h5>STEP 3: Fill New Batch page</h5>");
                string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, "", "", "AST and OPS Test", syncTime);
                ReportLib.PrintResult(driver, test, "Fill New Batch page");

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 4: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                ReportLib.PrintResult(test, "Batch #: + sBatchNumber");

                ReportLib.PrintResult(driver, test, "Back to Home Page");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.GoHomeButton, syncTime);
                //******************************************************
                // Check 'Cost Projection Page' button
                ReportLib.PrintResult(test, "Press 'Cost Projection' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.CostProjectionPageButton, syncTime);
                //******************************************************
                ReportLib.PrintResult(test, "Press 'Edit' button on Batch#: " + sBatchNumber.ToString());
                CDCAppDriver.CDCClickEditBatch(driver, test, sBatchNumber, syncTime);

                ReportLib.PrintResult(driver, test, "Press 'Save And Continue' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                // Select Finish Goods
                ReportLib.PrintResult(test, "<h5>STEP 5: Select Finish Good from the list</h5>");
                CDCAppDriver.CDCSelectFinishGoods(driver, test, "03682015", syncTime);
                ReportLib.PrintResult(driver, test, "Select Finish Good");

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 6: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);
                //////////////////////////////////////////////////////////////////////////////////
                ReportLib.PrintResult(driver, test, "Back to Home Page");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.GoHomeButton, syncTime);
                //******************************************************
                // Check 'Cost Projection Page' button
                ReportLib.PrintResult(test, "Press 'Cost Projection' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.CostProjectionPageButton, syncTime);
                //******************************************************
                ReportLib.PrintResult(test, "Press 'Edit' button on Batch#: " + sBatchNumber.ToString());
                CDCAppDriver.CDCClickEditBatch(driver, test, sBatchNumber, syncTime);

                ReportLib.PrintResult(driver, test, "Press 'Save And Continue' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 6: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);
                //////////////////////////////////////////////////////////////////////////////////

                //Enter Cost Forcast
                ReportLib.PrintResult(test, "<h5>STEP 7: Fill Cost Forcast page</h5>");
                CDCAppDriver.CDCFillCostForecast(driver, test, "GRITS", aFCost, "Test Comments", syncTime);
                ReportLib.PrintResult(driver, test, "Fill Cost Forecast page");

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 8: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                //////////////////////////////////////////////////////////////////////////////////
                ReportLib.PrintResult(driver, test, "Back to Home Page");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.GoHomeButton, syncTime);
                //******************************************************
                // Check 'Cost Projection Page' button
                ReportLib.PrintResult(test, "Press 'Cost Projection' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.CostProjectionPageButton, syncTime);
                //******************************************************
                ReportLib.PrintResult(test, "Press 'Edit' button on Batch#: " + sBatchNumber.ToString());
                CDCAppDriver.CDCClickEditBatch(driver, test, sBatchNumber, syncTime);

                ReportLib.PrintResult(driver, test, "Press 'Save And Continue' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 6: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 8: Press 'Save And Continue' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                //////////////////////////////////////////////////////////////////////////////////
                //Read Cost Summary Page
                ReportLib.PrintResult(test, "<h5>STEP 9: Review Cost Summary page</h5>");
                CDCAppDriver.CDCReadCostSummary(driver, test, "GRITS", "GRITS", aFCost, "Test Comments 123", syncTime);
                ReportLib.PrintResult(driver, test, "Cost Summary page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 10: Press 'SUBMIT' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.SubmitButton, syncTime);

                //Check and Close Confirmation Massage
                ReportLib.PrintResult(test, "<h5>STEP 11: Check Confirmation Massage</h5>");
                CDCAppDriver.CloseConfMassage(driver, test, syncTime);

                //Batch Confirmation Submited Page
                ReportLib.PrintResult(test, "<h5>STEP 12: Check Batch Submited Page</h5>");
                CDCAppDriver.CDCBatchSubmitedPage(driver, test, sBatchNumber, syncTime);
                ReportLib.PrintResult(driver, test, "Batch Submited Page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 13: Press 'GO TO HOME' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.ConfirmMassagePage.GoToHomeButton, syncTime);

                //Check batch number in the batch table
                ReportLib.PrintResult(test, "<h5>STEP 14: Verify submited batch</h5>");
                CDCAppDriver.CDCCheckBatchExists(driver, test, sBatchNumber, syncTime);

                AfterTest();

            }
            [Test]
            [TestCaseSource("BrowserToRun")]
            public void CheckCLApproval(string BrowserName)
            {
                BeforeTest(BrowserName);

                //Login into HAVI - CDC system
                string sUserID = "CDC_demo_bama1@havi.com";
                string sUserPsw = "Zy660n!g8zz";

                ReportLib.PrintResult(test, "<h5>STEP 1: Login into HAVI - Cost Data Capture</h5>");
                CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);

                //Open CREATE NEW BATCH page
                ReportLib.PrintResult(test, "<h5>STEP 2: Click 'CREATE NEW BATCH' button</h5>");
                CDCAppDriver.CDCOpenCreateNewBatch(driver, test, syncTime);

                //Fill New Batch
                ReportLib.PrintResult(test, "<h5>STEP 3: Fill New Batch page</h5>");
                string sBatchNumber = CDCAppDriver.CDCFillNewBatch(driver, test, "", "", "Baked Goods", syncTime);
                ReportLib.PrintResult(driver, test, "Fill New Batch page");
                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 4: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.NewBatchPage.SaveAndContinueButton, syncTime);

                sBatchNumber = CDCAppDriver.GetBatchNumber(driver, syncTime);
                ReportLib.PrintResult(test, "Batch #:" + sBatchNumber);

                // Select Finish Goods
                ReportLib.PrintResult(test, "<h5>STEP 5: Select Finish Good from the list</h5>");
                CDCAppDriver.CDCSelectFinishGoods(driver, test, "02682021", syncTime);
                ReportLib.PrintResult(driver, test, "Select Finish Good");
                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 6: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.FinishGoodsPage.SaveAndContinueButton, syncTime);

                //Enter Cost Forcast
                ReportLib.PrintResult(test, "<h5>STEP 7: Fill Cost Forcast page</h5>");
                aFCost = CDCAppDriver.CDCFillCostForecast(driver, test, "BISCUIT", aFCost, "", syncTime);
                ReportLib.PrintResult(driver, test, "Fill Cost Forecast page");

                // Press Next Step button
                ReportLib.PrintResult(test, "<h5>STEP 8: Press 'NEXT STEP' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostForcastPage.SaveAndContinueButton, syncTime);

                //Read Cost Summar Page
                ReportLib.PrintResult(test, "<h5>STEP 9: Review Cost Summary page</h5>");
                CDCAppDriver.CDCReadCostSummary(driver, test, "BISCUIT", "BISCUIT", aFCost, "", syncTime);
                ReportLib.PrintResult(driver, test, "Cost Summary page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 10: Press 'SUBMIT' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.SubmitButton, syncTime);

                //Check and Close Confirmation Massage
                ReportLib.PrintResult(test, "<h5>STEP 11: Check Confirmation Massage</h5>");
                CDCAppDriver.CloseConfMassage(driver, test, syncTime);

                //Batch Confirmation Submited Page
                ReportLib.PrintResult(test, "<h5>STEP 12: Check Batch Submited Page</h5>");
                CDCAppDriver.CDCBatchSubmitedPage(driver, test, sBatchNumber, syncTime);
                ReportLib.PrintResult(driver, test, "Batch Submited Page");

                // Press Submit button
                ReportLib.PrintResult(test, "<h5>STEP 13: Press 'GO TO HOME' button</h5>");
                CDCAppDriver.SyncAndPress(driver, CFORLib.ConfirmMassagePage.GoToHomeButton, syncTime);

                //Check batch number in the batch table
                ReportLib.PrintResult(test, "<h5>STEP 14: Verify submited batch</h5>");
                CDCAppDriver.CDCCheckBatchExists(driver, test, sBatchNumber, syncTime);
                ReportLib.PrintResult(driver, test, "Back to Home Page");

                ////////////////////////////////////////////////////////////////////////////
                //Login into HAVI - CDC system
                sUserID = "CDC_demo_categorylead1@havi.com";
                sUserPsw = "Zy660n!g8zz";

                driver.FindElement(By.ClassName("app-logout-button")).Click();
                driver.Close();
                Thread.Sleep(TimeSpan.FromSeconds(1));

                BeforeTest(BrowserName);
                //Login into HAVI - CDC system
                ReportLib.PrintResult(test, "<h5>STEP1: Login into HAVI - Cost Data Capture</h5>");

                CDCAppDriver.CDCLogin(driver, test, sUserID, sUserPsw, syncTime);

                //******************************************************
                // Check 'Cost Projection Page' button
                ReportLib.PrintResult(test, "Press 'Cost Projection' button");
                CDCAppDriver.SyncAndPress(driver, CFORLib.HomePage.CostProjectionPageButton, syncTime);
                //******************************************************
                ReportLib.PrintResult(test, "Press 'Edit' button on Batch#: " + sBatchNumber.ToString());
                //CDCAppDriver.CDCClickEditBatch(driver, test, sBatchNumber, syncTime);

                //Build Expected results
                string[,] aCompCost = new string[1, 24];
                aCompCost[0, 0] = "BISCUIT";
                aCompCost[0, 1] = "";
                aCompCost[0, 2] = "";
                aCompCost[0, 3] = "";
                //SOAPLib SOAPLib = new SOAPLib();
                int start = 5;
                for (int CompRow = start; CompRow < aCompCost.GetLength(1); CompRow++)
                {
                    aCompCost[0, CompRow] = aFCost[CompRow - start];
                }

                ReportLib.PrintResult(driver, test, "Check review batch page");
                CDCAppDriver.CDCCheckReviewBatch(driver, test, sBatchNumber, aCompCost, syncTime);

                CDCAppDriver.SyncAndPress(driver, CFORLib.ReviewBatch.ViewSummaryButton, syncTime);

                CDCAppDriver.CDCReviewCostSummary(driver, test, "BISCUIT", aFCost, "", syncTime);

                ReportLib.PrintResult(driver, test, "Back to Home Page");
                CDCAppDriver.SyncAndPress(driver, CFORLib.CostSummaryPage.CancelButton, syncTime);

                AfterTest();

            }
            [Test]
            public void CheckJDEComponents()
            {
                SOAPLib SOAPLib = new SOAPLib();

                DataTable WSIDataTable = SOAPLib.GetListOfWSI();
                var IWSIDataTable = (from DataRow dRow in WSIDataTable.Rows
                                     select dRow["WSI"]);
                foreach (int WSINum in IWSIDataTable)
                {
                    //Console.WriteLine("WSI: " + Convert.ToString(WSINum));
                    DataTable SupplierLineOfSupply = SOAPLib.GetSupplierLineOfSupply(WSINum);
                    DataTable SupplierBOM = SOAPLib.GetSupplierBOM(WSINum);
                    //itemNumber
                    var distinctRows = (from DataRow dRow in SupplierBOM.Rows
                                        select dRow["parentItemNumber"]).Distinct();

                    foreach (string row in distinctRows)
                    {
                        string Row1 = row.ToString();
                        //var rowsWithTheValue = SupplierLineOfSupply.Rows.Where(a => a["Column"] == value);
                        var ResultLOS = SupplierLineOfSupply.Select("itemNumber=" + Row1).Length > 0;
                        Console.WriteLine(String.Format("{0}\t{1}\t{2}", WSINum, Row1, ResultLOS));
                    }
                }
                Console.ReadLine();

                AfterTest();

                return;
            }
        }
        
        //[OneTimeTearDown]
        public void AfterTest()
        {
            ClosePage();

            extent.Flush();
            Thread.Sleep(TimeSpan.FromSeconds(2));

            ChromeOptions optionsReport = new ChromeOptions();
            optionsReport.AddArguments("start-maximized");
            driver = new ChromeDriver(optionsReport);
            driver.Navigate().GoToUrl(Settings.Report.ReportName);

        }

        //[TearDown]
        public void ClosePage()
        {
            //driver.Close();
            //driver.Quit();
            //ARTSReport.PrintResult(test, "Logout Successful");
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            if (logstatus == Status.Fail) test.Log(logstatus, "Test ended with " + logstatus + stacktrace, MediaEntityBuilder.CreateScreenCaptureFromPath(ReportLib.GetScreenshot(driver)).Build());
            else test.Log(logstatus, "Test ended with " + logstatus + stacktrace);

            test = null;

            // calling flush writes everything to the log file

        }
    }
}
