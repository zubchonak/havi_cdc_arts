﻿using System;
using System.IO;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
//using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
//using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.Generic;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace HAVITestProject
{
    public class CDCAppDriver
    {
        ReportLib ReportLib = new ReportLib();
        ExcelLib ExcelLib = new ExcelLib();
        GlobalLib GlobalLib = new GlobalLib();

        int SleepTime = 2000;


        public static void CDCLogin(IWebDriver driverChrome, ExtentTest test, string UserName, string Pswd, int syncTime)
        {
            int SleepTime = 2000;
            ReportLib ReportLib = new ReportLib();

            //mouseX = Me.Location.X + Button1.Location.X + Button1.Size.Width
            //mouseY = Me.Location.Y + Button1.Location.Y + Button1.Size.Height

            //Cursor.Position = (New System.Drawing.Point(mouseX, mouseY - speed))
            //Threading.Thread.Sleep(1000)
            //Cursor.Position = (New System.Drawing.Point(mouseX, mouseY))


            var wait1 = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement1 = wait1.Until(ExpectedConditions.ElementToBeClickable(CFORLib.LoginPage.PL_LoginButton));
            clickableElement1.Click();

            ReportLib.PrintResult(test, "Type User ID '" + UserName + "'");
            driverChrome.FindElement(CFORLib.LoginPage.UserName).SendKeys(UserName);

            ReportLib.PrintResult(test, "Type Password 'XXXXXXXXX'");
            driverChrome.FindElement(CFORLib.LoginPage.Password).SendKeys(Pswd);

            Thread.Sleep(SleepTime);
            ReportLib.PrintResult(test, "Press LogIn button");

            SyncAndPress(driverChrome, CFORLib.LoginPage.LoginButton, syncTime);


            if (GlobalLib.IsElementPresent(driverChrome, CFORLib.LoginPage.Massage))
            {
                var element = driverChrome.FindElement(CFORLib.LoginPage.Massage);
                if (element.Text == "Stay signed in?")
                {
                    ReportLib.PrintResult("PASS", test, "'Stay signed in?' massage appear");
                    Thread.Sleep(SleepTime);
                    SyncAndPress(driverChrome, CFORLib.LoginPage.LoginButton, syncTime);

                    //Actions builder = new Actions(driverChrome);
                    //builder.SendKeys(Keys.Enter);
                }
            }

            Thread.Sleep(3000);
            //Check 'Cost Data Capture' Title
            String sTitle = driverChrome.Title;
            Assert.AreEqual(sTitle, "Category Spend & Analytics");
            if (sTitle == "Category Spend & Analytics")
            {
                ReportLib.PrintResult("PASS", test, "'Category Spend & Analytics' page appear");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'Category Spend & Analytics' page does not appear");
            }


        }

        public void CDCOpenCreateNewBatch(IWebDriver driverChrome, ExtentTest test, int syncTime)
        {
            //******************************************************
            // Check 'Cost Projection Page' button
            ReportLib.PrintResult(test, "Press 'Cost Projection' button");
            CDCAppDriver.SyncAndPress(driverChrome, CFORLib.HomePage.CostProjectionPageButton, syncTime);
            //******************************************************

            // Check 'CREATE NEW BATCH' button
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(CFORLib.HomePage.CreateNewBatchButton));
            string NewBatchLable = clickableElement.Text;

            Assert.AreEqual("CREATE NEW BATCH", NewBatchLable);
            if ("CREATE NEW BATCH" == NewBatchLable)
            {
                ReportLib.PrintResult("PASS", test, "'CREATE NEW BATCH' button exists");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'CREATE NEW BATCH' button does not exists");
            }
            //Check Main Batch Table
            wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var HistoryTable = wait.Until(ExpectedConditions.ElementExists(CFORLib.HomePage.CreateNewBatchButton));
            ReportLib.PrintResult(driverChrome, test, "CREATE NEW BATCH page");
            //Click 'CREATE NEW BATCH' button
            ReportLib.PrintResult("Click 'CREATE NEW BATCH' button");
            clickableElement.Click();

            //Check Header Page button
            ReportLib.PrintResult("Check 'Header Page' button lable");
            wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(CFORLib.HomePage.HeaderPageButton));
            string HeaderPageLable = clickableElement.Text;
            //Assert.AreEqual(HeaderPageLable, "Header Page");
            if (HeaderPageLable.Contains("Header"))
            {
                ReportLib.PrintResult("PASS", test, "'Header Page' lable match");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'" + HeaderPageLable + "' <> 'Header Page' lable mismatch");
            }


        }

        public string CDCFillNewBatch(IWebDriver driverChrome, ExtentTest test, string sBatchName, string sBatchDesc, string BatchCat, int syncTime)
        {
            //***********************************************************
            //Check and Get Batch Number
            ReportLib.PrintResult(test, "Check and Get Batch Number");

            
                     

            //Get Effective Date
            string sEffectiveDate = driverChrome.FindElement(CFORLib.NewBatchPage.EffectiveDate).GetAttribute("value");
            ReportLib.PrintResult(test, "Effective Date: " + sEffectiveDate);

            if (!driverChrome.FindElement(CFORLib.NewBatchPage.SaveAndContinueButton).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'NEXT STEP' button by default is disabled");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'NEXT STEP' button by default is enabled");
            }

            //Enter Batch Name
            string BatchExtName = DateTime.Now.ToString("MMddHHmmss");
            if (sBatchName == "") sBatchName = "Test Batch " + BatchExtName;
            ReportLib.PrintResult(test, "Enter Batch Name: " + sBatchName);
            driverChrome.FindElement(CFORLib.NewBatchPage.BatchName).SendKeys(sBatchName);
            Thread.Sleep(SleepTime);
            if (!driverChrome.FindElement(CFORLib.NewBatchPage.SaveAndContinueButton).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'NEXT STEP' button is disabled with Batch Name entered");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'NEXT STEP' button is enabled with Batch Name entered");
            }
            //Enter Batch Description
            if (sBatchDesc == "") sBatchDesc = "Test Description " + BatchExtName;
            ReportLib.PrintResult(test, "Enter Batch Description: " + sBatchDesc);
            driverChrome.FindElement(CFORLib.NewBatchPage.BatchDesc).SendKeys(sBatchDesc);
            Thread.Sleep(SleepTime);
            if (driverChrome.FindElement(CFORLib.NewBatchPage.SaveAndContinueButton).Enabled)
            {
                ReportLib.PrintResult("FAIL", test, "'NEXT STEP' button is enabled with Batch Name and Batch Description entered");
            }
            else
            {
                ReportLib.PrintResult("PASS", test, "'NEXT STEP' button is disabled with Batch Name and Batch Description entered");
            }

            //Select Category
            IWebElement CatList = driverChrome.FindElement(CFORLib.NewBatchPage.BatchCategoryButton);
            CatList.Click();

            IWebElement dropdown = driverChrome.FindElement(CFORLib.NewBatchPage.CategoryItemList);
            IList<IWebElement> CatItemList = dropdown.FindElements(By.XPath(@"//*[@role='menuitem']"));
            string CatItemText = "";
            if (BatchCat == "")
            {
                CatItemText = CatItemList[0].Text;
                CatItemList[0].Click();
            }
            else
            {
                for (int item = 0; item < CatItemList.Count; item++)
                {
                    CatItemText = CatItemList[item].Text;
                    if (CatItemText.ToString() == BatchCat)
                    {
                        CatItemList[item].Click();
                        break;
                    }
                }
            }
            ReportLib.PrintResult(test, "Select Product Category: '" + CatItemText.ToString() + "'");

            Thread.Sleep(SleepTime);
            if (driverChrome.FindElement(CFORLib.NewBatchPage.SaveAndContinueButton).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'NEXT STEP' button is enabled with Batch Name, Description and Category entered");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'NEXT STEP' button is disabled with Batch Name, Description and Category entered");
            }

            //string sBatchNumberLable = driverChrome.FindElement(ORLib.NewBatchPage.BatchNumberLable).Text;
            string sBatchNumber = "0";
            //Assert.AreEqual(sBatchNumberLable, "Batch #:");
            //ReportLib.PrintResult(test, sBatchNumberLable + sBatchNumber);

            return (sBatchNumber);
        }

        public static void SyncAndPressByXPath(IWebDriver driverChrome, string xPath, int syncTime)
        {
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(xPath)));
            clickableElement.Click();

        }
        public static void SyncAndPress(IWebDriver driverChrome, By ObjName, int syncTime)
        {
            Thread.Sleep(1000);
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(ObjName));
                        
            Actions action = new Actions(driverChrome);
            
            action.MoveToElement(clickableElement, 5, 5).Click().Build().Perform();

            //clickableElement.Click();

        }
        public static string GetBatchNumber(IWebDriver driverChrome, int syncTime)
        {
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(CFORLib.NewBatchPage.BatchNumberLable));
            string sBatchNumberLable = clickableElement.Text;
            string sBatchNumber = driverChrome.FindElement(CFORLib.NewBatchPage.BatchNumber).Text;
            Assert.AreEqual(sBatchNumberLable, "Batch #:");
            

            return (sBatchNumber);
        }
        

        public static List<string> CDCSelectFinishGoods(IWebDriver driverChrome, ExtentTest test, string sFinishedGood, int syncTime)
        {
            bool found = false;
            if (sFinishedGood.Trim() == "") sFinishedGood = "02682018";

            List<string> OutWSIList = new List<string>();

            int SleepTime = 2000;
            ReportLib ReportLib = new ReportLib();


            Thread.Sleep(SleepTime);
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.FinishGoodsPage.FinishedGoodsTable));

            IList<IWebElement> FG_all_tables = driverChrome.FindElements(CFORLib.FinishGoodsPage.FinishedGoodsTable);
            IList<IWebElement> FG_table = FG_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowFGCell;

            ReportLib.PrintResult(test, "Finished Goods Row Count: " + FG_table.Count.ToString());

            foreach (IWebElement item in FG_table)
            {
                rowFGCell = item.FindElements(By.TagName("td"));
                IWebElement checkBox = item.FindElement(By.TagName("input"));
                if (checkBox.Enabled && rowFGCell[3].Text.Equals(sFinishedGood))
                {
                    //rowFGCell[0].Click();
                    //Actions actions = new Actions(driverChrome);
                    //actions..MoveToElement(rowFGCell[0]);
                    //actions.Perform();

                    checkBox.Click();
                    checkBox.SendKeys(Keys.Tab);
                    ReportLib.PrintResult(test, item.Text.ToString());
                    if (Convert.ToBoolean(checkBox.GetAttribute("Checked")))
                    {
                        ReportLib.PrintResult("Pass", test, "Product # / WRIN " + rowFGCell[3].Text.ToString() + " selected");
                    }
                    else
                    {
                        ReportLib.PrintResult("FAIL", test, "Product # / WRIN " + rowFGCell[3].Text.ToString() + " NOT selected");
                    }
                    found = true;
                    break;
                }
            }
            if (!found) Assert.Fail("Finish Good Product#: " + sFinishedGood + " not found");

            foreach (IWebElement item in FG_table)
            {
                rowFGCell = item.FindElements(By.TagName("td"));
                IWebElement checkBox = item.FindElement(By.TagName("input"));
                if (checkBox.Enabled && rowFGCell[3].Text.Equals(sFinishedGood))
                {
                    //rowFGCell[3].Text.Equals(sFinishedGood);
                    OutWSIList.Add(rowFGCell[1].Text);
                }

            }
            return ((from wsi in OutWSIList select wsi).Distinct().ToList());
            
        }

        public string[] CDCFillCostForecast(IWebDriver driverChrome, ExtentTest test, string sCompName, string[] aPrice, string sComments, int syncTime)
        {
            bool ClonePrice = false;
            String[] aFCost = new string[19];
            string OutputMsg = "";

            // Read and select Cost Forecast item
            if (aPrice.Length != aFCost.Length)
            {
                string sPrice = "2";

                aFCost[0] = sPrice;
                for (int i = 1; i < aFCost.Length; i++) aFCost[i] = sPrice;
            }
            else
            {
                aFCost = (string[])aPrice.Clone();
                ClonePrice = true;
            }
            Thread.Sleep(SleepTime);

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostForcastPage.CostForecastTable));

            if (!driverChrome.FindElement(By.Id(@"nxtStepBtn2")).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'NEXT STEP' button is disabled (Default)");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'NEXT STEP' button is enabled (Default)");
            }

            IList<IWebElement> CF_all_tables = driverChrome.FindElements(CFORLib.CostForcastPage.CostForecastTable);
            IList<IWebElement> CF_table_row = CF_all_tables[1].FindElements(By.TagName("table"));
            int RowToFill = 0;
            foreach (IWebElement item in CF_table_row)
            {
                if (item.Text.Contains(sCompName.Trim())) break;

                RowToFill++;
            }
            IList<IWebElement> CF_row = CF_table_row[RowToFill].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCFRow;

            OutputMsg = string.Concat(OutputMsg, "<br>", "Cost Forecast Row Count: " + CF_row.Count.ToString());
            //ReportLib.PrintResult(test, "Cost Forecast Row Count: " + CF_row.Count.ToString());

            foreach (IWebElement item in CF_row)
            {

                rowCFRow = item.FindElements(By.TagName("td"));
                //sActCompName1 = item.FindElement(By.ClassName("description")).Text;
                //sActCompName2 = rowCFRow[0].Text;
                if (rowCFRow.Count <= 1) continue;

                //if (sCompName != "" && !sActCompName1.Contains(sCompName)) continue;

                if (sComments != "")
                {
                    IWebElement lCommentLink = item.FindElement(By.CssSelector("[class='commentSvg']"));//commentTxt commentTxtPadding']"));
                    lCommentLink.Click();

                    item.FindElement(By.XPath("//*[contains(@id, 'componentComment')]")).SendKeys(sCompName + " " + sComments.ToString());

                    ReportLib.PrintResult(driverChrome, test, "Enter Component Comments: " + sCompName + " " + sComments.ToString());

                    //driverChrome.FindElement(ORLib.CommentsWindow.SaveButton).Click();
                    driverChrome.FindElement(By.XPath("/html/body/div[" + (RowToFill + 2).ToString() + "]/ div/div[1]/div/div/div[2]/button[2]")).Click();

                }
                string[] CurPriceArray = item.Text.ToString().Split(new Char[] { '$' });
                string CurPrice = "2";
                if (CurPriceArray[CurPriceArray.Length - 1] != "0.00")
                {
                    CurPrice = CurPriceArray[CurPriceArray.Length - 1];
                    aFCost[0] = CurPrice;
                }
                else
                    aFCost[0] = "0.00";

                IList<IWebElement> inputBox = item.FindElements(By.TagName("input"));
                int counter = 1;
                foreach (IWebElement TextItem in inputBox)
                {
                    if (TextItem.Enabled)
                    {
                        if (TextItem.GetAttribute("value") != "") TextItem.Clear();

                        if (!ClonePrice) aFCost[counter] = (Convert.ToDecimal(CurPrice) - (0.01m + (Convert.ToDecimal(counter) / 10000))).ToString();
                        //if (!ClonePrice) aFCost[counter] = (Convert.ToDecimal(CurPrice)).ToString();

                        TextItem.SendKeys(aFCost[counter] + Keys.Tab);

                        counter++;
                    }

                }
                OutputMsg = string.Concat(OutputMsg, "<br>", rowCFRow[0].Text.ToString());
                OutputMsg = string.Concat(OutputMsg, "<br>", "Used Default Price " + aFCost[counter - 1]);
                //ReportLib.PrintResult(test, "Used Default Price " + aFCost[counter - 1]);
                //ReportLib.PrintResult(test, rowCFRow[0].Text.ToString());

            }

            if (driverChrome.FindElement(CFORLib.CostForcastPage.SaveAndContinueButton).Enabled)
            {
                OutputMsg = string.Concat(OutputMsg, "<br>", "'NEXT STEP' button is enabled");
                //ReportLib.PrintResult(test, "'NEXT STEP' button is enabled");
            }
            else
            {
                OutputMsg = string.Concat(OutputMsg, "<br>", "'NEXT STEP' button is disabled");
                //ReportLib.PrintResult(test, "'NEXT STEP' button is disabled");
            }

            ReportLib.PrintResult(test, OutputMsg);

            return (aFCost);
        }
        public void CDCReadCostSummary(IWebDriver driverChrome, ExtentTest test, string sCompName, string sWSI, string[] aPrice, string sComments, int syncTime)
        {
            String[] aFCost = new string[19];
            // Read and select Cost Forecast item
            if (aPrice.Length != aFCost.Length)
            {
                string sPrice = "12";
                ReportLib.PrintResult(test, "Used Default Price " + sPrice);
                aFCost[0] = "17.91";
                for (int i = 1; i < aFCost.Length; i++) aFCost[i] = sPrice;
            }
            else
            {
                aFCost = (string[])aPrice.Clone();
            }
            Thread.Sleep(SleepTime);

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostSummaryPage.CostSummaryTable));

            if (driverChrome.FindElement(CFORLib.CostSummaryPage.SubmitButton).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'SUBMIT' button is enabled (Default)");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'SUBMIT' button is disabled (Default)");
            }

            // Read Cost Summary table
            IList<IWebElement> CS_all_tables = driverChrome.FindElements(CFORLib.CostSummaryPage.CostSummaryTable);
            IList<IWebElement> CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCSCell;

            ReportLib.PrintResult(test, "Cost Summary table row count: " + CS_table.Count.ToString());
            bool flagHeader = true;
            bool status = true;
            int CelCounter = 0;
            string outText;
            string sExpText = "";
            foreach (IWebElement tRow in CS_table)
            {
                rowCSCell = tRow.FindElements(By.TagName("td"));
                outText = "";
                sExpText = "";
                if (rowCSCell[0].Text.Contains(sCompName) && rowCSCell[0].Text.Contains(sWSI) && rowCSCell.Count == 22 && flagHeader)
                {
                    string[] ActFGCost = new string[rowCSCell.Count];
                    int counter = 0;
                    foreach (IWebElement sCell in rowCSCell)
                    {

                        sCell.Click();
                        ActFGCost[counter++] = sCell.Text;

                    }

                    if (ActFGCost[0].Contains(sCompName))
                        ReportLib.PrintResult("PASS", test, "Product '" + sCompName + "' match");
                    else
                        ReportLib.PrintResult("FAIL", test, "Product '" + sCompName + "' <> '" + ActFGCost[0].ToString() + "' missmatch");
                    string OutStatus = "FAIL";
                    counter = 1;
                    for (int itemNum = ActFGCost.Length - 19; itemNum < ActFGCost.Length; itemNum++)
                    {

                        outText = String.Format("{0:C}", Convert.ToDecimal(aFCost[CelCounter++]));


                        if (ActFGCost[itemNum].Contains(outText))
                        {
                            if (status) status = true;
                            OutStatus = "<font color='green'>PASS</font>";
                        }
                        else
                        {
                            status = false;
                            OutStatus = "<font color='red'>FAIL</font>";
                        }

                        sExpText = string.Concat(sExpText, " <br/> ", outText + ":" + OutStatus);

                        counter++;

                        flagHeader = false;
                    }
                    if (status && sExpText != "")
                    {

                        ReportLib.PrintResult("PASS", test, sExpText);
                    }
                    else
                        ReportLib.PrintResult("FAIL", test, sExpText);

                    flagHeader = false;
                }
            }
            if (sComments != "")
            {
                driverChrome.FindElement(CFORLib.CostSummaryPage.BatchComment).SendKeys(sComments.ToString());
                ReportLib.PrintResult(driverChrome, test, "Enter Review Cost Summary page comments: '" + sComments.ToString() + "'");
            }

        }
        public void CDCReadCostSummaryWithComp(IWebDriver driverChrome, ExtentTest test, string sCompName, string sWSI, string[] aPrice, string[,] aCompPrice, string sComments, int syncTime)
        {
            String[] aFCost = new string[19];
            // Read and select Cost Forecast item
            if (aPrice.Length != aFCost.Length)
            {
                string sPrice = "12";
                ReportLib.PrintResult(test, "Used Default Price " + sPrice);
                aFCost[0] = "17.91";
                for (int i = 1; i < aFCost.Length; i++) aFCost[i] = sPrice;
            }
            else
            {
                aFCost = (string[])aPrice.Clone();
            }
            Thread.Sleep(SleepTime);

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostSummaryPage.CostSummaryTable));

            if (driverChrome.FindElement(CFORLib.CostSummaryPage.SubmitButton).Enabled)
            {
                ReportLib.PrintResult("PASS", test, "'SUBMIT' button is enabled (Default)");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'SUBMIT' button is disabled (Default)");
            }

            // Read Cost Summary table
            IList<IWebElement> CS_all_tables = driverChrome.FindElements(CFORLib.CostSummaryPage.CostSummaryTable);
            IList<IWebElement> CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCSCell;
            IList<IWebElement> CopmLink;

            ReportLib.PrintResult(test, "Cost Summary table row count: " + CS_table.Count.ToString());
            bool flagHeader = true;
            bool status = true;
            int CelCounter = 0;
            string outText;
            string sExpText = "";
            bool SkeepCurrentPrice = true;
            string FailOpen = "<strong style='color: red;'>";
            string FailClose = "</strong>";

            foreach (IWebElement tRow in CS_table)
            {
                rowCSCell = tRow.FindElements(By.TagName("td"));
                outText = "";
                sExpText = "";
                if (rowCSCell[0].Text.Contains(sCompName) && rowCSCell[0].Text.Contains(sWSI) && rowCSCell.Count == 22 && flagHeader)
                {
                    string[] ActFGCost = new string[rowCSCell.Count];
                    int counter = 0;
                    foreach (IWebElement sCell in rowCSCell)
                    {
                        
                        ActFGCost[counter++] = sCell.Text;

                    }

                    CopmLink = rowCSCell[0].FindElements(By.TagName("a"));
                    CopmLink[0].Click();

                    if (ActFGCost[0].Contains(sCompName))
                        ReportLib.PrintResult("PASS", test, "Product '" + sCompName + "' match");
                    else
                        ReportLib.PrintResult("FAIL", test, "Product '" + sCompName + "' <> '" + ActFGCost[0].ToString() + "' missmatch");
                    string OutStatus = "FAIL";
                    string OutPrice = "";
                    counter = 1;
                                        
                    for (int itemNum = ActFGCost.Length - 19; itemNum < ActFGCost.Length; itemNum++)
                    {

                        outText = String.Format("{0:C}", Convert.ToDecimal(aFCost[CelCounter++]));

                        OutPrice = ActFGCost[itemNum];
                        
                        if (OutPrice.Contains(outText))
                        {
                            if (status) status = true;
                            OutStatus = "PASS";
                            FailOpen = "";
                            FailClose = "";
                        }
                        else
                        {
                            if (SkeepCurrentPrice && (counter - 1) == 0) status = true; else status = false;
                            FailOpen = "<strong style='color: red;'>";
                            FailClose = "</strong>";
                            OutStatus = "FAIL";
                        }
                        string[] words = Regex.Split(OutPrice, "\r\n");

                        sExpText = String.Format("{0}{5}{4}: ACT:{1} EXP:{2} :: {3}{6}<br>", sExpText, words[0], outText, OutStatus, counter-1, FailOpen, FailClose);

                        counter++;

                        flagHeader = false;
                    }
                    if (status && sExpText != "")
                    {

                        ReportLib.PrintResult("PASS", test, ActFGCost[1].ToString() + "<br>" + sExpText);
                    }
                    else
                        ReportLib.PrintResult("FAIL", test, ActFGCost[1].ToString() + "<br>" + sExpText);

                    
                    ReportLib.PrintResult(driverChrome, test, "Batch Cost Summary");

                    flagHeader = false;
                }
            }
            /// Check components
            ///             

            CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            CelCounter = 0;
            for (int expRowComp = 0; expRowComp < aCompPrice.GetLength(0); expRowComp++)
            {
                sExpText = "";
                foreach (IWebElement tRow in CS_table)
                {
                    rowCSCell = tRow.FindElements(By.TagName("td"));
                    rowCSCell[0].Click();

                    if (rowCSCell[0].Text.Contains(aCompPrice[expRowComp, 0]) && rowCSCell.Count == 22)
                    {
                        string[] ActFGCost = new string[rowCSCell.Count];
                        int counter = 0;
                        foreach (IWebElement sCell in rowCSCell)
                        {

                            ActFGCost[counter++] = sCell.Text;

                        }

                        string OutStatus = "FAIL";

                        counter = 5; //Same as bump from ARTS script
                        status = true;
                        CelCounter = 3;
                        for (int itemNum = ActFGCost.Length - 19; itemNum < ActFGCost.Length; itemNum++)
                        {

                            outText = String.Format("{0:C}", Convert.ToDecimal(aCompPrice[expRowComp, counter]));

                            string OutPrice = ActFGCost[CelCounter];
                            string[] words = Regex.Split(OutPrice, "\r\n");
                                                

                            if (OutPrice.Contains(outText))
                            {
                                if (status) status = true;
                                OutStatus = "PASS";
                                FailOpen = "";
                                FailClose = "";
                            }
                            else
                            {
                                if (SkeepCurrentPrice && (counter - 1) == 0) status = true; else status = false;
                                FailOpen = "<strong style='color: red;'>";
                                FailClose = "</strong>";
                                OutStatus = "FAIL";
                            }

                            //sExpText = string.Concat(sExpText, "<br>", outText + ":" + OutStatus);
                            sExpText = String.Format("{0}{5}{4}: ACT:{1} EXP:{2} :: {3}{6}<br>", sExpText, words[0], outText, OutStatus, counter - 5, FailOpen, FailClose);

                            counter++;
                            CelCounter++;

                            flagHeader = false;
                        }
                        if (status && sExpText != "")
                        {
                            ReportLib.PrintResult("PASS", test, "CMPT: " + aCompPrice[expRowComp, 0] + ":: <br>" + sExpText);
                        }
                        else
                        {
                            ReportLib.PrintResult("FAIL", test, "CMPT: " + aCompPrice[expRowComp, 0] + ":: <br>" + sExpText);
                        }
                        
                        break;
                    }
                }
            }

            if (sComments != "")
            {
                driverChrome.FindElement(CFORLib.CostSummaryPage.BatchComment).SendKeys(sComments.ToString());
                ReportLib.PrintResult(driverChrome, test, "Enter Review Cost Summary page comments: '" + sComments.ToString() + "'");
            }

        }
        public void CDCReviewCostSummaryWithComp(IWebDriver driverChrome, ExtentTest test, string sCompName, string sWSI, string[] aPrice, string[,] aCompPrice, string sComments, int syncTime)
        {
            String[] aFCost = new string[19];
            // Read and select Cost Forecast item
            if (aPrice.Length != aFCost.Length)
            {
                string sPrice = "12";
                ReportLib.PrintResult(test, "Used Default Price " + sPrice);
                aFCost[0] = "17.91";
                for (int i = 1; i < aFCost.Length; i++) aFCost[i] = sPrice;
            }
            else
            {
                aFCost = (string[])aPrice.Clone();
            }
            Thread.Sleep(SleepTime);

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostSummaryPage.CostSummaryTable));
            
            // Read Cost Summary table
            IList<IWebElement> CS_all_tables = driverChrome.FindElements(CFORLib.CostSummaryPage.CostSummaryTable);
            IList<IWebElement> CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCSCell;
            IList<IWebElement> CopmLink;

            ReportLib.PrintResult(test, "Cost Summary table row count: " + CS_table.Count.ToString());
            bool flagHeader = true;
            bool status = true;
            int CelCounter = 0;
            string outText;
            string sExpText = "";
            bool SkeepCurrentPrice = true;
            string FailOpen = "<strong style='color: red;'>";
            string FailClose = "</strong>";

            foreach (IWebElement tRow in CS_table)
            {
                rowCSCell = tRow.FindElements(By.TagName("td"));
                outText = "";
                sExpText = "";
                if (rowCSCell[0].Text.Contains(sCompName) && rowCSCell[0].Text.Contains(sWSI) && rowCSCell.Count == 22 && flagHeader)
                {
                    string[] ActFGCost = new string[rowCSCell.Count];
                    int counter = 0;
                    foreach (IWebElement sCell in rowCSCell)
                    {

                        ActFGCost[counter++] = sCell.Text;

                    }

                    CopmLink = rowCSCell[0].FindElements(By.TagName("a"));
                    CopmLink[0].Click();

                    if (ActFGCost[0].Contains(sCompName))
                        ReportLib.PrintResult("PASS", test, "Product '" + sCompName + "' match");
                    else
                        ReportLib.PrintResult("FAIL", test, "Product '" + sCompName + "' <> '" + ActFGCost[0].ToString() + "' missmatch");
                    string OutStatus = "FAIL";
                    string OutPrice = "";
                    counter = 1;
                    for (int itemNum = ActFGCost.Length - 19; itemNum < ActFGCost.Length; itemNum++)
                    {

                        outText = String.Format("{0:C}", Convert.ToDecimal(aFCost[CelCounter++]));

                        OutPrice = ActFGCost[itemNum];

                        if (OutPrice.Contains(outText))
                        {
                            if (status) status = true;
                            OutStatus = "PASS";
                            FailOpen = "";
                            FailClose = "";
                        }
                        else
                        {
                            status = false;
                            OutStatus = "FAIL";
                            FailOpen = "<strong style='color: red;'>";
                            FailClose = "</strong>";
                        }
                        string[] words = Regex.Split(OutPrice, "\r\n");

                        //sExpText = String.Format("{0}{4}: ACT:{1} EXP:{2} :: {3}<br>", sExpText, words[0], outText, OutStatus, counter - 1);
                        sExpText = String.Format("{0}{5}{4}: ACT:{1} EXP:{2} :: {3}{6}<br>", sExpText, words[0], outText, OutStatus, counter - 1, FailOpen, FailClose);

                        counter++;

                        flagHeader = false;
                    }
                    if (status && sExpText != "")
                    {

                        ReportLib.PrintResult("PASS", test, ActFGCost[1].ToString() + "<br>" + sExpText);
                    }
                    else
                        ReportLib.PrintResult("FAIL", test, ActFGCost[1].ToString() + "<br>" + sExpText);


                    ReportLib.PrintResult(driverChrome, test, "Batch Cost Summary");

                    flagHeader = false;
                }
            }
            /// Check components
            ///             

            CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            CelCounter = 0;
            for (int expRowComp = 0; expRowComp < aCompPrice.GetLength(0); expRowComp++)
            {
                sExpText = "";
                foreach (IWebElement tRow in CS_table)
                {
                    rowCSCell = tRow.FindElements(By.TagName("td"));
                    rowCSCell[0].Click();

                    if (rowCSCell[0].Text.Contains(aCompPrice[expRowComp, 0]) && rowCSCell.Count == 22)
                    {
                        string[] ActFGCost = new string[rowCSCell.Count];
                        int counter = 0;
                        foreach (IWebElement sCell in rowCSCell)
                        {

                            ActFGCost[counter++] = sCell.Text;

                        }

                        string OutStatus = "FAIL";

                        counter = 5;
                        status = true;
                        CelCounter = 3;
                        for (int itemNum = ActFGCost.Length - 19; itemNum < ActFGCost.Length; itemNum++)
                        {

                            outText = String.Format("{0:C}", Convert.ToDecimal(aCompPrice[expRowComp, counter]));

                            string OutPrice = ActFGCost[CelCounter];
                            string[] words = Regex.Split(OutPrice, "\r\n");

                            
                            if (OutPrice.Contains(outText))
                            {
                                if (status) status = true;
                                OutStatus = "PASS";
                                FailOpen = "";
                                FailClose = "";
                            }
                            else
                            {
                                if (SkeepCurrentPrice && (counter - 1) == 0) status = true; else status = false;
                                FailOpen = "<strong style='color: red;'>";
                                FailClose = "</strong>";
                                OutStatus = "FAIL";
                            }

                            //sExpText = string.Concat(sExpText, "<br>", outText + ":" + OutStatus);
                            sExpText = String.Format("{0}{5}{4}: ACT:{1} EXP:{2} :: {3}{6}<br>", sExpText, words[0], outText, OutStatus, counter - 4, FailOpen, FailClose);

                            counter++;
                            CelCounter++;

                            flagHeader = false;
                        }
                        if (status && sExpText != "")
                        {
                            ReportLib.PrintResult("PASS", test, "CMPT: " + aCompPrice[expRowComp, 0] + ":: <br>" + sExpText);
                        }
                        else
                        {
                            ReportLib.PrintResult("FAIL", test, "CMPT: " + aCompPrice[expRowComp, 0] + ":: <br>" + sExpText);
                        }

                        break;
                    }
                }
            }

            if (sComments != "")
            {
                string OutComments = driverChrome.FindElement(CFORLib.CostSummaryPage.BatchComment).Text;
                if(OutComments == sComments.ToString())
                    ReportLib.PrintResult("PASS", test, "Review Cost Summary page comments: '" + sComments.ToString() + "'");
                else
                    ReportLib.PrintResult("FAIL", test, "Review Cost Summary page comments: EXP:'" + sComments.ToString() + "' ACT:" + OutComments);
            }

        }
        public void CDCReviewCostSummary(IWebDriver driverChrome, ExtentTest test, string sCompName, string[] aPrice, string sComments, int syncTime)
        {
            String[] aFCost = new string[19];
            // Read and select Cost Forecast item
            if (aPrice.Length != aFCost.Length)
            {
                string sPrice = "12";
                ReportLib.PrintResult(test, "Used Default Price " + sPrice);
                aFCost[0] = "17.91";
                for (int i = 1; i < aFCost.Length; i++) aFCost[i] = sPrice;
            }
            else
            {
                aFCost = (string[])aPrice.Clone();
            }
            Thread.Sleep(SleepTime);

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostSummaryPage.CostSummaryTable));

            // Read Cost Summary table
            IList<IWebElement> CS_all_tables = driverChrome.FindElements(CFORLib.CostSummaryPage.CostSummaryTable);
            IList<IWebElement> CS_table = CS_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCSCell;


            bool flagHeader = true;
            bool status = true;
            int CelCounter = 0;
            int ActCelCounter = 0;
            int RowCounter = 0;
            string outText;
            string sExpText = "";
            foreach (IWebElement tRow in CS_table)
            {
                rowCSCell = tRow.FindElements(By.TagName("td"));
                outText = "";
                sExpText = "";
                if (rowCSCell[0].Text.Contains(sCompName) && rowCSCell.Count == 22)
                {
                    RowCounter++;
                    foreach (IWebElement sCell in rowCSCell)
                    {
                        sCell.Click();


                        if (!flagHeader && ActCelCounter > 2)
                        {
                            if (aFCost[CelCounter] == "N/A")
                                outText = aFCost[CelCounter++];
                            else
                                outText = String.Format("{0:C}", Convert.ToDecimal(aFCost[CelCounter++]));

                            sExpText = string.Concat(sExpText, "<br>", outText);

                            if (sCell.Text.Contains(outText))
                                status = true;
                            else
                            {
                                status = false;
                                break;
                            }

                        }

                        if (flagHeader && ActCelCounter == 1)
                        {
                            if (sCell.Text.Contains(sCompName))
                                ReportLib.PrintResult("PASS", test, "Product '" + sCompName + "' match");
                            else
                                ReportLib.PrintResult("FAIL", test, "Product '" + sCompName + "' <> '" + sCell.Text.ToString() + "' missmatch");

                            flagHeader = false;
                        }

                        ActCelCounter++;
                    }

                    if (status && sExpText != "")
                    {

                        ReportLib.PrintResult("PASS", test, sExpText + " match");
                    }
                    else
                        ReportLib.PrintResult("FAIL", test, sExpText + " missmatch");
                }
            }
            ReportLib.PrintResult(test, "Cost Summary table row count: " + RowCounter.ToString());
            if (sComments != "")
            {
                //driverChrome.FindElement(ORLib.CostSummaryPage.BatchComment).SendKeys(sComments.ToString());
                if (driverChrome.FindElement(CFORLib.CostSummaryPage.BatchComment).Text == sComments)
                    ReportLib.PrintResult("PASS", test, "Comments '" + sComments + "' match");
                else
                    ReportLib.PrintResult("FAIL", test, "Product '" + sComments + "' <> '" + driverChrome.FindElement(CFORLib.CostSummaryPage.BatchComment).Text.ToString() + "' missmatch");

                ReportLib.PrintResult(driverChrome, test, "Review Cost Summary page comments: '" + sComments.ToString() + "'");
            }

        }
        public void CloseConfMassage(IWebDriver driverChrome, ExtentTest test, int syncTime)
        {
            //Check and close Confirmation Massage
            ReportLib.PrintResult(test, "Check Confirmation Massage");

            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementExists(CFORLib.ConfirmSubmitPage.MessageText));

            string sConfMsg = clickableElement.Text;
            string sExpMsg = "Are you sure you want to submit the forecast?";
            if (sConfMsg == sExpMsg)
            {
                ReportLib.PrintResult("PASS", test, "'" + sConfMsg + "' massage appear");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'" + sConfMsg + "' <> '" + sExpMsg + "' massage missmatch");
            }
            ReportLib.PrintResult(driverChrome, test, "Confirmation Massage");
            wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(CFORLib.ConfirmSubmitPage.ConfirmSubmitButton));
            clickableElement.Click();
        }

        public void CDCBatchSubmitedPage(IWebDriver driverChrome, ExtentTest test, string sBatchNumber, int syncTime)
        {
            //Check if batch submited
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var clickableElement = wait.Until(ExpectedConditions.ElementExists(CFORLib.ConfirmMassagePage.MassageText));
            string sSubmitedMsg = clickableElement.Text;

            //Assert.AreEqual(sSubmitedMsg, "Your batch #" + sBatchNumber + " has been submitted successfully");
            string sExpMsg = "Thank You, batch #" + sBatchNumber + " has been successfully submitted!";

            if (sSubmitedMsg == sExpMsg)
            {
                ReportLib.PrintResult("PASS", test, "'" + sSubmitedMsg + "' massage appear");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "'" + sSubmitedMsg + "' <> '" + sExpMsg + "' massage missmatch");
            }
        }

        public void CDCCheckBatchExists(IWebDriver driverChrome, ExtentTest test, string sBatchNumber, int syncTime)
        {
            // Check if Batch appear in batch table
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.HomePage.BatchHistoryTable));

            IList<IWebElement> BT_all_tables = driverChrome.FindElements(CFORLib.HomePage.BatchHistoryTable);
            IList<IWebElement> rows_table = BT_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCell;

            ReportLib.PrintResult(test, "Batch Row Count: " + rows_table.Count.ToString());
            bool flag = false;
            foreach (IWebElement item in rows_table)
            {
                rowCell = item.FindElements(By.TagName("td"));
                //rowCell = item.FindElements(By.TagName("a"));
                if (rowCell[0].Text.Equals(sBatchNumber))
                {
                    rowCell[0].Click();
                    ReportLib.PrintResult(test, item.Text.ToString());
                    flag = true;
                    break;
                }

            }
            if (flag)
            {
                ReportLib.PrintResult("PASS", test, "Batch # '" + sBatchNumber + "' appear in the Batch Table");
            }
            else
            {
                ReportLib.PrintResult("FAIL", test, "Batch # '" + sBatchNumber + "' does not appear in the Batch Table");
            }
        }
        public void CDCCheckReviewBatch(IWebDriver driverChrome, ExtentTest test, string sBatchNumber, String[,] aPrice, int syncTime)
        {
            bool found = false;
            String[,] aFCost = new string[0, 24];
            // Read and select Cost Forecast item
            if (aPrice.GetLength(1) != aFCost.GetLength(1))
            {
                string sPrice = "2";
                ReportLib.PrintResult(test, "Used Default Price " + sPrice);
                aFCost[0, 3] = "17.91";
                for (int i = 4; i < aFCost.GetLength(1); i++) aFCost[0, i] = sPrice;
            }
            else
            {
                aFCost = (string[,])aPrice.Clone();
            }

            // Check if Batch appear in batch table
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.HomePage.BatchHistoryTable));

            IList<IWebElement> BT_all_tables = driverChrome.FindElements(CFORLib.HomePage.BatchHistoryTable);
            IList<IWebElement> rows_table = BT_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCell;

            foreach (IWebElement item in rows_table)
            {
                rowCell = item.FindElements(By.TagName("td"));

                if (rowCell[0].Text.Equals(sBatchNumber))
                {
                    IWebElement ReviewLink = rowCell[10].FindElement(By.TagName("a"));
                    rowCell[0].Click();
                    ReportLib.PrintResult(test, item.Text.ToString());
                    ReviewLink.Click();
                    found = true;
                    break;
                }

            }
            if (found)
            {
                for (int CompRow = 0; CompRow < aFCost.GetLength(0); CompRow++)
                {
                    wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
                    FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.CostForcastPage.CostForecastTable));
                    

                    IList<IWebElement> CF_all_tables = driverChrome.FindElements(CFORLib.CostForcastPage.CostForecastTable);
                    IList<IWebElement> CF_table_row = CF_all_tables[1].FindElements(By.TagName("table"));
                    int RowToFill = 0;
                    foreach (IWebElement item in CF_table_row)
                    {
                        if (item.Text.Contains(aFCost[CompRow, 0].Trim())) break;

                        RowToFill++;
                    }
                    IList<IWebElement> CF_row = CF_table_row[RowToFill].FindElements(By.TagName("tr"));
                    IList<IWebElement> rowCFRow;

                    //ReportLib.PrintResult(test, "Cost Forecast Row Count: " + CF_row.Count.ToString());
                    
                    foreach (IWebElement item in CF_row)
                    {

                        rowCFRow = item.FindElements(By.TagName("td"));

                        

                        if (rowCFRow.Count <= 1) continue;
                        string sExpText = "";
                        //if (sCompName != "" && !sActCompName1.Contains(sCompName)) continue;
                        sExpText = String.Format("{0}{1}:<br>", sExpText, aFCost[CompRow, 0].ToString());

                        if (aFCost[CompRow, 1] != "")
                        {
                            IWebElement lCommentLink = item.FindElement(By.CssSelector("[class='commentSvg']"));//commentTxt commentTxtPadding']"));
                            lCommentLink.Click();
                            Thread.Sleep(500);
                            string OutComents = item.FindElement(By.XPath("//*[contains(@id, 'componentComment')]")).Text;
                            if (OutComents == aFCost[CompRow, 0] + " " + aFCost[CompRow, 1].ToString())
                                ReportLib.PrintResult("PASS", test, "Component Comments: " + aFCost[CompRow, 0] + " " + aFCost[CompRow, 1].ToString());
                            else
                                ReportLib.PrintResult("FAIL", test, "Component Comments: " + aFCost[CompRow, 0] + " " + aFCost[CompRow, 1].ToString());

                            //driverChrome.FindElement(ORLib.CommentsWindow.SaveButton).Click();
                            driverChrome.FindElement(By.XPath("/html/body/div[" + (RowToFill + 2).ToString() + "]/ div/div[1]/div/div/div[2]/button")).Click();

                        }

                        //aFCost[0] = CurPrice;
                        IList<IWebElement> inputBox = item.FindElements(By.TagName("input"));
                        
                        string OutStatus = "FAIL";
                        int counter = 6;
                        bool status = true;
                        foreach (IWebElement TextItem in inputBox)
                        {
                            string OutText = TextItem.GetAttribute("value");
                            if (OutText.Contains(aFCost[CompRow, counter]))
                            {
                                if (status) status = true;
                                OutStatus = "PASS";
                                //ReportLib.PrintResult(test, "P" + (counter-4).ToString() + " Price " + OutText.ToString());                              
                            }
                            else
                            {
                                status = false;
                                OutStatus = "FAIL";                                
                            }

                            sExpText = String.Format("{0}P{4}: ACT:{1} EXP:{2} :: {3}<br>", sExpText, aFCost[CompRow, counter], OutText, OutStatus, (counter - 4).ToString());
                                                   

                            counter++;
                        }

                        if (status && sExpText != "")
                        {
                            ReportLib.PrintResult("PASS", test, sExpText);
                        }
                        else
                            ReportLib.PrintResult("FAIL", test, sExpText);

                        //ReportLib.PrintResult(test, "Used Default Price " + aFCost[counter - 1]);
                        //ReportLib.PrintResult(test, inputBox[0].Text.ToString());

                    }
                }
            }
        }
        public void CDCClickEditBatch(IWebDriver driverChrome, ExtentTest test, string sBatchNumber, int syncTime)
        {
            bool found = false;
            // Check if Batch appear in batch table
            var wait = new WebDriverWait(driverChrome, TimeSpan.FromSeconds(syncTime));
            var FinishedGoods_Table = wait.Until(ExpectedConditions.ElementExists(CFORLib.HomePage.BatchHistoryTable));

            IList<IWebElement> BT_all_tables = driverChrome.FindElements(CFORLib.HomePage.BatchHistoryTable);
            IList<IWebElement> rows_table = BT_all_tables[1].FindElements(By.TagName("tr"));
            IList<IWebElement> rowCell;

            foreach (IWebElement item in rows_table)
            {
                rowCell = item.FindElements(By.TagName("td"));

                if (rowCell[0].Text.Equals(sBatchNumber))
                {
                    IWebElement ReviewLink = rowCell[10].FindElement(By.TagName("a"));
                    rowCell[0].Click();
                    ReportLib.PrintResult(test, item.Text.ToString());
                    ReviewLink.Click();
                    found = true;
                    break;
                }

            }
            if (found)
            {

            }
        }

        public DataTable GetCompFromExcel(string FG_Item_Numbers)
        {
            string file = "BOM_Components.xlsx";

            string sqlWhere = " where FG_Item_Number in (" + FG_Item_Numbers + ")";
            // SELECT name, COUNT(email) FROM users GROUP BY email HAVING(COUNT(email) > 1)
            //DataTable result = ExcelLib.QueryExcel("select max(WRINItemID), max(FG_Item_Number), max(COMPONENT_ITEM), COMPONENT_DESC from [Sheet1$] "+
            DataTable dtResult = ExcelLib.QueryExcel("select max(WRINItemID), max(FG_Item_Number), max(COMPONENT_ITEM), COMPONENT_DESC from " +
            "(select* from [Sheet1$]" + sqlWhere + ") group by COMPONENT_DESC, COMPONENT_ITEM", file);

            return dtResult;

        }


    }
}
