﻿using System;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using System.Data;
using HAVITestProject.SupplierCatalogManager;
using HAVITestProject;

namespace HAVITestProject
{
    
    [SetUpFixture]
    public abstract class Debug
    {
        ExtentReports extent = new ExtentReports();
        CDCAppDriver CDCAppDriver = new CDCAppDriver();
        ExcelLib ExcelLib = new ExcelLib();
        ReportLib ARTSReport = new ReportLib();
        getSupplierBOM getSupplierBOM = new getSupplierBOM();
        SOAPLib SOAPLib = new SOAPLib();
        
        ExtentTest test;
        //string file = "BOM_Components.xlsx";

        [OneTimeSetUp]
        protected void BuildReport()
        {
            // start reporters

            var dir = TestContext.CurrentContext.TestDirectory + "\\";
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(dir + "CDC-ARTS-debug.html");
            htmlReporter.Configuration().DocumentTitle = "HAVI - CDC Automation";
            htmlReporter.Configuration().ReportName = "CDC - ARTS";
            extent.AttachReporter(htmlReporter);

        }
        [SetUp]
        public void BeforeTest()
        {
            test = extent.CreateTest(TestContext.CurrentContext.Test.Name);
        }

        [TestFixture]
        public class TestInitializeWithNullValues2 : Debug
        {
            [Test]
            public void Debug()
            {

                
                //select max(WRINItemID), max(FG_Item_Number), max(COMPONENT_ITEM), COMPONENT_DESC from
                //(select* from Sheet1 where FG_Item_Number in ('02813084', '03210045', '03210057'))
                //group by COMPONENT_DESC, COMPONENT_ITEM

                //select WRINItemID, FG_Item_Number, COMPONENT_ITEM, COMPONENT_DESC from [Sheet1$] group by COMPONENT_DESC
                //string sqlWhere = " where FG_Item_Number in (02813084, 03210045, 03210057)";
                // SELECT name, COUNT(email) FROM users GROUP BY email HAVING(COUNT(email) > 1)
                //DataTable result = ExcelLib.QueryExcel("select max(WRINItemID), max(FG_Item_Number), max(COMPONENT_ITEM), COMPONENT_DESC from [Sheet1$] "+
                //DataTable result = ExcelLib.QueryExcel("select max(WRINItemID), max(FG_Item_Number), max(COMPONENT_ITEM), COMPONENT_DESC from "+
                //"(select* from [Sheet1$]" + sqlWhere + ") group by COMPONENT_DESC, COMPONENT_ITEM", file);
                //DataTable result = CDCAppDriver.GetCompFromExcel("02813084, 03210045, 03210057");

                //DataTable result = CDCAppDriver.GetCompFromExcel("02682018, 02682021");
                //ExcelLib.PrintDataSet(result);

                //string Item = getSupplierBOM.itemNumber;



            }

            
        
        }

        [OneTimeTearDown]
        protected void TearDown()
        {
            extent.Flush();
        }

        [TearDown]
        public void ClosePage()
        {
            //driver.Close();
            //driver.Quit();
            ARTSReport.PrintResult(test, "Logout Successful");
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stacktrace = string.IsNullOrEmpty(TestContext.CurrentContext.Result.StackTrace)
                    ? ""
                    : string.Format("{0}", TestContext.CurrentContext.Result.StackTrace);
            Status logstatus;

            switch (status)
            {
                case TestStatus.Failed:
                    logstatus = Status.Fail;
                    break;
                case TestStatus.Inconclusive:
                    logstatus = Status.Warning;
                    break;
                case TestStatus.Skipped:
                    logstatus = Status.Skip;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            test.Log(logstatus, "Test ended with " + logstatus + stacktrace);
            // calling flush writes everything to the log file

        }
    }
}
