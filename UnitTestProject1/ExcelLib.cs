﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data;
using NUnit.Framework;
using System.Reflection;
using System.IO;
using ExcelDataReader;

namespace HAVITestProject
{

    class ExcelLib
    {
        public DataTable QueryExcel(string query, string file)
        {
            var dir = TestContext.CurrentContext.TestDirectory + "\\";
            //OleDbConnection con = new OleDbConnection(
            //        "provider=Microsoft.Jet.OLEDB.4.0;data source="
            //        + dir+file + ";Extended Properties=Excel 8.0;");

            // Excel XLSX
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data Source=" + dir + file + ";" + "Extended Properties=" + "\"" + "Excel 12.0;HDR=YES;" + "\"";

            OleDbConnection con = new OleDbConnection(connectionString);

            StringBuilder stbQuery = new StringBuilder();
            stbQuery.Append(query);
            OleDbDataAdapter adp = new OleDbDataAdapter(stbQuery.ToString(), con);

            DataTable dsXLS = new DataTable();
            adp.Fill(dsXLS);

            return (dsXLS);
                       
        }
        public DataTable QueryExcel(string query, string dir,string file)
        {
            //var dir = TestContext.CurrentContext.TestDirectory + "\\";
            //OleDbConnection con = new OleDbConnection(
            //        "provider=Microsoft.Jet.OLEDB.4.0;data source="
            //        + dir+file + ";Extended Properties=Excel 8.0;");

            // Excel XLSX
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Data Source=" + dir + file + ";" + "Extended Properties=" + "\"" + "Excel 12.0;HDR=YES;" + "\"";

            OleDbConnection con = new OleDbConnection(connectionString);

            StringBuilder stbQuery = new StringBuilder();
            stbQuery.Append(query);
            OleDbDataAdapter adp = new OleDbDataAdapter(stbQuery.ToString(), con);

            DataTable dsXLS = new DataTable();
            adp.Fill(dsXLS);

            return (dsXLS);

        }

        public DataSet ReadXlsx(string filepath)
        {
            try
            {
                FileStream stream = File.Open(filepath, FileMode.Open, FileAccess.Read);
                //stream.Unlock("Stella",6);
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                IExcelDataReader excelReader2 = ExcelReaderFactory.CreateOpenXmlReader(stream);

                //...
                //4. DataSet - Create column names from first row
                //excelReader2.IsFirstRowAsColumnNames = true;
                DataSet result2 = excelReader2.AsDataSet();

                return result2;
            }
            catch
            {

                return null;
            }
        }

        public void PrintDataSet(DataTable dt)
        {
            Console.WriteLine("{0} Count.\n", dt.Rows.Count);
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                Console.Write(dt.Columns[curCol].ColumnName.Trim() + "\t");
            }
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    Console.Write(dt.Rows[curRow][curCol].ToString().Trim() + "\t");
                }
                Console.WriteLine();
            }
        }
        public DataTable GetExpDataFromExcel(string SheetName)
        {
            string file = "TestData.xlsx";

            DataTable dtResult = QueryExcel("select * from " + SheetName.Trim(), file);

            return dtResult;

        }

        public List<string> DataTableToList(DataTable dt)
        {
            string[] header = new string[1];

            int colCount = dt.Columns.Count;
            int rowCount = dt.Rows.Count;

            Array.Resize<string>(ref header, colCount);
            int col = 0;
            foreach (DataColumn column in dt.Columns) header[col++] = column.ColumnName;

            List<string> dtList = new List<string>();
            dtList = ExcelLib.ConvertDataTable<string>(dt);

            return (dtList);

        }
        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
