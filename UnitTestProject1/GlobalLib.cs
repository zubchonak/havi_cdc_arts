﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HAVITestProject
{
    public class GlobalLib
    {
        public string CDCEnv = "";

        public static bool IsElementPresent(IWebDriver driver, By by)
        {
            try
            {
                bool elem = driver.FindElement(by).Displayed;
                return true;

            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        public static string PrintDataTable(DataTable table)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (null != table && null != table.Rows)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    foreach (var item in dataRow.ItemArray)
                    {
                        sb.Append(item);
                        sb.Append("\t");
                    }
                    sb.AppendLine();
                }

                data = sb.ToString();
            }
            return data;
        }

        public static DataTable CDCQueryTableToDataTable(string TextSQL)
        {
            DataTable ReturnDataTable = new DataTable();
            try
            {
                string connString = Properties.Settings.Default.cdcdbqa.ToString();
                //string connString = Properties.Settings.Default.cdcdbdev.ToString();

                SqlConnection sqlConnection1 = new SqlConnection(connString);
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;
                

                cmd.CommandText = TextSQL;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                sqlConnection1.Open();

                reader = cmd.ExecuteReader();
                // Data is accessible through the DataReader object here.
                ReturnDataTable.Load(reader);

                sqlConnection1.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

            return (ReturnDataTable);

        }
        public static DataTable CDCQueryTableToDataTable(string TextSQL, string CDCEnv)
        {
            DataTable ReturnDataTable = new DataTable();
            try
            {
                string connString = "";

                Security Security = new Security();
                CDCEnv = CDCEnv.ToUpper();


                if (CDCEnv == "QA") connString = Properties.Settings.Default.cdcdbqa.ToString();
                else if (CDCEnv == "PL_QA") connString = Properties.Settings.Default.hcqadbhcapp.ToString();
                
                else if (CDCEnv == "DEV") connString = Properties.Settings.Default.cdcdbdev.ToString();
                else if (CDCEnv == "PL_DEV") connString = Properties.Settings.Default.hcdvdbhcapp.ToString();

                else if (CDCEnv == "LOCAL") connString = Properties.Settings.Default.csadblocal.ToString();
                else if (CDCEnv == "PL_LOCAL") connString = Properties.Settings.Default.hcdblocal.ToString();

                else if (CDCEnv == "PL_DEMO") connString = Properties.Settings.Default.hcdmodbhcapp.ToString();
                else if (CDCEnv == "DEMO") connString = Properties.Settings.Default.cdcdbdmo.ToString();

                else if (CDCEnv == "PL_PROD") connString = Properties.Settings.Default.hcprdbhcapp.ToString();
                else if (CDCEnv == "PROD") connString = Properties.Settings.Default.cdcdbpr.ToString();

                else connString = Properties.Settings.Default.cdcdbqa.ToString();

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connString);
                builder.Password = Security.Decrypt(builder.Password);

                //string ConPassword = builder.Password;
                //string outText1 = Security.Encrypt(ConPassword);
                //string outText2 = Security.Decrypt(outText1);
                //string outText3 = Security.Decrypt(ConPassword);


                SqlConnection sqlConnection1 = new SqlConnection(builder.ConnectionString);
                
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;


                cmd.CommandText = TextSQL;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                sqlConnection1.Open();

                reader = cmd.ExecuteReader();
                // Data is accessible through the DataReader object here.
                ReturnDataTable.Load(reader);

                sqlConnection1.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

            return (ReturnDataTable);

        }
        public static string CDCQuerySingle(string TextSQL, string CDCEnv)
        {
            DataTable ReturnDataTable = new DataTable();
            string OutText = "";
            try
            {
                string connString = "";

                if (CDCEnv == "QA") connString = Properties.Settings.Default.cdcdbqa.ToString();
                else if (CDCEnv == "PL_QA") connString = Properties.Settings.Default.hcqadbhcapp.ToString();

                else if (CDCEnv == "DEV") connString = Properties.Settings.Default.cdcdbdev.ToString();
                else if (CDCEnv == "PL_DEV") connString = Properties.Settings.Default.hcdvdbhcapp.ToString();

                else if (CDCEnv == "LOCAL") connString = Properties.Settings.Default.csadblocal.ToString();
                else if (CDCEnv == "PL_LOCAL") connString = Properties.Settings.Default.hcdblocal.ToString();
                                
                else if (CDCEnv == "PL_DEMO") connString = Properties.Settings.Default.hcdmodbhcapp.ToString();
                else if (CDCEnv == "DEMO") connString = Properties.Settings.Default.cdcdbdmo.ToString();

                else if (CDCEnv == "PL_PROD") connString = Properties.Settings.Default.hcprdbhcapp.ToString();
                else if (CDCEnv == "PROD") connString = Properties.Settings.Default.cdcdbpr.ToString();

                else connString = Properties.Settings.Default.cdcdbqa.ToString();

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connString);
                builder.Password = Security.Decrypt(builder.Password);

                SqlConnection sqlConnection1 = new SqlConnection(builder.ConnectionString);
                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;


                cmd.CommandText = TextSQL;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection1;

                sqlConnection1.Open();

                reader = cmd.ExecuteReader();
                // Data is accessible through the DataReader object here.
                ReturnDataTable.Load(reader);

                OutText = (from DataRow dRow in ReturnDataTable.Rows
                               select dRow[0].ToString()).FirstOrDefault().ToString();

                sqlConnection1.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);
            }

            return (OutText);

        }
        public void ResizeArray<T>(ref T[,] original, int newCoNum, int newRoNum)
        {
            var newArray = new T[newCoNum, newRoNum];
            int columnCount = original.GetLength(1);
            int columnCount2 = newRoNum;
            int columns = original.GetUpperBound(0);
            for (int co = 0; co <= columns; co++)
                Array.Copy(original, co * columnCount, newArray, co * columnCount2, columnCount);
            original = newArray;
        }


    }
    public static class DBLib
    {
        public static DataTable EnumToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable();
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();
            //Create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                dt.Columns.Add(pi.Name, pi.PropertyType);
            }
            //Populate the table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {

                    dr[pi.Name] = pi.GetValue(item, null);
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}

