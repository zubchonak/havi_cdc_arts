﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAVITestProject
{
    public static class CFORLib
    {
        public struct HomePage
        {
            public static By CreateNewBatchButton = By.Id("createNewBatchButton");
            public static By BatchHistoryTable = By.ClassName("batchHistoryTable");
            public static By HeaderPageButton = By.XPath(@"//*[@id='root']/div/div[3]/div/div/div[1]/button/div/span/span[2]");
            public static By GoHomeButton = By.ClassName("muidocs-icon-custom-github");
            public static By CostProjectionPageButton = By.Id(@"CF");
                                                                    
        }

        public struct LoginPage
        {
            public static By PL_LoginButton = By.Id("loginButton");
            public static By UserName = By.Id(@"username");
            public static By Password = By.Id(@"password");
            public static By LoginButton = By.XPath("/html/body/div/div/div[2]/form/div/div[3]/button");
            public static By Massage = By.XPath(@"/html/body/div/form/div[1]/div/div[1]/div[2]/div/div/div[2]");
        }

        public struct NewBatchPage
        {
            public static By SaveAndContinueButton = By.Id("nxtStepBtn0");
            public static By BatchNumberLable = By.ClassName("BatchNum--label");
            public static By BatchNumber = By.ClassName("BatchNum--number");
            public static By EffectiveDate = By.Id(@"effectiveDate");
            public static By BatchName = By.Id(@"batchField");
            public static By BatchDesc = By.Id(@"batchDesc");
            public static By BatchCategoryButton = By.XPath(@"//*[@id='batchCategory']/div[1]/button");            
            public static By CategoryItemList = By.XPath(@"/html/body/div[3]/div/div/div/div/span"); //role="menuitem"
        }

        public struct FinishGoodsPage
        {
            public static By SaveAndContinueButton = By.Id("nxtStepBtn1");
            public static By FinishedGoodsTable = By.ClassName("finishedGoodsTable");

        }

        public struct CostForcastPage
        {
            public static By SaveAndContinueButton = By.Id("nxtStepBtn2");
            public static By CostForecastTable = By.ClassName("costForecastTable");
        }                                                      

        public struct CommentsWindow
        {
            public static By SaveButton = By.XPath("/html/body/div[2]/div/div[1]/div/div/div[2]/button[2]");
            
        }

        public struct CostSummaryPage
        {
            public static By SubmitButton = By.Id("submitBtn3");
            public static By CostSummaryTable = By.ClassName("costSummaryTable");
            public static By BatchComment = By.Id("batchComment");
            public static By CancelButton = By.XPath("//*[@id='reviewBackBtn']/span");
        }

        public struct ConfirmSubmitPage
        {
            public static By MessageText = By.XPath("/ html / body / div[2] / div / div[1] / div / div / div[1]");
            public static By ConfirmSubmitButton = By.Id("confirmSubmitbtn");

        }

        public struct ConfirmMassagePage
        {
            public static By GoToHomeButton = By.Id(@"goToHomeBtn");
            public static By MassageText = By.XPath(@"//*[@id='root']/div/div[3]/div/h3");
        }

        public struct ReviewBatch
        {
            public static By ViewSummaryButton = By.Id("viewSummaryBtn");
            public static By ViewCostFrcstButton = By.Id("viewCostFrcstBtn");
        }
    }



    public static class PAORLib
    {
        public struct Login
        {
            public static By UserId = By.Id("User");
            public static By Password = By.Id("Password");
        }
        public struct HomePage
        {
            public static By CreateNewBatchButton = By.Id("createNewBatchButton");
            public static By BatchHistoryTable = By.ClassName("batchHistoryTable");
            public static By HeaderPageButton = By.XPath(@"//*[@id='root']/div/div[3]/div/div/div[1]/button/div/span/span[2]");
            public static By GoHomeButton = By.ClassName("muidocs-icon-custom-github");
            public static By CostProjectionPageButton = By.Id(@"CF");

        }
        public struct NewBatchPage
        {
            public static By SaveAndContinueButton = By.Id("nxtStepBtn0");
            public static By BatchNumberLable = By.ClassName("BatchNum--label");
            public static By BatchNumber = By.ClassName("BatchNum--number");
            public static By EffectiveDate = By.Id(@"effectiveDate");
            public static By BatchName = By.Id(@"batchField");
            public static By BatchDesc = By.Id(@"batchDesc");
            public static By BatchCategoryButton = By.XPath(@"//*[@id='batchCategory']/div[1]/button");
            public static By CategoryItemList = By.XPath(@"/html/body/div[3]/div/div/div/div/span"); //role="menuitem"
        }

    }



    public static class ORJde
    {
        public struct Login
        {
            public static By UserId = By.Id("User");
            public static By Password = By.Id("Password");
            public static By SignInButton = By.XPath(@"//*[@id='F1']/table/tbody/tr/td/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/div[14]/input");
            public static By WRINSearch = By.XPath(@"//*[@id='listFav_14408067200020_FFKHQGMQDG_HHMJSYYYYY_APP_P5741002_W5741002B_null_40']/table/tbody/tr/td[2]");

            public static By rbEnv = By.Id("env_NACRP900J");
            public static By EnvSelect = By.Id("selectNACRP900J");

            public static By OKButton = By.Id("SUBMIT_BUTTON");

        }

        public struct WRINSearch
        {
            public static By WRINField = By.XPath("//*[@id='qbeRow0_1']/td[2]/div/nobr/input");
            public static By WRINFieldTable = By.XPath("//*[@id='jdeGridContainer0_1']");
            public static By StatusField = By.XPath("//*[@id='qbeRow0_1']/td[8]/div/nobr/input");
            public static By HeaderColumnRow = By.ClassName("JSQBECell");
            
        }
    }

    public static class GlobalParam
    {
        public struct WorkDir
        {
            public static string ReportDir = "C:\\temp";
            public static string ReportImgDir = "";
            public static string ReportPath = "";
        }
    }

}
