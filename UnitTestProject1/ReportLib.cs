﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Drawing.Imaging;
using System.Threading;
using System.Data;

namespace HAVITestProject
{
    public class ReportLib
    {
        public void PrintResult(string InText)
        {

            System.Diagnostics.Debug.WriteLine(InText);
            
        }
        public void PrintResult(ExtentTest test, string InText)
        {
            
            System.Diagnostics.Debug.WriteLine(InText);
            test.Info(InText);
            
        }
        public void PrintResult(IWebDriver driver, ExtentTest test, string InText)
        {
            
            System.Diagnostics.Debug.WriteLine(InText);
            test.Info(InText, MediaEntityBuilder.CreateScreenCaptureFromPath(GetScreenshot(driver)).Build());
            //test.Info(InText);
        }
        public void PrintResult(string status, ExtentTest test, string InText)
        {
            Status logstatus;
            
            switch(status.ToUpper())
            {
                case "FAIL":
                    logstatus = Status.Fail;
                    //Assert.Fail(InText);
                    break;
                case "WARNING":
                    logstatus = Status.Warning;
                    break;
                case "SKIP":
                    logstatus = Status.Skip;
                    break;
                case "PASS":
                    logstatus = Status.Pass;
                    break;
                default:
                    logstatus = Status.Pass;
                    break;
            }

            
            test.Log(logstatus, InText);
            System.Diagnostics.Debug.WriteLine("****"+status+"**** "+InText);
            

        }
            
        private DateTime date;
        private StringBuilder reportcsv;
        private string filePath;
        private string fileName;
          
        public void Reports(string BrowserType, string url)
        {
            //string BrowserType = BrowserType;
            //string.url = url;
            date = DateTime.Now;
            fileName = date.Date.Date.ToShortDateString() + date.TimeOfDay.Hours.ToString() + date.TimeOfDay.Minutes.ToString();
            reportcsv = new StringBuilder();
            filePath = @"C:\Temp\" + fileName + ".csv";
            CreateCsvFile();
        }
        private void CreateCsvFile()
        {
            reportcsv.Append("StepDescription,");
            reportcsv.Append("Pass/Fail,");
            reportcsv.Append("Exception");
            File.AppendAllText(filePath, reportcsv.ToString());
        }
        public void AddLine(string description, string result, string exception)
        {
            reportcsv.Append(Environment.NewLine);
            reportcsv.Append(description + ",");
            reportcsv.Append(result + ",");
            reportcsv.Append(exception + ",");
            reportcsv.Append(Environment.NewLine);
            File.WriteAllText(filePath, reportcsv.ToString());
        }
        public string GetScreenshot(IWebDriver driver)
        {
            string ImgDir = Settings.Report.ReportDir + "\\img\\";
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            string filename = "ss"+ DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
            ss.SaveAsFile(ImgDir + filename);
            Thread.Sleep(2000);
            return ImgDir + filename;

            

        }
        
    }
}
