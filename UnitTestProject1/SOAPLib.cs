﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Reflection;
using NUnit.Framework;
using HAVITestProject.SupplierCatalogManager;
using HAVITestProject.GetUomConversionManager;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading;
using System.Text.RegularExpressions;
using ClosedXML.Excel;
using System.Windows.Forms;

namespace HAVITestProject
{
    public class SOAPLib
    {
        public string JDEEnv = Settings.RunTimeParam.CSAEnv;

        string header = @"<soapenv:Header>
                                <wsse:Security soapenv:mustUnderstand=""0"" xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
                                    <wsse:UsernameToken wsu:Id = ""UsernameToken-58613B91772C324F3314591769992933"">
                                    <wsse:Username >interop</wsse:Username>
                                    <wsse:Password Type = ""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"">HGSinpw1</wsse:Password>
                                    <wsse:Nonce EncodingType = ""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">ABCDed</wsse:Nonce>
                                    <wsu:Created>2016 - 06 - 20T14: 56:39.293Z</wsu:Created>
                                    </wsse:UsernameToken>
                                </wsse:Security>
                        </soapenv:Header>";
                
        public static void Main(string[] args)
        {
            SOAPLib SOAPLib = new SOAPLib();
            //string pathToTestLibrary = TestContext.CurrentContext.TestDirectory + "\\HAVITestProject.exe";
            string CDCEnv = SOAPLib.JDEEnv;
            //string CDCEnv = "PROD";
            Console.WriteLine("Test in " + CDCEnv + " environment");
            Console.WriteLine("1) Test for all users WSI Components");
            Console.WriteLine("2) Print WSI Components");
            Console.WriteLine("3) Test Service Call (Debug)");
            Console.WriteLine("4) Print WSI");
            Console.WriteLine("5) Check CDC Users Category vs. JDE Category");
            Console.WriteLine("6) JDE Performance Test");
            Console.WriteLine("7) JDE Print UOM");
            Console.WriteLine("8) Print History Components Type Codes");
            Console.WriteLine("9) Check missing JDE UOM");
            Console.WriteLine("10) Debug script");
            Console.WriteLine("11) Get JDE script");
            int LastOption = 11;

            string InputText = "";
            do {
                //Console.WriteLine("");
                InputText = Console.ReadLine();
            } while (InputText.Trim() == "");
            int mode;
            Int32.TryParse(InputText, out mode);
            while (mode > LastOption || mode <= 0)
            {
                Console.WriteLine("Invalid input");
                InputText = Console.ReadLine();
                Int32.TryParse(InputText, out mode);
            }

            //int mode = Convert.ToInt32(InputText);
            if(mode == 1)
            {
                //SOAPLib SOAPLib = new SOAPLib();

                //DataTable WSIDataTable = SOAPLib.GetListOfWSI();
                DataTable UserData = SOAPLib.GetUserData();
                DataTable table = new DataTable();
                table.Columns.Add("UserEmail", typeof(string));
                table.Columns.Add("WSI", typeof(int));
                table.Columns.Add("WRIN", typeof(string));
                table.Columns.Add("BOM", typeof(bool));
                table.Columns.Add("BOM Count", typeof(int));

                int WSINum = 0;
                var UserList = (from DataRow dRow in UserData.Rows
                                where dRow["RoleName"].ToString() == "Supplier"
                                select dRow["UserUpn"]).Distinct();

                foreach (string UserEmail in UserList)
                {
                    //Console.WriteLine(String.Format("{0}\t", UserEmail));

                    var IWSIDataTable = (from DataRow dRow in UserData.Rows
                                         where dRow["UserUpn"].ToString() == UserEmail
                                         select dRow["WSI"]);
                    foreach (string strWSINum in IWSIDataTable)
                    {

                        Int32.TryParse(strWSINum.Trim(), out WSINum);
                        //Console.WriteLine("WSI: " + Convert.ToString(WSINum));
                        DataTable SupplierLineOfSupply = SOAPLib.GetSupplierLineOfSupply(WSINum);
                        DataTable SupplierBOM = SOAPLib.GetSupplierBOM(WSINum);
                        //itemNumber
                        var WsiLOS = (from DataRow dRow in SupplierLineOfSupply.Rows
                                                select dRow["itemNumber"]).Distinct();

                        

                        var distinctItemNumber = (from DataRow dRow in SupplierBOM.Rows
                                                  select dRow["parentItemNumber"].ToString().Trim()).Distinct();

                        //int WSIId = 0;
                        foreach (string OutWrin in WsiLOS)
                        {
                            //Int32.TryParse(OutWrin["WSI"].ToString().Trim(), out WSIId);
                            int BOMCount = SupplierBOM.Select("parentItemNumber=" + OutWrin.ToString().Trim()).Length;
                            var ResultLOS = BOMCount > 0;
                            table.Rows.Add(UserEmail, WSINum, OutWrin.ToString().Trim(), ResultLOS, BOMCount);
                            //Console.WriteLine(String.Format("{0}\t{1}\t{2}\t{3}\t{4}", UserEmail, WSINum, OutWrin.ToString().Trim(), ResultLOS, BOMCount));
                        }


                    }
                    
                    var OutResult = table.Select("UserEmail='" + UserEmail.ToString().Trim().Replace("'","''") + "' AND BOM=true");
                    if (OutResult.Count() > 0)
                    {
                        DataTable result = OutResult.CopyToDataTable();
                        foreach (DataRow ResRow in result.Rows)
                        {
                            //string Row1 = row.ToString();
                            DataTable UserWrinList = table.Select("UserEmail='" + UserEmail.ToString().Trim().Replace("'", "''") + "' AND WRIN=" + ResRow["WRIN"].ToString().Trim()).CopyToDataTable();

                            var BOMCount = UserWrinList.Select("BOM=false").Count();
                            if (BOMCount > 0)
                            {
                                //Console.WriteLine("Found");
                                Console.WriteLine(GlobalLib.PrintDataTable(UserWrinList));
                            }
                            //var ResultLOS = SupplierLineOfSupply.Select("itemNumber=" + Row1).Length > 0;
                            //Console.WriteLine(String.Format("{0}\t{1}\t{2}\t{3}", WSINum, Row1, ResultLOS, WsiLOS.Count()));
                        }
                    }
                }

                Console.WriteLine("****************************************************");
                Console.WriteLine("****************************************************");
                Console.WriteLine("****************************************************");

                Console.WriteLine(GlobalLib.PrintDataTable(table));

                Console.WriteLine("****************************************************");
                Console.WriteLine("****************************************************");
                Console.WriteLine("****************************************************");

                Console.WriteLine("Test Complete");
                Console.ReadLine();
            }
            if(mode == 2)
            {
                do
                {
                    //creating object of program class to access methods  
                    SOAPLib obj = new SOAPLib();
                    Console.WriteLine("Please Enter Supplier Number..");
                    //Reading input values from console  
                    int supplierNumber = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("***********************  List of Products   *******************");
                    //Calling getSupplierBOM method  
                    DataTable SupplierLineOfSupply = obj.GetSupplierLineOfSupply(supplierNumber);
                    Console.WriteLine(GlobalLib.PrintDataTable(SupplierLineOfSupply));
                    Console.WriteLine("***********************  List of Components   *******************");
                    //Calling getSupplierBOM method  
                    DataTable SupplierBOM = obj.GetSupplierBOM(supplierNumber);
                    Console.WriteLine(GlobalLib.PrintDataTable(SupplierBOM));

                } while (Convert.ToString(Console.ReadLine()).ToLower() != "exit");
            }
            if(mode == 3)
            {
                string dir = "C:\\DZStuff\\Share\\DataTesting\\DataToTest\\";
                //string fileName = "AryztaQ12018.xlsx";
                string fileName = "Quaker Cost Submission Q2 2018.xlsx";
                ExcelLib ExcelLib = new ExcelLib();
                GlobalLib GlobalLib = new GlobalLib();
                //SOAPLib SOAPLib = new SOAPLib();

                
                DataSet excel = ExcelLib.ReadXlsx(dir + fileName);
                if (excel == null)
                    return;
                int CompCount = excel.Tables.Count;
                DataTable SummarySheet = excel.Tables["Summary"];
                //Chocolate Chip Cookies
                //string FGName = "Chocolate Chip Cookies";
                string FGName = "OATMEAL";
                
                //int index = SummarySheet.FindIndex(c => c.dRow["Column0"].ToString() == FGName);


                string WRIN = (from DataRow dRow in SummarySheet.Rows
                                 where dRow["Column0"].ToString() == FGName
                               select dRow["Column1"].ToString()).FirstOrDefault().ToString();
                DataTable FGTableAll = SummarySheet.Select("Column1 = '" + WRIN.ToString() + "'").CopyToDataTable();
                var EnumFGTable = from DataRow dRow in FGTableAll.AsEnumerable()
                               select new
                               {
                                   ItemName = FGName,
                                   WRIN = dRow.Field<string>("Column1").ToString(),
                                   WSI = dRow.Field<double>("Column2").ToString(),
                                   Cost = dRow.Field<double>("Column3").ToString()
                               };
                DataTable FGTable = EnumFGTable.EnumToDataTable();
                //string WSI = (from DataRow dRow in FGTable.Rows
                //               select dRow["WSI"].ToString()).FirstOrDefault().ToString();
                DataTable SupplierBOM = new DataTable();
                //foreach (int FG in FGTable.Rows)
                //{
                //    SupplierBOM = SOAPLib.GetSupplierBOM(5);


                //}
                DataTable dtOutComp = new DataTable();
                for (int TableIndex = 0; TableIndex < CompCount; TableIndex++)
                {
                    string name = excel.Tables[TableIndex].TableName;
                    if (name.Contains("CC "))
                    {
                        DataTable Worksheet = excel.Tables[name];
                        DataTable results = Worksheet.Select("Column0 <> '' AND Column6 <> 0 AND Column7 <> 0").CopyToDataTable();
                        var results2 = from DataRow dRow in results.AsEnumerable()
                                        select new {
                                            ItemNum = dRow.Field<string>("Column0"),
                                            Desc = dRow.Field<string>("Column3"),
                                            UOM = dRow.Field<string>("Column5"),
                                            UOMperCase = dRow.Field<double>("Column6"),
                                            Cost = dRow.Field<double>("Column7")
                                        };
                        results = results2.EnumToDataTable();
                        dtOutComp.Merge(results);

                        Console.WriteLine("{0}: {1}::{2}", name, results.Rows.Count, dtOutComp.Rows.Count);

                    }
                }





                //Console.WriteLine(GlobalLib.PrintDataTable(results));

                return;
                string[,] BOMCostArray ={
                        { "6BAMA010        ", "gjhg" },
                        { "02813084                 ", "baz" },
                        { "qux", "Denis" },
                        { "6BAMA010   ", "NotNo" },
                        { "qux", "Denis" },
                        { "6BAMA010   ", "NotNo" },
                        { "qux", "Denis" },
                        { "qux", "Denis" },
                        { "qux", "Denis" },
                        { "6BAMA010   ", "NotNo" },
                };
                SOAPLib obj = new SOAPLib();
                SupplierBOM = obj.GetSupplierBOM(45503);

                //GlobalLib GlobalLib = new GlobalLib();
                string[,] aFCWithCasePrice = new string[0,0];
                
                int currentRow = 0;
                for (int i = 0; i < BOMCostArray.GetLength(0); i++)
                {
                    var ItemCount = (from DataRow dRow in SupplierBOM.Rows
                                      where dRow["componentItemNumber"].ToString().Trim() == BOMCostArray[i,0].ToString().Trim()
                                      select dRow).ToArray();
                    if (ItemCount.Count() > 0)
                    {
                        GlobalLib.ResizeArray(ref aFCWithCasePrice, currentRow + 1, BOMCostArray.GetLength(1));

                        for (int j = 0; j < BOMCostArray.GetLength(1); j++)
                        {

                            aFCWithCasePrice[currentRow, j] = BOMCostArray[i, j].ToString().Trim();
                        }
                        currentRow++;
                    }
                }


                return;
                var ItemBOMAll = (from DataRow dRow in SupplierBOM.Rows
                                  where dRow["componentItemNumber"].ToString().Trim() == "6BAMA010" &&
                                       dRow["parentItemNumber"].ToString().Trim() == "02813084"
                                  select dRow).ToArray();

                decimal CF1 = 2;
                decimal componentQuantity = Convert.ToDecimal(ItemBOMAll[0].ItemArray[3].ToString().Trim());
                decimal costPercentage = Convert.ToDecimal(ItemBOMAll[0].ItemArray[4].ToString().Trim());

                string CompCost = (((CF1 * componentQuantity) / 100) * costPercentage).ToString();

                Console.WriteLine("Result {0}", CompCost);
                //Console.WriteLine(GlobalLib.PrintDataTable(ItemBOMAll));
            }
            if (mode == 4)
            {
                //SOAPLib SOAPLib = new SOAPLib();

                DataTable WSIDataTable = SOAPLib.GetListOfWSI();
                var IWSIDataTable = (from DataRow dRow in WSIDataTable.Rows
                                     select dRow["WSI"]);
                foreach (int WSINum in IWSIDataTable)
                {

                    string outLine = "";
                    //Console.WriteLine("WSI: " + Convert.ToString(WSINum));
                    DataTable SupplierLineOfSupply = SOAPLib.GetSupplierLineOfSupply(WSINum);
                    DataTable SupplierBOM = SOAPLib.GetSupplierBOM(WSINum);
                    //itemNumber
                    var distinctCategory = (from DataRow dRow in SupplierLineOfSupply.Rows
                                            select dRow["itemCategoryCode8"]).Distinct();
                    var distinctName = (from DataRow dRow in SupplierLineOfSupply.Rows
                                            select dRow["supplierName"]).Distinct();
                                        

                    foreach (string row in distinctCategory)
                    {
                        string Row1 = row.ToString();
                        var CatFGList = SupplierLineOfSupply.Select("itemCategoryCode8='"+Row1+"'");
                        var CatFGItem = (from DataRow FGRow in CatFGList
                                         select FGRow["itemNumber"]); 

                        var BOMinCat = (from DataRow dRow in SupplierBOM.Rows
                                        where CatFGItem.Contains(dRow["parentItemNumber"])
                                        select dRow).Distinct();

                        var BOMAll = (from DataRow dRow in SupplierBOM.Rows
                                       select dRow["componentItemNumber"]).Distinct();
                        

                        //var rowsWithTheValue = SupplierLineOfSupply.Rows.Where(a => a["Column"] == value);
                        string ResultLOS = distinctName.FirstOrDefault().ToString();
                        string outText = String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", WSINum, ResultLOS, Row1, CatFGList.Length, BOMinCat.Count(), BOMAll.Count());
                        Console.WriteLine(outText);
                        
                        
                    }
                    for (int i = 0; i < 70; i++) outLine = outLine + "-";
                    Console.WriteLine(outLine);
                }
                
            }
            if(mode == 5)
            {
                //SOAPLib SOAPLib = new SOAPLib();
                DataTable CDCUser = new DataTable();
                
                CDCUser = GlobalLib.CDCQueryTableToDataTable(@"select a.UserID, a.WsiId, b.UserUpn, c.CategoryId, c.CategoryCode, c.CategoryDesc, f.RoleName
                                                                from CdcUserSupplierSite a, CDCUser b, ProductCategory c, UserProductCategory d, CDCRole f, CDCUserRole e
                                                                where a.UserID=b.UserID and a.UserID=d.UserID and c.CategoryId = d.CategoryId and a.UserID=e.UserID and f.RoleId = e.RoleId
                                                                union all
                                                                select u.UserID, sgss.WsiId, u.UserUpn, pc.CategoryId, pc.CategoryCode, pc.CategoryDesc, f.RoleName
                                                                from CDCUser u, CDCUserSiteGroup usg, SiteGroupSupplierSite sgss, ProductCategory pc, UserProductCategory upc, CDCRole f, CDCUserRole e
                                                                where u.UserID = usg.UserID and usg.SiteGroupId = sgss.SiteGroupId and usg.UserID=upc.UserID and pc.CategoryId = upc.CategoryId  and u.UserID=e.UserID and f.RoleId = e.RoleId
                                                                order by UserID, WsiId", CDCEnv);

                

                Console.WriteLine(GlobalLib.PrintDataTable(CDCUser));

                var distinctName = (from DataRow dRow in CDCUser.Rows
                                    select dRow["UserUpn"]).Distinct();
                int StatusPass = 0;
                int StatusFail = 0;
                foreach (string row in distinctName)
                {
                    
                    DataTable UserSet = (from DataRow dRow in CDCUser.Rows
                                    where dRow["UserUpn"].Equals(row.ToString())
                                    select dRow).CopyToDataTable();

                    var distinctWsi = (from DataRow dRow2 in UserSet.Rows
                                       select dRow2["WsiId"]).Distinct();

                    var distinctCategoryCode = (from DataRow dRow2 in UserSet.Rows
                                       select dRow2["CategoryCode"]).Distinct();

                    var RoleName = (from DataRow dRow2 in UserSet.Rows
                                                where dRow2["UserUpn"].Equals(row.ToString())
                                                select dRow2["RoleName"]).Distinct();

                    object[] arrayJDECat = new string[] { };
                    
                    foreach (string OutWSI in distinctWsi)
                    {
                        DataTable SupplierLineOfSupply = SOAPLib.GetSupplierLineOfSupply(Int32.Parse(OutWSI));
                        var dstJDECategoryCode = (from DataRow dRow in SupplierLineOfSupply.Rows
                                                    select dRow["itemCategoryCode8"]).Distinct().ToArray();
                        arrayJDECat = arrayJDECat.Concat(dstJDECategoryCode).Distinct().ToArray();
                    }
                    string status = "*****FAIL*****";
                    Console.WriteLine("{2}\tCount:: JDE:{0}  CDC:{1}", arrayJDECat.Length, distinctCategoryCode.Count(), row.ToString());

                    foreach (string OutCDCCat in distinctCategoryCode)
                    {
                        arrayJDECat = (from e in arrayJDECat
                                       select e.ToString().Trim()).ToArray();
                        if (arrayJDECat.Contains(OutCDCCat.ToString().Trim()))
                        {
                            status = "PASS";
                            StatusPass++;
                        }
                        else
                        {
                            status = "**FAIL**";
                            StatusFail++;
                        }
                        Console.WriteLine("\t {0}\t{1}", OutCDCCat.ToString().Trim(), status);
                    }
                    
                }
                Console.WriteLine("\r\nTest Result: PASS:{0}\tFAIL:{1}\r\n", StatusPass, StatusFail);
            }
            if(mode == 6)
            {
                int OutInt;
                Console.WriteLine("Enter number of VUsers");
                InputText = Console.ReadLine();
                Int32.TryParse(InputText, out OutInt);
                while (InputText.Trim() != "")
                {

                    Int32.TryParse(InputText, out OutInt);
                    if (OutInt <= 0) break;
                    for (int trd = 1; trd <= OutInt; trd++)
                    {
                        
                        //ThreadStart childref = new ThreadStart(CallToChildThread);
                        string VUName = trd.ToString().PadLeft(4, '0') + " VUser";
                        Console.WriteLine("In Main: Creating the Child thread");
                        Thread childThread = new Thread(() => CallToChildThread(VUName));
                        childThread.IsBackground = true;
                        childThread.Name = VUName;
                        childThread.Start();
                        //childThread.Join();
                        Console.WriteLine("Start {0}", childThread.Name);
                    }
                    Console.WriteLine("Loaded {0} VUsers", InputText);
                    InputText = Console.ReadLine();
                    Int32.TryParse(InputText, out OutInt);
                }
                
            }
            if (mode == 7)
            {
                //Console.WriteLine("UserName: {0}", Environment.UserName);
                do { 
                    SOAPLib obj = new SOAPLib();
                    Console.WriteLine("Please Enter Item Number..");
                    //Reading input values from console  
                    string itemNumber = Console.ReadLine();
                    DataTable GetUomConv = obj.GetUomConversionManager(itemNumber);
                    Console.WriteLine(GlobalLib.PrintDataTable(GetUomConv));

                } while (Convert.ToString(Console.ReadLine()).ToLower() != "exit") ;
        }
            if (mode == 8)
            {
                //SOAPLib SOAPLib = new SOAPLib();
                DataTable CompList = new DataTable();
                DataTable TypeCodeTemp = new DataTable();
                DataTable TypeCode = new DataTable();
                DataTable HFGList = GlobalLib.CDCQueryTableToDataTable(@"select fg.FgDesc from[dbo].[HistBatchFinishedGoods] fg");

                TypeCodeTemp.Columns.Add("TypeCode", typeof(string));

                foreach (DataRow FGWrin in HFGList.Rows)
                {
                    //if (CompWrin["FgDesc"].ToString() == "") CompWrin["FgDesc"] = "";

                    CompList = GlobalLib.CDCQueryTableToDataTable(@"select fg.HBatchId, fg.WsiId, fg.Wrin, fg.FgDesc, bci.ItemWrin, bci.ItemDesc, bfgc.HBatchFgID, bfgc.HBatchCmpItemId 
	                                                                from	[dbo].[HistBatchFinishedGoods] fg,
			                                                                [dbo].[HistBatchComponentItem] bci,
			                                                                [dbo].[HistBatchFinishedGoodsComponent] bfgc
	                                                                where	fg.HBatchFgID = bfgc.HBatchFgID 
		                                                                and bci.HBatchCmpItemId = bfgc.HBatchCmpItemId
		                                                                and fg.FgDesc = '" + FGWrin["FgDesc"] + "'");

                    //Console.WriteLine("{0}: {1}", FGWrin, CompList.Rows.Count);
                    foreach (DataRow CompWrin in CompList.Rows)
                    {
                        int res1 = CompWrin["ItemWrin"].ToString().IndexOf(FGWrin["FgDesc"].ToString());
                        if (res1 < 0) continue;
                        string NoFgPrefix = CompWrin["ItemWrin"].ToString().Substring(FGWrin["FgDesc"].ToString().Length);
                        NoFgPrefix = Regex.Replace(NoFgPrefix, @"[\d-]", string.Empty);
                        //Console.WriteLine(NoFgPrefix);
                        TypeCodeTemp.Rows.Add(NoFgPrefix);
                    }
                    
                    TypeCode.Merge(TypeCodeTemp);
                }
                TypeCode = TypeCode.DefaultView.ToTable(true, "TypeCode");

                //Console.WriteLine("#############################");

                Console.WriteLine(GlobalLib.PrintDataTable(TypeCode));
            }
            if (mode == 9)
            {
                //SOAPLib SOAPLib = new SOAPLib();
                DataTable table = new DataTable();
                table.Columns.Add("user", typeof(string));
                table.Columns.Add("cat", typeof(string));
                table.Columns.Add("WSI", typeof(string));
                table.Columns.Add("WRIN", typeof(string));
                table.Columns.Add("UOM", typeof(string));
                                
                DataTable MissingUOMTable = new DataTable();
                MissingUOMTable.Columns.Add("user", typeof(string));
                MissingUOMTable.Columns.Add("cat", typeof(string));
                MissingUOMTable.Columns.Add("WRIN", typeof(string));
                MissingUOMTable.Columns.Add("UOM", typeof(string));
                
                DataTable UserInfo = GlobalLib.CDCQueryTableToDataTable(@"select a.UserID, a.WsiId, b.UserUpn, c.CategoryId, c.CategoryCode, c.CategoryDesc, f.RoleName
                                                                            from CdcUserSupplierSite a, CDCUser b, ProductCategory c, UserProductCategory d, CDCRole f, CDCUserRole e
                                                                            where a.UserID=b.UserID and a.UserID=d.UserID and c.CategoryId = d.CategoryId and a.UserID=e.UserID and f.RoleId = e.RoleId
                                                                            union all
                                                                            select u.UserID, sgss.WsiId, u.UserUpn, pc.CategoryId, pc.CategoryCode, pc.CategoryDesc, f.RoleName
                                                                            from CDCUser u, CDCUserSiteGroup usg, SiteGroupSupplierSite sgss, ProductCategory pc, UserProductCategory upc, CDCRole f, CDCUserRole e
                                                                            where u.UserID = usg.UserID and usg.SiteGroupId = sgss.SiteGroupId and usg.UserID=upc.UserID and pc.CategoryId = upc.CategoryId  and u.UserID=e.UserID and f.RoleId = e.RoleId
                                                                            order by UserID, WsiId", CDCEnv);

                List<string> UserList = (from DataRow dRow2 in UserInfo.Rows
                                         where dRow2["UserUpn"].ToString().Contains("CDC")
                                         select dRow2["UserUpn"].ToString()).Distinct().ToList();


                foreach (string user in UserList) // Loop through List with foreach
                {
                    Console.WriteLine(user);

                    List<string> UserCatList = (from DataRow dRow2 in UserInfo.Rows
                                              where dRow2["UserUpn"].Equals(user)
                                              select dRow2["CategoryCode"].ToString()).Distinct().ToList();

                    foreach (string cat in UserCatList) // Loop through List with foreach
                    {
                        Console.WriteLine("\t{0}", cat);
                        List<string> UserCatWsiList = (from DataRow dRow2 in UserInfo.Rows
                                                        where dRow2["UserUpn"].Equals(user) && dRow2["CategoryCode"].Equals(cat)
                                                        select dRow2["WsiId"].ToString()).Distinct().ToList();

                        foreach (string WSI in UserCatWsiList) // Loop through List with foreach
                        {
                            Console.WriteLine("\t\t{0}", WSI);

                            DataTable UserCatWrinTable = SOAPLib.GetSupplierLineOfSupply(Convert.ToInt32(WSI.ToString().Trim()));

                            List<string> UserCatWrinList = (from DataRow dRow2 in UserCatWrinTable.Rows
                                                           select dRow2["itemNumber"].ToString()).Distinct().ToList();

                            foreach (string WRIN in UserCatWrinList) // Loop through List with foreach
                            {
                                Console.WriteLine("\t\t\t{0}", WRIN);

                                DataTable UserCatWrinUOMTable = SOAPLib.GetUomConversionManager(WRIN.ToString());

                                List<string> UserCatWrinUOMList = (from DataRow dRow2 in UserCatWrinUOMTable.Rows
                                                                select dRow2["toUOM"].ToString()).Distinct().ToList();

                                foreach (string UOM in UserCatWrinUOMList) // Loop through List with foreach
                                {
                                    Console.WriteLine("\t\t\t\t{0}", UOM);

                                    table.Rows.Add(user.Trim(),cat.Trim(), WSI.Trim(), WRIN.Trim(), UOM.Trim());                                    
                                }

                            }                            
                        }
                        List<string> DistUserCatUOMList = (from DataRow dRow2 in table.Rows
                                                           where dRow2["user"].Equals(user) && dRow2["cat"].Equals(cat.ToString())
                                                           select dRow2["UOM"].ToString()).Distinct().ToList();

                        List<string> DistUserWRINList = (from DataRow dRow2 in table.Rows
                                                           where dRow2["user"].Equals(user) && dRow2["cat"].Equals(cat.ToString())
                                                           select dRow2["WRIN"].ToString()).Distinct().ToList();

                        foreach (string WRIN in DistUserWRINList) // Loop through List with foreach
                        {
                            //Console.WriteLine("###{0}", WRIN);

                            List<string> DistUserWRINUOMList = (from DataRow dRow2 in table.Rows
                                                               where dRow2["user"].Equals(user) && dRow2["cat"].Equals(cat.ToString()) && dRow2["WRIN"].Equals(WRIN.ToString())
                                                                select dRow2["UOM"].ToString()).Distinct().ToList();

                            List<string> result = DistUserCatUOMList.Where(p => !DistUserWRINUOMList.Any(p2 => p2 == p)).ToList();
                            
                            //Build list of needed UOM to select if not all
                            //List<string> result = DistUserCatUOMList.Where(p => !DistUserWRINUOMList.Any(p2 => p2 == p)).ToList();
                            foreach (string UOM in result) // Loop through List with foreach
                            {
                                MissingUOMTable.Rows.Add(user.Trim(), cat.Trim(), WRIN.Trim(), UOM.Trim());
                            }   
                        }
                    }
                }
                List<string> DistWRINList = (from DataRow dRow2 in MissingUOMTable.Rows
                                             select dRow2["WRIN"].ToString().Trim()).Distinct().ToList();
                DataTable WRINUOMToAdd = new DataTable();
                WRINUOMToAdd.Columns.Add("WRIN", typeof(string));
                WRINUOMToAdd.Columns.Add("UOM", typeof(string));

                foreach (string WRIN in DistWRINList) // Loop through List with foreach
                {
                    List<string> UOMList = (from DataRow dRow2 in MissingUOMTable.Rows
                                            where dRow2["WRIN"].Equals(WRIN)
                                            select dRow2["UOM"].ToString()).Distinct().ToList();

                    foreach (string UOM in UOMList) // Loop through List with foreach
                    {
                        WRINUOMToAdd.Rows.Add(WRIN.Trim(), UOM.Trim());
                    }

                }

                XLWorkbook wb = new XLWorkbook();
                wb.Worksheets.Add(table, "AllData");
                wb.Worksheets.Add(MissingUOMTable, "MissingUOMTable");
                wb.Worksheets.Add(WRINUOMToAdd, "WRINUOMToAdd");

                string file = "C:\\DZStuff\\MissingUOM.xlsx";

                wb.SaveAs(file, true);
                Console.WriteLine("File saved at '" + file + "'");
                //return;
            }

            if (mode == 10)
            {
                //Settings Settings = new Settings();
                DataTable table = SOAPLib.NewGetSupplierBOM(15477);

                string ReportDir = Settings.Report.ReportDir;
                Console.WriteLine(ReportDir);

                ReportDir = "C:/TestReport/.....";
                Console.WriteLine(ReportDir);

                Console.ReadLine();

            }

            Console.WriteLine("Press <ENTER> to Exit..");
            Console.ReadLine();

        }
        public static void CallToChildThread(string ThreadName)
        {
            SOAPLib SOAPLib = new SOAPLib();
            DataTable WSIDataTable = SOAPLib.GetListOfWSI();
            var IWSIDataTable = (from DataRow dRow in WSIDataTable.Rows
                                 select dRow["WSI"]);
            foreach (int WSINum in IWSIDataTable)
            {
                long SupplierLineOfSupply = SOAPLib.CallSupplierLineOfSupply(WSINum);

                long SupplierBOM = SOAPLib.CallSupplierBOM(WSINum);
                
                Console.WriteLine("{2}\tWSI:{3}\tGetSupplierLineOfSupply\t{0}\tGetSupplierBOM\t{1}", SupplierLineOfSupply, SupplierBOM, ThreadName, WSINum);
            }

            Console.WriteLine("Done");

        }
        public DataTable GetSupplierBOM(int supplierNumber)
        {
            HttpWebRequest request;
            if (JDEEnv == "QA")
            {
                //Calling CreateSOAPWebRequest method QA
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            else if (JDEEnv == "PROD")
            {
                //Calling CreateSOAPWebRequest method  Prod
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerPROD);
            }
            else
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request  
            SOAPReqBody.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:havi=""http://havi.e1.bssv.JP570302/"">" + header.ToString() +
                                   @"<soapenv:Body>
                                   <havi:getSupplierBOM>
                                   <supplierNumber>" + Convert.ToString(supplierNumber) + @"</supplierNumber>
                                    </havi:getSupplierBOM>
                                    </soapenv:Body>
                                    </soapenv:Envelope>");

            

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    //reading stream  
                    var ServiceResult = rd.ReadToEnd();
                    //writting stream result on console

                    //Console.WriteLine(ServiceResult);
                    using (DataTable table = new DataTable())
                    {


                        table.Columns.Add("parentItemNumber", typeof(string));
                        table.Columns.Add("componentItemNumber", typeof(string));
                        table.Columns.Add("componentItemDescription", typeof(string));
                        table.Columns.Add("componentQuantity", typeof(string));
                        table.Columns.Add("costPercentage", typeof(string));
                        table.Columns.Add("componentCost", typeof(string)); 

                        var xml = XElement.Parse(ServiceResult);
                        var books = from r in xml.Descendants("supplierBOM")
                                    select new
                                    {
                                        parentItemNumber = r.Element("parentItemNumber").Value,
                                        componentItemNumber = r.Element("componentItemNumber").Value,
                                        componentItemDescription = r.Element("componentItemDescription").Value,
                                        componentQuantity = r.Element("componentQuantity").Value,
                                        costPercentage = r.Element("costPercentage").Value,
                                        componentCost = r.Element("componentCost").Value,
                                    };

                        foreach (var r in books)
                        {
                            //Console.WriteLine(String.Format("{0}  {1}  {2}  {3}  {4}", r.parentItemNumber, r.componentItemNumber, r.componentItemDescription, r.componentQuantity, r.costPercentage));
                            table.Rows.Add(r.parentItemNumber, r.componentItemNumber, r.componentItemDescription, r.componentQuantity, r.costPercentage, r.componentCost);
                        }

                    
                    return (table);
                    //Console.ReadLine();
                    }
                }
            }
        }
        public long CallSupplierBOM(int supplierNumber)
        {
            //Calling CreateSOAPWebRequest method  QA
            HttpWebRequest request;
            if (JDEEnv == "QA")
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            else if (JDEEnv == "PROD")
            {
                //Calling CreateSOAPWebRequest method  Prod
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerPROD);
            }
            else
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request  
            SOAPReqBody.LoadXml(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:havi=""http://havi.e1.bssv.JP570302/"">" + header.ToString() +
                                   @"<soapenv:Body>
                                   <havi:getSupplierBOM>
                                   <supplierNumber>" + Convert.ToString(supplierNumber) + @"</supplierNumber>
                                    </havi:getSupplierBOM>
                                    </soapenv:Body>
                                    </soapenv:Envelope>");


            long SLSStartTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            
            
            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    return ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond)-SLSStartTime);
                }

            }
        }
        public long CallSupplierLineOfSupply(int supplierNumber)
        {
            HttpWebRequest request;
            if (JDEEnv == "QA")
            {
                //Calling CreateSOAPWebRequest method QA
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            else if (JDEEnv == "PROD")
            {
                //Calling CreateSOAPWebRequest method  Prod
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerPROD);
                //request.Headers.Add(header);
            }
            else
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
            }
            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request
            string TxtReq = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:havi=""http://havi.e1.bssv.JP570302/"">" + header.ToString() +
                                @"<soapenv:Body>
                                <havi:getSupplierLineOfSupply>
                                    <supplierNumber>" + Convert.ToString(supplierNumber) + @"</supplierNumber>
                                </havi:getSupplierLineOfSupply>
                            </soapenv:Body>
                            </soapenv:Envelope>";

            SOAPReqBody.LoadXml(TxtReq);

            long SLSStartTime = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    return ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - SLSStartTime);
                }
            }
        }
        public DataTable GetSupplierLineOfSupply(int supplierNumber)
        {
            DataTable outTable = new DataTable();
            try
            {
                
                HttpWebRequest request;
                if (JDEEnv == "QA")
                {
                    //Calling CreateSOAPWebRequest method  
                    request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
                }
                if (JDEEnv == "DEMO")
                {
                    //Calling CreateSOAPWebRequest method  
                    request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
                }
                else if (JDEEnv == "PROD")
                {
                    //Calling CreateSOAPWebRequest method  Prod
                    request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerPROD);
                }
                else
                {
                    request = CreateSOAPWebRequest(Settings.EndPoints.JDESupplierCatalogueManagerCRP);
                }
                //request.Headers.Add(header);

                XmlDocument SOAPReqBody = new XmlDocument();
                //SOAP Body Request
                string TxtReq = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:havi=""http://havi.e1.bssv.JP570302/"">" + header.ToString() + 
                                          @"<soapenv:Body>
                                          <havi:getSupplierLineOfSupply>
                                             <supplierNumber>" + Convert.ToString(supplierNumber) + @"</supplierNumber>
                                          </havi:getSupplierLineOfSupply>
                                        </soapenv:Body>
                                        </soapenv:Envelope>";
            
                SOAPReqBody.LoadXml(TxtReq);

            
                using (Stream stream = request.GetRequestStream())
                {
                    SOAPReqBody.Save(stream);
                }
                //Geting response from request  
                using (WebResponse Serviceres = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                    {
                        //reading stream  
                        var ServiceResult = rd.ReadToEnd();
                        //writting stream result on console

                        using (DataTable table = new DataTable())
                        {

                    
                            table.Columns.Add("supplierNumber", typeof(string));
                            table.Columns.Add("itemNumber", typeof(string));
                            table.Columns.Add("itemDescription", typeof(string));
                            table.Columns.Add("supplierCatalogPrice", typeof(string));
                            table.Columns.Add("supplierName", typeof(string));
                            table.Columns.Add("itemCategoryCode8", typeof(string));
                            XElement xml = XElement.Parse(ServiceResult);
                                                     

                            var books = from r in xml.Descendants("supplierLineOfSupply")
                                        select new
                                        {
                                            supplierNumber = r.Element("supplierNumber").Value.Trim(),
                                            itemNumber = r.Element("itemNumber").Value.Trim(),
                                            itemDescription = r.Element("itemDescription").Value.Trim(),
                                            supplierCatalogPrice = r.Element("supplierCatalogPrice") == null ? "0" : r.Element("supplierCatalogPrice").Value.Trim(),
                                            supplierName = r.Element("supplierName").Value.Trim(),
                                            itemCategoryCode8 = r.Element("itemCategoryCode8").Value.Trim(),
                                        };
                            //outTable = books.EnumToDataTable();

                            foreach (var r in books)
                            {
                                //Console.WriteLine(String.Format("{0}  {1}  {2}  {3}  {4}  {5}", r.supplierName, r.supplierNumber, r.itemNumber, r.itemDescription, r.supplierCatalogPrice, r.itemCategoryCode8));
                                table.Rows.Add(r.supplierNumber, r.itemNumber, r.itemDescription, r.supplierCatalogPrice ?? "0", r.supplierName, r.itemCategoryCode8);
                                //table.Rows.Add(r.supplierNumber, r.itemNumber, r.itemDescription, "0", r.supplierName, r.itemCategoryCode8);
                            }

                            outTable = table.Copy();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Cannot load data from JDE.\r\nSupplier Number: " + supplierNumber);
                System.Diagnostics.Debug.WriteLine("An error occurred: '{0}'", ex);

            }
            
            return (outTable);
        }
        
        public DataTable GetUomConversionManager(string itemNumber)
        {
            HttpWebRequest request;
            if (JDEEnv == "QA")
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDEGetUomCRP);
            }
            else if (JDEEnv == "PROD")
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDEGetUomPROD);
            }
            else
            {
                request = CreateSOAPWebRequest(Settings.EndPoints.JDEGetUomCRP);
            }
            
            //request.Headers.Add(header);

            XmlDocument SOAPReqBody = new XmlDocument();
            //SOAP Body Request
            string TxtReq = @"<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:havi='http://havi.e1.bssv.JP574103/'>
                                <soapenv:Body>
                                    <havi:getUomConversion>
                                        <items>
                                            <itemNumber>" + Convert.ToString(itemNumber) + @"</itemNumber>
                                        </items>
                                        </havi:getUomConversion>
                                    </soapenv:Body>
                                </soapenv:Envelope>";

            
            SOAPReqBody.LoadXml(TxtReq);


            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            //Geting response from request  
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    //reading stream  
                    var ServiceResult = rd.ReadToEnd();
                    //writting stream result on console

                    using (DataTable table = new DataTable())
                    {


                        table.Columns.Add("descriptionLine1", typeof(string));
                        table.Columns.Add("itemNumber", typeof(string));
                        table.Columns.Add("toUOM", typeof(string));
                        table.Columns.Add("conversionFactor", typeof(string));

                        XElement xml = XElement.Parse(ServiceResult);
                        var books = from r in xml.Descendants("itemUomConversionFactors")
                                    select new
                                    {
                                        descriptionLine1 = r.Element("descriptionLine1").Value,
                                        itemNumber = r.Element("itemNumber").Value,
                                        toUOM = r.Element("toUOM").Value,
                                        conversionFactor = r.Element("conversionFactor").Value,                                        
                                    };

                        foreach (var r in books)
                        {
                            //Console.WriteLine(String.Format("{0}  {1}  {2}  {3}  {4}  {5}", r.supplierName, r.supplierNumber, r.itemNumber, r.itemDescription, r.supplierCatalogPrice, r.itemCategoryCode8));
                            table.Rows.Add(r.descriptionLine1, r.itemNumber, r.toUOM, r.conversionFactor);
                        }
                                                

                        //Console.WriteLine(output.supplierBOM.Length);
                                               
                        return (table);
                    }
                }
            }
        
        }
        public DataTable NewGetSupplierBOM(int supplierNumber)
        {
            //DataTable OutTable = new DataTable();
            SupplierCatalogueManagerClient SupplierCatalogueManagerClient = new SupplierCatalogueManagerClient();
            

            SupplierCatalogueManagerClient.ClientCredentials.UserName.UserName = "interop";
            SupplierCatalogueManagerClient.ClientCredentials.UserName.Password = "hgsinpw1";
            //getSupplierBOMResponse

            getSupplierBOM getSupplierBOM = new getSupplierBOM();
            //getSupplierBOMRequest getSupplierBOMRequest = new getSupplierBOMRequest();

            getSupplierBOM.supplierNumber = 47689;
            getSupplierBOM.itemNumber = "05358013";
            getSupplierBOM.supplierNumberSpecified = true;
            
            showSupplierBOM output = SupplierCatalogueManagerClient.getSupplierBOM(getSupplierBOM);

            using (DataTable table = new DataTable())
            {


                table.Columns.Add("parentItemNumber", typeof(string));
                table.Columns.Add("componentItemNumber", typeof(string));
                table.Columns.Add("componentItemDescription", typeof(string));
                table.Columns.Add("componentQuantity", typeof(decimal));
                table.Columns.Add("costPercentage", typeof(decimal));

                
                var books = from r in output.supplierBOM
                            select new
                            {
                                parentItemNumber = r.parentItemNumber == null ? "null" : r.parentItemNumber.ToString(),
                                componentItemNumber = r.componentItemNumber == null ? "null" : r.componentItemNumber.ToString(),
                                componentItemDescription = r.componentItemDescription == null ? "null" : r.componentItemDescription.ToString(),
                                componentQuantity = r.componentQuantity.ToString(),
                                costPercentage = r.costPercentage.ToString(),
                            };

                foreach (var r in books)
                {
                    Console.WriteLine(String.Format("{0}  {1}  {2}  {3}  {4}", r.parentItemNumber, r.componentItemNumber, r.componentItemDescription, r.componentQuantity, r.costPercentage));
                    table.Rows.Add(r.parentItemNumber, r.componentItemNumber, r.componentItemDescription, r.componentQuantity, r.costPercentage);
                }

                //Console.WriteLine(output.supplierBOM.Length);

                Console.ReadLine();

                return (table);
            }
        }

        public HttpWebRequest CreateSOAPWebRequest(string WsdlUrl)
        {
            //Making Web Request  
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(WsdlUrl);
            
            Req.ContentType = "text/xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            //HTTP method  
            Req.Method = "POST";
            //return HttpWebRequest  
            return Req;
        }
        public DataTable GetUserData()
        {
            DataTable CDCUser = GlobalLib.CDCQueryTableToDataTable(@"select a.UserID, a.WsiId as WSI, b.UserUpn, c.CategoryId, c.CategoryCode, f.RoleName
                                                                from CdcUserSupplierSite a, CDCUser b, ProductCategory c, UserProductCategory d, CDCRole f, CDCUserRole e
                                                                where a.UserID=b.UserID and a.UserID=d.UserID and c.CategoryId = d.CategoryId and a.UserID=e.UserID and f.RoleId = e.RoleId
                                                                union all
                                                                select u.UserID, sgss.WsiId, u.UserUpn, pc.CategoryId, pc.CategoryCode, f.RoleName
                                                                from CDCUser u, CDCUserSiteGroup usg, SiteGroupSupplierSite sgss, ProductCategory pc, UserProductCategory upc, CDCRole f, CDCUserRole e
                                                                where u.UserID = usg.UserID and usg.SiteGroupId = sgss.SiteGroupId and usg.UserID=upc.UserID and pc.CategoryId = upc.CategoryId  and u.UserID=e.UserID and f.RoleId = e.RoleId
                                                                order by UserID, WsiId", JDEEnv);
            return (CDCUser);
        }
        public DataTable GetListOfWSI()
        {
            DataTable table = new DataTable();
            string Env = JDEEnv;

            var CDCUser = GlobalLib.CDCQueryTableToDataTable(@"select a.UserID, a.WsiId as WSI, b.UserUpn, c.CategoryId, c.CategoryCode, f.RoleName
                                                                from CdcUserSupplierSite a, CDCUser b, ProductCategory c, UserProductCategory d, CDCRole f, CDCUserRole e
                                                                where a.UserID=b.UserID and a.UserID=d.UserID and c.CategoryId = d.CategoryId and a.UserID=e.UserID and f.RoleId = e.RoleId
                                                                union all
                                                                select u.UserID, sgss.WsiId, u.UserUpn, pc.CategoryId, pc.CategoryCode, f.RoleName
                                                                from CDCUser u, CDCUserSiteGroup usg, SiteGroupSupplierSite sgss, ProductCategory pc, UserProductCategory upc, CDCRole f, CDCUserRole e
                                                                where u.UserID = usg.UserID and usg.SiteGroupId = sgss.SiteGroupId and usg.UserID=upc.UserID and pc.CategoryId = upc.CategoryId  and u.UserID=e.UserID and f.RoleId = e.RoleId
                                                                order by UserID, WsiId", Env);



            //Console.WriteLine(GlobalLib.PrintDataTable(CDCUser));
            var ListWSI = (from DataRow dRow in CDCUser.Rows
                           select dRow["WSI"]).Distinct();

            table.Columns.Add("WSI", typeof(int));
            int WSIId = 0;
            foreach (string OutWSI in ListWSI)
            {
                Int32.TryParse(OutWSI.Trim(), out WSIId);
                table.Rows.Add(WSIId);
            }
            return (table);

            //Console.WriteLine(GlobalLib.PrintDataTable(table));
            table.Columns.Add("WSI", typeof(int));
            table.Rows.Add("25122");
            table.Rows.Add("22978");
            

            return (table);
        }

    }
}
