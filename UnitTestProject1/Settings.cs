﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HAVITestProject
{
    public class Settings
    {
        public class CSAEndPoints
        {
            private static string CSA_URL_LEG_QA = "https://ui.cdc-qa.havi.com/";
            private static string CSA_URL_LEG_DEV = "https://ui.cdc-dev.havi.com/";
            private static string CSA_URL_QA = "https://qa.connect.havi.com/app/login";
            private static string CSA_URL_DEMO = "https://demo.connect.havi.com/app/login";
            private static string CSA_URL_DEV = "https://dev.connect.havi.com/app/login";
            private static string CSA_URL_PROD = "https://connect.havi.com/app/login";

            public static string CsaUrlQA { get { return CSA_URL_QA; } }
            public static string CsaUrlDEV { get { return CSA_URL_DEV; } }
            public static string CsaUrlDEMO { get { return CSA_URL_DEMO; } }
            public static string CsaUrlPROD { get { return CSA_URL_PROD; } }
            public static string CsaUrlPLEGQA { get { return CSA_URL_PROD; } }
            public static string CsaUrlLEGDEV { get { return CSA_URL_PROD; } }

        }

        public class Report
        {
            private static string reportDir = TestContext.CurrentContext.TestDirectory;

            public static string ReportDir { get { return reportDir; } set { reportDir = value; } }
            public static string ReportName { get; set; }
            public static string ReportImg { get; set; }

        }
        public class RunTimeParam
        {
            private static string DefEnv = "DEMO";
            
            public static string CSAEnv { get { return DefEnv; } set { DefEnv = value; } }
            public static bool AuthUser { get; set; }
            public static bool IsUpdateRequired { get { return true; } }
            public static string UserNetId { get; set; }
            public static string UserName { get; set; }
        }
        public class EndPoints
        {
            //private static string JDESCMEnvCRP = "http://10.217.92.50:7381/PD920/SupplierCatalogueManager";
            private static string JDESCMEnvCRP = "http://hgspjdena02.perseco.com:9089/PY900/SupplierCatalogueManager";
            private static string JDESCMEnvPROD = "http://hgspjdena03:9090/PD900/SupplierCatalogueManager";
            private static string JDEUOMEnvCRP = "http://hgspjdena02:9089/PY900/GetUomConversionManager";
            //private static string JDEUOMEnvCRP = "http://10.217.92.50:7381/PD920/GetUomConversionManager";
            private static string JDEUOMEnvPROD = "http://hgspjdena03:9090/PD900/GetUomConversionManager";
            
            public static string JDESupplierCatalogueManagerCRP { get { return JDESCMEnvCRP; } }
            public static string JDESupplierCatalogueManagerPROD { get { return JDESCMEnvPROD; } }
            public static string JDEGetUomCRP { get { return JDEUOMEnvCRP; } }
            public static string JDEGetUomPROD { get { return JDEUOMEnvPROD; } }
        }

    }
}
